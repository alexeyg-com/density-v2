import lib.reader
import lib.writer
import lib.processors

def get_records(records = ['../data/gff/R64/SGD_Everything.gff3']) :
    '''
    Return read GFF records array.
    :param records : list of filenames with GFF records to read.
    '''
    records = lib.reader.read_features(records)
    print '[+] Read GFF annotations.'

    return records

def get_reference() :
    '''
    Return read reference sequences.
    '''
    reference = lib.reader.read_references(['../data/sequences/R64/chrI.fasta', '../data/sequences/R64/chrII.fasta', '../data/sequences/R64/chrIII.fasta', '../data/sequences/R64/chrIV.fasta', '../data/sequences/R64/chrV.fasta', '../data/sequences/R64/chrVI.fasta', '../data/sequences/R64/chrVII.fasta', '../data/sequences/R64/chrVIII.fasta', '../data/sequences/R64/chrIX.fasta', '../data/sequences/R64/chrX.fasta', '../data/sequences/R64/chrXI.fasta', '../data/sequences/R64/chrXII.fasta', '../data/sequences/R64/chrXIII.fasta', '../data/sequences/R64/chrXIV.fasta', '../data/sequences/R64/chrXV.fasta', '../data/sequences/R64/chrXVI.fasta', '../data/sequences/R64/chrmt.fasta'])
    print '[+] Read reference.'

    return reference

def create_coverage_records(input, kernel, assignment_kernel, filters, output = None, selection = -1, mismatches = 2, unique = False) :
    '''
    Create read coverage records.
    :param input : input functions.
    :param kernel : kernel functions.
    :param assignment_kernel : read assignment kernel functions.
    :param filters : filter functions.
    :param output : name of the file to output records to.
    :param selection : the switch for selecting reads (replicate 1, 2 or both).
    :param mismatches: maximum number of allowed mismatches.
    :param unique : if True, only unique reads are processed into records.
    '''
    if output is None :
        output = ['../results/mcmanus-reads/rib_coverage.out', '../results/mcmanus-reads/mrna_coverage.out']

    reference = get_reference()
    records = get_records()
    genes = lib.processors.group_gff_records_by_name(records)
    genes = lib.processors.unify_gff_record_groups(genes)
    print '[+] Grouped features (CDS)'

    for i in range(len(input)) :
        print '[i] Processing: %s' % output[i]
        alignment_reader = input[i]
        filter = filters[i]
        alignment = alignment_reader(selection = selection)
        
        filtered_alignment = filter(alignment, mismatches = mismatches, unique = unique)
        del alignment
        positions = lib.processors.map_alignment(reference, filtered_alignment)
        del filtered_alignment
        print '\t[+] Mapped reads to positions.'

        positions = lib.processors.cover_positions(genes, positions, assignment_kernel[i])
        print '\t[+] Covered alignments with features.'

        records = lib.processors.convert_to_mrna_array(reference, genes, positions, kernel[i], assignment_kernel = assignment_kernel[i])
        del positions
        print '\t[+] Created records.'

        lib.writer.write_pickle(records, output[i])

def create_read_records(input, kernel, filters, output = None, selection = -1, mismatches = 2, unique = False) :
    '''
    Create read records.
    :param input : input functions.
    :param kernel : kernel functions.
    :param filters : filter functions.
    :param output : name of the file to output records to.
    :param selection : the switch for selecting reads (replicate 1, 2 or both).
    :param mismatches: maximum number of allowed mismatches.
    :param unique : if True, only unique reads are processed into records.
    '''
    if output is None :
        output = ['../results/mcmanus-reads/rib_read.out', '../results/mcmanus-reads/mrna_read.out']

    reference = get_reference()
    records = get_records()
    genes = lib.processors.group_gff_records_by_name(records)
    genes = lib.processors.unify_gff_record_groups(genes)
    print '[+] Grouped features (CDS)'

    for i in range(len(input)) :
        print '[i] Processing: %s' % output[i]
        alignment_reader = input[i]
        filter = filters[i]
        alignment = alignment_reader(selection = selection)

        filtered_alignment = filter(alignment, unique = unique, mismatches = mismatches)
        del alignment
        positions = lib.processors.map_alignment(reference, filtered_alignment)
        del filtered_alignment
        print '\t[+] Mapped reads to positions.'

        positions = lib.processors.cover_positions(genes, positions, kernel[i])
        print '\t[+] Covered alignments with features.'

        records = lib.processors.convert_to_mrna_array(reference, genes, positions, kernel[i])
        del positions
        print '\t[+] Created records.'

        lib.writer.write_pickle(records, output[i])

def check_alignments(files = None, adapter = 'CTGTAGGCACCATCAATAGATCGGAAGAGCACACGTCTGA') :
    '''
    Check extended alignments.
    :param files : input files.
    '''
    if files is None :
        files = ['../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551_trim_recovered_extended.out', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552_trim_recovered_extended.out', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553_trim_recovered_extended.out', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555_trim_recovered_extended.out']
    references = get_reference()
    for file in files :
        print '[i] Checking alignments: %s' % file
        alignments = lib.reader.read_pickle(file)
        lib.processors.check_alignment(alignments, references, adapter)
        del alignments

def produce_alignment_statistics(files = None) :
    '''
    Produce alignment statistics (mismatch and multiplicity histograms) for extended alignments.
    :param files : input files.
    '''
    if files is None :
        files = ['../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551_trim_recovered_extended_grouped.out', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552_trim_recovered_extended_grouped.out', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553_trim_recovered_extended_grouped.out', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555_trim_recovered_extended_grouped.out']
    references = get_reference()
    for file in files :
        print '[i] Checking alignments: %s' % file
        alignments = lib.reader.read_pickle(file)
        lib.processors.produce_group_alignment_statistics(alignments)
        del alignments

