import numpy as np

class GeneScheduler(object) :
    def __init__(self, genes, threshold = None, n_std = 6) :
        '''
        Constructor of the gene scheduler.
        :param genes: a list of genes to keep times for.
        :param threshold: if an integer - minimum number of runtime estimates required for a reliable runtime prediction. If a float - static threshold in seconds. If a None, the feature is disabled.
        :param n_std: number of standard deviations above the mean allowed before timeout.
        '''
        self.genes = genes
        self.times = {}
        self.mean_times = {}
        self.std_times = {}
        for gene in genes :
            self.times[gene] = []
            self.mean_times[gene] = 0.0
            self.std_times[gene] = 0.0
        
        self.threshold = threshold
        self.n_std = n_std
   
    def update_gene(self, gene, time) :
        '''
        Update runtime estimate of a gene.
        :param gene: gene to update the estimate for.
        :param time: time to update the estimate with.
        '''
        self.times[gene].append(time)
        self.mean_times[gene] = np.mean(self.times[gene])
        self.std_times[gene] = np.std(self.times[gene])

    def restore_order(self, indices, params) :
        '''
        Restores original gene order.
        :param indices : gene indices 
        '''
        n_param = len(params)
        result = np.zeros((n_param, ), dtype = np.dtype(object))
        for i in xrange(n_param) :
            result[indices[i]] = params[i]
        return result.tolist()

    def reorder_genes(self, params) :
        '''
        Reorder simulation tasks for optimal execution (slowest first).
        :param params: simulation tasks to be reordered.
        '''
        n_params = len(params)
        sort_list = np.array([range(n_params), params], np.dtype(object)).transpose()
        sort_list = np.array(sorted(sort_list, key = lambda x : self.mean_times[x[1][0]], reverse = True)).transpose()
        indices = sort_list[0, :].astype(np.int32)
        params = sort_list[1, :].tolist()
        return (indices, params)

    def get_timeout(self, gene) :
        '''
        Get maximum allowed runtime for the gene.
        :param gene: gene name.
        '''
        if self.threshold is None :
            return float('Inf')
        elif isinstance(self.threshold, float) :
            return self.threshold
        elif isinstance(self.threshold, int) :
            if len(self.times[gene]) >= self.threshold :
                return self.mean_times[gene] + self.n_std * self.std_times[gene]
        return float('Inf')
