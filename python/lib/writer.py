'''
Implements routines for data output.

Alexey Gritsenko
31-10-2013
'''

import pickle
import marshal
import Bio.SeqIO

def write_fasta(sequences, filename) :
    '''
    Output FASTA sequences into a file.
    :param filename : name of the file to output sequences into.
    '''
    fout = open(filename, 'wb')
    Bio.SeqIO.write(sequences, fout, 'fasta')
    fout.close()

def write_marshal(data, filename) :
    '''
    Marshals a variable into file. Should be faster than pickling.
    :param data: the variable to be marshaled.
    :param filename: name of the file to save the marshal in.
    '''
    fout = open(filename, 'wb')
    marshal.dump(data, fout)
    fout.close()

def write_pickle(data, filename) :
    '''
    Pickles a variable into file.
    :param data: the variable to be pickled.
    :param filename: name of the file to save the pickle in.
    '''
    fout = open(filename, 'wb')
    pickle.dump(data, fout, protocol = pickle.HIGHEST_PROTOCOL)
    fout.close()
