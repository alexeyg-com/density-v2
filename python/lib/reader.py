'''
Contains routines for data input and processing.

Alexey Gritsenko
31-10-2013
'''

from SimpleNamespace import *
import re
import os
import Bio.SeqIO
import Bio.Seq
import cPickle
import marshal
import pysam
import warnings
import datetime
import copy

def read_list(filename) :
    '''
    Reads a list of lines (e.g. gene names) from a file.
    :param filename: name of the file to read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    res = []
    for line in lines :
        res.append(line.strip())
    return res

def read_fasta(filename) :
    '''
    Read reads from a fasta file.
    :param filename : name of the file to read.
    '''
    fin = open(filename, 'rb')
    dictionary = Bio.SeqIO.to_dict(Bio.SeqIO.parse(fin, 'fasta'))
    fin.close()
    return dictionary

def read_fastq(filename) :
    '''
    Read reads from a fastq file.
    :param filename : name of the file to read.
    '''
    fin = open(filename, 'rb')
    dictionary = Bio.SeqIO.to_dict(Bio.SeqIO.parse(fin, 'fastq'))
    fin.close()
    return dictionary

def read_bam(filename) :
    '''
    Read SAM/BAM file and return alignments.
    :param filename : name of the file to read.
    '''
    alignments = []
    fin = pysam.Samfile(filename, 'rb')
    for alg in fin :
        if alg.is_unmapped :
            continue
        palg = SimpleNamespace()
        palg.reference = fin.getrname(alg.tid)
        palg.mapq = alg.mapq
        palg.seq = alg.seq
        palg.name = alg.qname
        palg.begin, palg.end = alg.pos, alg.pos + alg.alen
        palg.is_reverse = alg.is_reverse
        match = 0
        for type, length in alg.cigar :
            if type == 0 :
                match += length
        palg.matches = match
        alignments.append(palg)
    fin.close()
    return alignments

def get_gff_attribute(string, attribute) :
    '''
    Return GFF attribute from string.
    :param string : string to parse (i.e. GFF description).
    :param attribute : name of the attribute.
    '''
    m = re.search(('(^|;)%s=([^;]+)($|;)') % attribute, string)
    found = None
    if m is not None :
        found = m.group(2)
    return found

def read_features(filename) :
    '''
    Read GFF features.
    :param filename : name of the file to read GFF features from.
    '''
    if not isinstance(filename, list) :
        filename = [filename]

    records = []
    for file in filename :
        fin = open(file, 'rb')
        for line in fin :
            line = line.strip()
            length = len(line)
            if length > 0 and line[0] == '#' :
                continue
            if length > 0 :
                item = line.split('\t')
                if len(item) != 9 :
                    continue
                description = item[8]
                reference = item[0]
                if reference == 'chrMito' :
                    reference = 'chrmt'
                record = SimpleNamespace()
                record.name = get_gff_attribute(description, 'Name')
                record.type = item[2]
                record.reference = reference
                record.start = int(item[3]) - 1
                record.stop = int(item[4])
                record.strand = item[6]
                record.phase = item[7]
                record.source = item[1]
                record.description = description
                records.append(record)

    return records

def read_references(filename) :
    '''
    Read references from file.
    :param filename : name of the file to read the references from.
    '''
    if not isinstance(filename, list) :
        filename = [filename]

    ref = {}
    for file in filename :
        name = os.path.basename(file).split('.')
        name = name[0]
        fin = open(file, 'rb')
        records = list(Bio.SeqIO.parse(fin, 'fasta'))
        fin.close()
        record = records[0]
        ref_record = SimpleNamespace()
        ref_record.seq = record.seq.tostring()
        ref_record.description = record.description
        ref[name] = ref_record

    return ref

def read_ingolia_alignment(filename) :
    '''
    Read read alignment in Ingolia format.
    :param filename : name of the file to read the alignment from.
    '''
    fin = open(filename, 'rb')

    alignments = []
    group = []
    for line in fin :
        line = line.strip()
        length = len(line)
        # Check if we hit a comment line
        if length > 0 and line[0] == '#' :
            continue

        # We hit a group separator
        if length == 0 : 
            group_size = len(group)
            counts = set()
            for i in range(group_size) :
                item = group[i].split()
                reference = item[3]
                if reference == 'chrMito' :
                    reference = 'chrmt'
                alg = SimpleNamespace()
                seq = item[0]
                counts.add(int(item[1]))
                alg.mismatches = len(seq) - int(item[2])
                alg.reference = reference
                start, stop = int(item[4]) - 1, int(item[5]) - 1
                if start < stop :
                    alg.is_reverse = False
                else :
                    alg.is_reverse = True
                    start, stop = stop, start
                    seq = Bio.Seq.Seq(seq).reverse_complement().tostring()
                alignment_length = stop - start + 1
                if not alg.is_reverse :
                    seq = seq[:alignment_length] + seq[alignment_length:].lower()
                else :
                    seq = seq[:-alignment_length].lower() + seq[-alignment_length:]
                alg.seq = seq
                alg.begin = start
                alg.end = stop + 1
                alg.maximum_length = int(item[6])
                alg.ambiguity_length = int(item[7])
                count = int(item[1])
                group[i] = alg
            
            if len(counts) > 1 :
                print '[-] Error: multiple different counts encountered per read.'
            count = list(counts)[0]
            for i in range(count) :
                alignments.append(copy.deepcopy(group))
            group = []
        else :
            group.append(line)
    fin.close()

    return alignments

def read_changes_file(filename) :
    '''
    Read SGD genome changes file (used for updating Ingolia alignment).
    :param filename : name of the file to read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    res = []
    for line in lines :
        line = line.split()
        date = line[0].split('-')
        date = datetime.date(int(date[0]), int(date[1]), int(date[2]))
        old_seq = line[6]
        new_seq = ''
        if len(line) >= 8 : 
            new_seq = line[7]
        elif line[3] == 'Insertion' :
            new_seq = old_seq
            old_seq = ''

        change_rec = SimpleNamespace()
        change_rec.data = date
        change_rec.chromosome = int(line[1])
        change_rec.release = line[2]
        change_rec.type = line[3]
        change_rec.start = int(line[4]) - 1 
        change_rec.stop = int(line[5]) - 1 
        change_rec.old_seq = old_seq
        change_rec.new_seq = new_seq
        res.append(change_rec)
    return res

def read_marshal(filename) :
    '''
    Read marshaled data from a file and return it.
    :param filename : name of file to read data from.
    '''
    fin = open(filename, 'rb')
    loaded = marshal.load(fin)
    fin.close()

    return loaded

def read_pickle(filename) :
    '''
    Read pickled data from a file and return it.
    :param filename : name of file to read data from.
    '''
    fin = open(filename, 'rb')
    loaded = cPickle.load(fin)
    fin.close()

    return loaded
