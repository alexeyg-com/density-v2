import numpy as np

import lib.tools

def make_coverage_profile(mrna, rib, N_mrna, N_rib, type = 'mean') :
    '''
    Create ribosome coverage profile from ribosome and mRNA coverage counts.
    :param mrna: mRNA coverage count.
    :param rib: ribosome coverage count.
    :param N_mrna: number of exon-mapped mRNA reads.
    :param N_rib: number of exon-mapped ribosome reads.
    :param type: type of profile to construct, can be 'mean' or 'profile'.
    '''
    mrna, rib = lib.tools.get_codon_array(mrna), lib.tools.get_codon_array(rib)
    mrna, rib = mrna / float(N_mrna), rib / (3.0 * float(N_rib))
    length_codons = mrna.shape[0]
    
    result = np.zeros((length_codons,))
    if type == 'mean' :
        mean = np.mean(mrna)
        for i in range(length_codons) :
            result[i] = rib[i] / mean
    elif type == 'profile' :
       for i in range(length_codons) :
           result[i] = rib[i] / mrna[i] if mrna[i] > 0 else float('nan')
    
    return result

def make_occupancy_profile(mrna, rib, N_mrna, N_rib, type = 'mean') :
    '''
    Create ribosome occupancy profile from ribosome and mRNA occupancy counts.
    :param mrna: mRNA occupancy count.
    :param rib: ribosome occupancy count.
    :param N_mrna: number of exon-mapped mRNA reads.
    :param N_rib: number of exon-mapped ribosome reads.
    :param type: type of profile to construct, can be 'mean' or 'profile'.
    '''
    mrna, rib = lib.tools.get_codon_array(mrna), lib.tools.get_codon_array(rib)
    mrna, rib = mrna / float(N_mrna), rib / float(N_rib)
    length_codons = mrna.shape[0]
    
    result = np.zeros((length_codons,))
    if type == 'mean' :
        mean = np.mean(mrna)
        for i in range(length_codons) :
            result[i] = rib[i] / mean
    elif type == 'profile' :
       for i in range(length_codons) :
           result[i] = rib[i] / mrna[i] if mrna[i] > 0 else float('nan')
    
    return result
