from SimpleNamespace import SimpleNamespace
from suds.client import Client

_SERVICE_URL = 'http://david.abcc.ncifcrf.gov/webservice/services/DAVIDWebService?wsdl'
_SERVICE_USER = 'a.gritsenko@tudelft.nl'

def enrich(ids, bg_ids = [], id_type = 'ENSEMBL_GENE_ID', categories = 'GOTERM_BP_FAT,GOTERM_CC_FAT,GOTERM_MF_FAT', threshold = 0.1, count = 2) :
    '''
    Uses DAVID for functional enrichment.
    :param ids: list of IDs to check for enrichment.
    :param bg_ids: list of background IDs.
    :param id_type: type of IDs.
    :param categories: a comma separated list of functional categories to check for enrichment.
    :param threshold: maximum p-value.
    :param count: minimal enrichment group size.
    '''
    have_bg = len(bg_ids) > 0
    client = Client(_SERVICE_URL)
    client.service.authenticate(_SERVICE_USER)
    ids = ','.join([id for id in ids])
    bg_ids = ','.join([id for id in bg_ids])
    mapped_ids = client.service.addList(ids, id_type, 'ids', 0)
    if have_bg :
        mapped_bg = client.service.addList(bg_ids, id_type, 'background', 1)
    else :
        mapped_bg = 1.0
    categories = client.service.setCategories(categories)
    print '[i] Mapped IDs: %.2f%% (enrichment), %.2f%% (background)' % (mapped_ids * 100, mapped_bg * 100)
    print '[i] Used categories: %s' % categories
    report = client.service.getChartReport(threshold, count)
    n_records = len(report)
    print '[i] Found enrichments: %d' % n_records
    results = []
    for row in report :
        result = SimpleNamespace()
        rowDict = dict(row)
        result.category_name = rowDict['categoryName']
        result.term_name = rowDict['termName']
        result.list_hits = rowDict['listHits']
        result.percent = rowDict['percent']
        result.score = rowDict['ease']
        result.genes = rowDict['geneIds']
        result.list_totals = rowDict['listTotals']
        result.pop_hits = rowDict['popHits']
        result.pop_totals = rowDict['popTotals']
        result.fold_enrichment = rowDict['foldEnrichment']
        result.bonferroni = rowDict['bonferroni']
        result.benjamini = rowDict['benjamini']
        result.afdr = rowDict['afdr']
        results.append(result)
    return results
