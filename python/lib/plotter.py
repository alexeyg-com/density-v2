'''
Functions for producing various plots. In most cases the names are self-explanatory.
Uses matplotlib and pylab for plotting.

Alexey Gritsenko
20-06-2013
'''

import pkg_resources
pkg_resources.require("matplotlib") 

import matplotlib
import numpy as np
import pylab
import matplotlib.pyplot as plot
import matplotlib.colors
import pandas
import math
import scipy.stats

import tools
import simulator
from SimpleNamespace import SimpleNamespace

def plot_replicate_correlation_matrix(mrna_replicates, rib_replicates, names, only_nonzero = False, type = 'mean', n_bins = 14) :
    '''
    Calculate and plot profile correlation histograms for all pairs of replicates.
    :param mrna_replicates : a list of mrrna profiles for which the correlation has to be computed.
    :param rib_replicates : a list of ribosome profiles for which the correlation has to be computed.
    :param names : a list of replicate names.
    :param only_nonzero : a boolean flag determining whether only nonzero profile positions should be correlated.
    :param type : type of normalization to use. Can be mean or profile.
    :param n_bins : number of histogram bins to be used in plotting.
    '''
    n_replicates = len(mrna_replicates)
    profiles = np.zeros((n_replicates,), dtype = np.object)
    
    for i in range(n_replicates) :
        print '[i] Creating profiles for replicate %d' % (i + 1)
        profiles[i] = tools.build_coverage_profiles(mrna_replicates[i], rib_replicates[i], n_mrna = 1.0, n_rib = 1.0, type = type)
    
    matrix = np.zeros((n_replicates, n_replicates), dtype = np.object)
    for i in range(n_replicates - 1) :
        for j in range(i + 1, n_replicates) :
            print '[i] Correlating replicates %d and %d' % (i + 1, j + 1)
            corr = tools.correlate_profiles(profiles[i], profiles[j], only_nonzero = only_nonzero)
            matrix[i][j] = corr
            matrix[j][i] = corr
    
    f, ax = plot.subplots(n_replicates - 1, n_replicates - 1)
    for i in range(n_replicates) :
        for j in range(n_replicates) :
            if i < j :
                cur = ax[i][j - 1]
                corr = matrix[i][j]
                corr = corr[1]
                #corr = np.random.rand(1000)
                n, bins, patches = cur.hist(corr, n_bins, normed = True, histtype = 'stepfilled')
                pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
                median = np.median(corr)
                cur.plot([median, median], [0, 10], 'r--', lw = 1.5)
                cur.text(median - 0.13, 0.9, 'Median $\\tilde{r}=%.2f$' % median, va = 'center', ha = 'left', rotation = 'vertical', fontsize = 16)
                cur.set_xlim(-0.5, 1.1)
                cur.set_ylim(0, 2.3)
                cur.set_yticklabels([])
                if i != j - 1 :
                    cur.set_xticklabels([])
                if i == j - 1 :
                    cur.set_xlabel('Pearson $r$', fontsize = 16)
                    cur.set_ylabel('Fraction of genes', fontsize = 16)
                if i == 0 :
                    cur.set_title(names[j], fontsize = 16, fontweight = 'bold')
                if i == j - 1 :
                    pos = cur.get_position()
                    x = pos.x0 - 0.04
                    y = (pos.y0 + pos.y1) / 2.0
                    f.text(x, y, names[i], fontsize = 16, fontweight = 'bold', va = 'center', rotation = 'vertical')
            elif i > j  and i < n_replicates - 1 :
                cur = ax[i][j]
                cur.set_axis_off()
    #f.tight_layout()
    f.set_size_inches(16, 16, forward = True)
    pylab.show()

def plot_alignment_statistics(alignments) :
    '''
    Plot read length, multiplicity and mismatch histograms based on alignments.
    :param alignments : alignments to obtain read lengths form.
    '''
    lengths, multiplicity, mismatches = [], [], []
    for group in alignments :
        for alg in group :
            length = alg.end - alg.begin
            lengths.append(length)
        multiplicity.append(len(group))
        mismatches.append(group[0].mismatches)
    lengths = np.array(lengths)
    multiplycity = np.array(multiplicity)
    mismatches = np.array(mismatches)
    min_length, max_length = np.min(lengths), np.max(lengths)
    n_bins = max_length - min_length + 1
    f, ax = plot.subplots(1, 3)
    n, bins, patches = ax[0].hist(lengths, range(min_length, max_length + 2), normed = True)
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax[0].set_xlabel('Read length', fontsize = 16)
    ax[0].set_ylabel('Fraction of reads', fontsize = 16)
    min_multiplicity, max_multiplicity = np.min(multiplicity), np.max(multiplicity)
    n_bins = max_multiplicity - min_multiplicity + 1
    n, bins, patches = ax[1].hist(multiplicity, range(min_multiplicity, max_multiplicity + 2), normed = True)
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax[1].set_xlabel('Alignment multiplicity', fontsize = 16)
    min_mismatches, max_mismatches = np.min(mismatches), np.max(mismatches)
    n_bins = max_mismatches - min_mismatches + 1
    n, bins, patches = ax[2].hist(mismatches, range(min_mismatches, max_mismatches + 2), normed = True)
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax[2].set_xlabel('Alignment mismatches', fontsize = 16)
    f.tight_layout()
    plot.show()

def _alignment_cmp(a, b) :
    '''
    A function for comparing two alignments.
    :param a : alignment A.
    :param b : alignment B.
    '''
    if a.reference == b.reference :
        if a.begin < b.begin :
            return -1
        elif a.begin == b.begin :
            if a.end < b.end :
                return -1
            else :
                return +1
        else :
            return +1
    elif a.reference < b.reference :
        return -1
    else :
        return +1

def _alignment_feature_cmp(alg, feat) :
    '''
    Compare alignment and feature.
    :param alg : alignment to compare.
    :param feat : feature to compare.
    '''
    if alg.reference == feat.reference :
        if alg.begin < feat.start :
            return -1
        elif alg.begin == feat.start :
            if alg.end < feat.stop :
                return -1
            else :
                return +1
        else :
            return +1
    elif alg.reference < feat.reference :
        return -1
    else :
        return +1

def _alignment_position_cmp(alg, position, reference) :
    '''
    Compare alignment and a position.
    :param alg : alignment to compare.
    :param feat : feature to compare.
    '''
    if alg.reference == reference :
        if alg.begin < position :
            return -1
        elif alg.begin == position :
            if alg.end < position + 1 :
                return -1
            else :
                return +1
        else :
            return +1
    elif alg.reference < reference :
        return -1
    else :
        return +1

def _locate_feature(alignments, feat) :
    left, right = 0, len(alignments) - 1
    while right - left > 1 :
        mid = (left + right) / 2
        cmp = _alignment_feature_cmp(alignments[mid], feat)
        if cmp < 0 :
            left = mid
        elif cmp > 0 :
            right = mid
    return left + 1

def _locate_position(alignments, position, reference) :
    left, right = 0, len(alignments) - 1
    while right - left > 1 :
        mid = (left + right) / 2
        cmp = _alignment_position_cmp(alignments[mid], position, reference)
        #print '[%d %s - %d %s] - %d %s - %d %s' % (alignments[left].begin, alignments[left].reference, alignments[right].begin, alignments[right].reference, alignments[mid].begin, alignments[mid].reference, position, reference)
        if cmp < 0 :
            left = mid
        elif cmp > 0 :
            right = mid
    return left + 1

def plot_alignment_strandedness(alignments, features, n_bins = 100) :
    '''
    For each gene count the number of reads with matching alignment and the number of reads with mismatching alignment and plot the results.
    :param alignments : alignments to obtain counts from.
    :param features : gene GFF features.
    '''
    n_alignments = len(alignments)
    alignments_sorted = []
    for group in alignments :
        for alg in group :
            alignments_sorted.append(alg)
    alignments_sorted.sort(cmp = _alignment_cmp)
    alignments = alignments_sorted

    count_match, count_mismatch = {}, {}
    for feat in features :
        if not feat.type == 'CDS' :
            continue
        print 'Feature: %s' % feat.name
        start = _locate_feature(alignments, feat)
        for i in range(start, n_alignments) :
            alg = alignments[i]
            if alg.reference != feat.reference or alg.end > feat.stop :
                break
            #print 'Alg: %s %d - %d - Feat: %s %d - %d' % (alg.reference, alg.begin, alg.end, feat.reference, feat.start, feat.stop)
        #if (feat.start <= alg.begin and alg.begin < feat.stop) or (feat.start < alg.end and alg.end <= feat.stop) :
            if not count_match.has_key(feat.name) :
                count_match[feat.name] = 0
                count_mismatch[feat.name] = 0
            is_feat_reverse = feat.strand == '-'
            if alg.is_reverse == is_feat_reverse :
                count_match[feat.name] = count_match[feat.name] + 1
            else :
                count_mismatch[feat.name] = count_mismatch[feat.name] + 1
    ratios = []
    for gene in count_match.keys() :
        match, mismatch = count_match[gene], count_mismatch[gene]
        ratio = (match + 1) / float(mismatch + 1)
        ratios.append(ratio)
    ratios = np.log2(ratios)
    f, ax = plot.subplots(1, 1)
    n, bins, patches = ax.hist(ratios, n_bins, normed = True)
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax.set_xlabel('Ratio of read strandedness : log2(match + 1 / mismatch + 1)', fontsize = 16)
    ax.set_ylabel('Fraction of genes', fontsize = 16)
    f.tight_layout()
    plot.show()
    return ratios

def plot_orf_offsets(alignments, features, feature_offset = 100, filename_format = None, do_plot = True) :
    '''
    Plot offset in reads to reach ATG codons and stop codons per read length.
    :param alignments : alignments to obtain offsets from.
    :param features : gene GFF features.
    :param feature_offset : nucleotide offset for selecting reads around the CDS start/stop.
    :param filename_format : format of the filenames to save figures.
    :param do_plot : whether the data should be plotted (Default - True).
    '''
    n_alignments = len(alignments)
    alignments_sorted = []
    for group in alignments :
        for alg in group :
            alignments_sorted.append(alg)
    alignments_sorted.sort(cmp = _alignment_cmp)
    alignments = alignments_sorted
    
    offset_atg, offset_stop = {}, {}
    for feat in features :
        if not feat.type == 'CDS' :
            continue
        print 'Feature: %s' % feat.name
        #print '%d and %d (offset %d)' % (feat.start, feat.start - offset, offset)
        is_feature_reverse = feat.strand == '-'
        _start = _locate_position(alignments, feat.start - feature_offset, feat.reference)
        _stop = _locate_position(alignments, feat.stop - feature_offset, feat.reference)
        #_start, _stop = 0, 0
        for i in range(_start, n_alignments) :
            alg = alignments[i]
            length = alg.end - alg.begin
            if not offset_atg.has_key(length) :
                offset_atg[length] = []
            if not offset_stop.has_key(length) :
                offset_stop[length] = []
            
            if alg.reference != feat.reference or alg.begin > feat.start :
                break
            if alg.begin <= feat.start and feat.start + 2 < alg.end :
                if not alg.is_reverse and not is_feature_reverse : # Getting position 0 most of the times - bullshit
                    offset = feat.start - alg.begin
                    offset_atg[length].append(offset)
                    #print 'Forward %d' % offset
                elif alg.is_reverse and is_feature_reverse : # Has the right peaks it seems, but also an additional distribution that is completely off (17-29) and more abundant! Something going very wrong here.
                    offset = alg.end - feat.start - 3
                    offset_stop[length].append(offset)

        for i in range(_stop, n_alignments) :
            alg = alignments[i]
            length = alg.end - alg.begin
            if not offset_atg.has_key(length) :
                offset_atg[length] = []
            if not offset_stop.has_key(length) :
                offset_stop[length] = []
            
            if alg.reference != feat.reference or alg.begin > feat.stop :
                break
            if alg.begin <= feat.stop - 3 and feat.stop <= alg.end :
                if not alg.is_reverse and not is_feature_reverse : # Gives no results at all, why?
                    offset = feat.stop - alg.begin - 3
                    offset_stop[length].append(offset)
                    #print 'Reverse %d' % offset
                elif alg.is_reverse and is_feature_reverse : # Seems ok, but strange peaks at 6-8, 10-12, 21, and 27-29 | strange...
                    offset = alg.end - feat.stop
                    offset_atg[length].append(offset)
    
    lengths = offset_atg.keys()
    min_length, max_length = np.min(lengths), np.max(lengths)
    n_lengths = len(lengths)
    width = 0.35
    for i in range(n_lengths) :
        length = lengths[i]
        f, ax = plot.subplots(2, 1)
        ind = np.array(range(length)) * width + width

        ax[0].set_xticks(ind + width / 2.0)
        ax[1].set_xticks(ind + width / 2.0)
        ax[0].set_xticklabels(range(length))
        ax[1].set_xticklabels(range(length))
        ax[0].set_xlim([0, np.max(ind) + 2 * width])
        ax[1].set_xlim([0, np.max(ind) + 2 * width])
        
        ax[0].set_title('Start codon', fontsize = 16, fontweight = 'bold')
        ax[1].set_title('Stop codon', fontsize = 16, fontweight = 'bold')
        ax[1].set_xlabel('Offset', fontsize = 16)
        f.text(0.01, 0.5, 'Frac. of alignments (length %d)' % length, va = 'center', rotation = 'vertical', fontsize = 16)
        
        values = np.array(offset_atg[length])
        values_dict = {}
        for j in range(length) :
            values_dict[j] = 0
        for val in values :
            values_dict[val] = values_dict[val] + 1
        grouped_values = np.zeros((length,))
        for j in range(length) :
            grouped_values[j] = values_dict[j]
        grouped_values = grouped_values / float(np.sum(grouped_values))
        grouped_values[np.isnan(grouped_values)] = 0
        max_atg = np.max(grouped_values)
        ax[0].bar(ind, grouped_values, width, color = 'b', alpha = 0.45)
        
        values = np.array(offset_stop[length])
        values_dict = {}
        for j in range(length) :
            values_dict[j] = 0
        for val in values :
            values_dict[val] = values_dict[val] + 1
        grouped_values = np.zeros((length,))
        for j in range(length) :
            grouped_values[j] = values_dict[j]
        grouped_values = grouped_values / float(np.sum(grouped_values))
        grouped_values[np.isnan(grouped_values)] = 0
        max_stop = np.max(grouped_values)
        ax[1].bar(ind, grouped_values, width, color = 'b', alpha = 0.45)
        
        ax[0].set_ylim([0, 1.1 * max(max_atg, max_stop)])
        ax[1].set_ylim([0, 1.1 * max(max_atg, max_stop)])
        f.tight_layout()
        f.set_size_inches(11, 5, forward = True)
        if do_plot :
            if filename_format is None :
                plot.show()
            else :
                f.savefig(filename_format % length)
    return offset_atg, offset_stop

def plot_codon_density(mrna, rib, gene, marker_size = 4) :
    '''
    Plot mRNA and ribosome densities on top of each other.
    :param mrna: mRNA alignment list.
    :param rib: ribosome alignment list.
    :param string gene: name of the gene to plot densities for.
    :param marker_size: size of the marker to be used for plotting densities (densities are plotted as scatter plots for those codons, where measurements are available)
    '''
    mrna_array_codon = tools.get_cds_array(mrna[gene])
    rib_array_codon = tools.get_cds_array(rib[gene])
    codon_len = len(mrna_array_codon)
   
    x = np.array(range(1, codon_len + 1))
    
    fig = pylab.gcf()
    fig.suptitle('Gene: %s' % gene, fontsize = 14)
    pylab.subplot(211)
    #pylab.plot(x, rib_array_codon)
    pylab.scatter(x[rib_array_codon != 0], rib_array_codon[rib_array_codon != 0], s=marker_size)
    pylab.xlim(0, codon_len + 1)
    pylab.xlabel('Codon')
    pylab.ylabel('Ribosome density')
    pylab.grid(True)

    pylab.subplot(212)
    #pylab.plot(x, mrna_array_codon)
    pylab.scatter(x[mrna_array_codon != 0], mrna_array_codon[mrna_array_codon != 0], s=marker_size)
    pylab.xlim(0, codon_len + 1)
    pylab.xlabel('Codon')
    pylab.ylabel('mRNA density')
    pylab.grid(True)
    pylab.show()

def plot_cummulative_read_count_distribution(mrna, rib, is_density = False) :
    '''
    Plot cummulative read count distribution for mRNA and ribosome reads.
    :param mrna: mRNA density dictionary.
    :param rib: ribosome density dictionary.
    :param boolean is_density: a flag defining whether read count or density distribution is being plotted.
    '''
    totals_mrna = [tools.get_cds_count(gene) for gene in mrna.values()]
    totals_rib = [tools.get_cds_count(gene) for gene in rib.values()]

    totals_mrna.sort(reverse=True)
    totals_rib.sort(reverse=True)

    count = len(totals_mrna)
    cumsum_mrna = np.cumsum(totals_mrna) / np.sum(totals_mrna, dtype=np.float128)
    cumsum_rib = np.cumsum(totals_rib) / np.sum(totals_rib, dtype=np.float128)
    
    handles = pylab.plot(range(1, count + 1), cumsum_mrna, range(1, count + 1), cumsum_rib)
    if not is_density :
        pylab.legend(handles, ['mRNA reads', 'Ribosome reads'], loc = 4)
    else :
        pylab.legend(handles, ['mRNA density', 'Ribosme density'], loc = 4)
    pylab.ylim(0, 1.01)
    pylab.xlim(0, count + 1)
    pylab.xlabel('Gene')
    if not is_density :
        pylab.title('# mRNA reads = %s, # Ribosome reads = %s' % (format(np.sum(totals_mrna), ',.0f'), format(np.sum(totals_rib), ',.0f')))
        pylab.ylabel('Fraction of reads')
    else :
        pylab.title('Total mRNA density = %s, Total ribosome density = %s' % (format(np.sum(totals_mrna), ',.0f'), format(np.sum(totals_rib), ',.0f')))
        pylab.ylabel('Fraction of density')
    pylab.grid(True)
    pylab.show()

def _get_orf_count(arrays) :
    '''
    Calculate ORF count array.
    :param arrays : a dictionary of arrays to compute the ORF counts for.
    '''
    genes = arrays.keys()
    count = np.zeros((4,))
    for gene in genes :
        array = arrays[gene].mrna.array
        length = len(array)
        for i in range(length) :
            cnt = array[i]
            pos = (i - 1 + 3) % 3
            count[pos] += cnt
    count[3] = np.sum(count[:3])
    return count

def plot_orf_read_count_histogram(mrna, rib, bar_width = 0.35) :
    '''
    Plot ORF read count histogram for mRNA and ribosome reads.
    :param mrna: mRNA ORF read counts.
    :param rib: ribosome ORF read counts.
    :param bad_width : width of the histogram bar.
    '''
    mrna_count = _get_orf_count(mrna)
    rib_count = _get_orf_count(rib)
    x = np.array(range(3))
    mrna_count[:3] /= float(mrna_count[3])
    rib_count[:3] /= float(rib_count[3])
    pylab.hold(True)
    rib_handle = pylab.bar(x, rib_count[:3], bar_width, color='r', alpha = 0.45)
    mrna_handle = pylab.bar(x + bar_width, mrna_count[:3], bar_width, color='b', alpha = 0.45)
    pylab.legend((rib_handle, mrna_handle), ('Ribosome reads', 'mRNA reads'))
    pylab.xlim(0, 2 + 2 * bar_width)
    pylab.xticks(x + bar_width, ('0', '1', '2'))
    pylab.xlabel('ORF', fontsize = 16)
    pylab.ylabel('Fractions', fontsize = 16)
    pylab.title('# mRNA = %s, # Ribosome = %s' % (format(int(mrna_count[3]), ',d'), format(int(rib_count[3]), ',d')), fontsize = 18)
    pylab.show()

def plot_autocorrelation(mrna, rib, gene) :
    '''
    Plot autocorrelation plots for mRNA and ribosome density signals on top of each other.
    :param mrna: mRNA alignment list.
    :param rib: ribosome alignment list.
    :param string gene: name of the gene to plot densities for.
    '''
    mrna_array = tools.get_cds_array(mrna[gene])
    rib_array = tools.get_cds_array(rib[gene])

    fig, axes = pylab.subplots(nrows = 2, ncols = 1)
    pandas.tools.plotting.autocorrelation_plot(mrna_array, ax = axes[0])
    pandas.tools.plotting.autocorrelation_plot(rib_array, ax = axes[1])
    axes[0].set_ylim(-1, 1)
    axes[0].set_title('mRNA autocorrelation')
    axes[1].set_ylim(-1, 1)
    axes[1].set_title('Ribosome autocorrelation')
    fig = pylab.gcf()
    fig.suptitle('Gene: %s' % gene, fontsize =  14)

    pylab.show()

def plot_average_density(density, length_cutoff = 0, max_length_cutoff = float('Inf'), density_cutoff = 0.2, cds_only = False, normalize_by_mean = False, xmax = None, marker_size = 4) :
    '''
    Plot average density profile across the genes.
    :param density: density dictionary.
    :param length_cutoff: minimum gene length.
    :param max_length_cutoff: maximum gene length.
    :param density_cutoff: average density cutoff to use for plotting genes.
    :param cds_only: plot only for the CDS part of the array?
    :param normalize_by_mean: a flag defining whether density profiles should be normalized by the mean.
    :param xmax: x-axis limit.
    :param marker_size: size of the marker to use for plotting.
    '''
    expected_length = 0
    selected_genes = []
    for gene in density.keys() :
        gene_array = tools.get_cds_array(density[gene]) if cds_only else density[gene].mrna.array
        length = len(gene_array)
        average = np.sum(gene_array) / float(length)
        if average >= density_cutoff and length >= length_cutoff and length <= max_length_cutoff :
            expected_length = max(expected_length, length)
            selected_genes.append(gene)

    array = [0] * expected_length
    counts = [0] * expected_length

    for gene in selected_genes:
        gene_array = tools.get_cds_array(density[gene]) if cds_only else density[gene].mrna.array
        length = len(gene_array)
        mean = np.sum(gene_array) / float(length)
        for i in range(length) :
            if normalize_by_mean :
                array[i] += density[gene].mrna.array[i] / float(mean)
            else :
                array[i] += density[gene].mrna.array[i]
            counts[i] += 1
        
    array, counts = np.atleast_1d(array), np.atleast_1d(counts)
    array /= counts.astype(np.float)

    x = np.array(range(1, expected_length + 1))

    if xmax is None :
        xmax = expected_length + 1
    fig = pylab.gcf()
    pylab.subplot(211)
    pylab.scatter(x, array, s=marker_size)
    pylab.xlim(0, xmax)
    pylab.xlabel('Position')
    pylab.ylabel('Average density')
    pylab.grid(True)

    pylab.subplot(212)
    pylab.scatter(x, counts, s=marker_size)
    pylab.xlim(0, xmax)
    pylab.xlabel('Position')
    pylab.ylabel('Number of genes')
    pylab.grid(True)
    pylab.show()

def plot_simulation(solution, forest, gene = None, marker_size = 16, plot_data = True, log_space = False) :
    '''
    Plot simulation results.
    :param solution: a single solution or a dictionary of solutions (for gene group fitters).
    :param forest : forest that should be used for obtaining the records.
    :param gene: name of the gene to plot the simulation results for. If None, results are plotted for the first gene.
    :param marker_size: size of the marker to be used for plotting densities (densities are plotted as scatter plots for those codons, where measurements are available)
    :param plot_data: a boolean flag defining whether the data (scatter) should also be plotted.
    :param log_space: a boolean flag defining whether the data should be plotter in log space.
    '''
    fig = pylab.gcf()
    if gene is not None :
        record, initiation, simulation, fitness = forest[gene], solution.initiation[gene], solution.density[gene], solution.fitness[gene]
        scale = solution.scale if isinstance(solution.scale, float) else solution.scale[gene]
    else :
        if not isinstance(solution.initiation, dict) :
            record, initiation, simulation, fitness, scale = None, solution.initiation, solution.density, solution.fitness, solution.scale
        else :
            genes = solution.names
            gene = genes[0]
            record, initiation, simulation, fitness = forest[gene], solution.initiation[gene], solution.density[gene], solution.fitness[gene]
            scale = solution.scale if isinstance(solution.scale, float) else solution.scale[gene]
    
    initiation = simulator.RateFunction.sigmoid(initiation)

    if gene is not None :
        fig.suptitle('Gene: %s - Init: %.10f - Scale: %.3f - Fitness: %.5f' % (gene, initiation, scale, fitness), fontsize = 14)
    else :
        fig.suptitle('Init: %.3f - Scale: %.10f - Fitness: %.5f' % (initiation, scale, fitness), fontsize = 14)
   
    codon_len = len(simulation)
    x = np.array(range(1, codon_len + 1))
    if plot_data :
        pylab.hold(True)
        if record is not None:
            for node in record.tree :
                left, right, ratio_average_log2, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
                length = right - left + 1
                mean_occupancy = scale * np.sum(simulation[left:right + 1]) / float(length)
                mean_occupancy_log2 = math.log(mean_occupancy, 2)
                mean_occupancy_log = math.log(mean_occupancy)
                ratio_average = 2 ** ratio_average_log2
                ratio_average_log = math.log(ratio_average)

                if not log_space :
                    pylab.plot([left, right], [mean_occupancy, mean_occupancy], 'r', linewidth = 2)
                    pylab.plot([left, right], [ratio_average, ratio_average], 'g', linewidth = 2)
                    diff = mean_occupancy - ratio_average
                    print '[%3d, %3d] len = %3d %.2f -> %.2f (diff: %5.2f)' % (left, right, length, mean_occupancy, ratio_average, diff)
                else :    
                    pylab.plot([left, right], [mean_occupancy_log2, mean_occupancy_log2], 'r', linewidth = 2)
                    pylab.plot([left, right], [ratio_average_log2, ratio_average_log2], 'g', linewidth = 2)
                    diff = mean_occupancy_log2 - ratio_average_log2
                    diff_p = -abs(mean_occupancy_log - ratio_average_log)
                    p_value = scipy.stats.norm(0, sigma).cdf(diff_p) * 2
                    print '[%3d, %3d] len = %3d %.2f -> %.2f (diff: %5.2f, p %.5f)' % (left, right, length, mean_occupancy_log2, ratio_average_log2, diff, p_value)
    if log_space :
        pylab.plot(x, np.log2(scale * simulation))
    else :
        pylab.plot(x, scale * simulation)
    pylab.xlim(0, codon_len + 1)
    pylab.xlabel('Codon')
    pylab.ylabel('Occupancy')
    pylab.grid(True)

    pylab.show()
