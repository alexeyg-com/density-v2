import numpy as np
import tools
import zhang
import fitter
import math

from SimpleNamespace import SimpleNamespace

def evaluate_profiles(occupancy, forest, genes = None) :
    '''
    Evaluate profiles.
    :param occupancy: simulated occupancy for genes.
    :param forest: forest to be used for profile evaluation.
    :param genes: list of genes that should be evaluated.
    '''
    genes_occupancy = occupancy.keys()
    genes_forest = forest.keys()
    if genes is None :
        genes = tools.overlap_gene_lists(genes_occupancy, genes_forest)
    print '[i] Occupancy: %d' % len(genes_occupancy)
    print '[i] Forest: %d' % len(genes_forest)
    print '[i] Common: %d' % len(genes)

    result = SimpleNamespace()
    result.density = {}
    result.fitness = {}

    used_genes = []
    likelihood_ind = 0.0
    numerator, denominator = 0.0, 0.0
    skipped = 0
    for gene in genes :
        if occupancy[gene].shape[0] != forest[gene].length_codons :
            skipped += 1
            continue
        gene_numerator, gene_denominator = forest[gene].compute_scale(occupancy[gene])
        numerator += gene_numerator
        denominator += gene_denominator
        gene_scale = gene_numerator / gene_denominator
        likelihood_ind += forest[gene].evaluate(gene_scale, occupancy[gene])
    
    scale = numerator / denominator
    likelihood_global = 0.0
    for gene in genes :
        if occupancy[gene].shape[0] != forest[gene].length_codons :
            continue
        gene_fitness = forest[gene].evaluate(scale, occupancy[gene])
        likelihood_global += gene_fitness
        result.density[gene] = occupancy[gene]
        result.fitness[gene] = gene_fitness

    result.total_fitness = likelihood_global
    result.scale = math.exp(scale)

    print '[+] Individual: %.2f' % likelihood_ind
    print '[+] Global:     %.2f' % likelihood_global
    print '[i] Scale: %.5f' % scale
    print '[-] Skipped: %d' % skipped
    
    return result

def evaluate_profiles_cv(profiles, folds, forest) :
    n_folds = len(folds)
    fold_results = []
    scales = []
    for i in xrange(n_folds) :
        genes = []
        for j in xrange(n_folds) :
            if j != i :
                for g in folds[j] :
                    genes.append(g)
        coef = np.zeros((3,))
        for gene in genes :
            coef_gene = forest[gene].evaluate_scalefree(profiles[gene])
            coef += coef_gene
        scale = -coef[1] / (2 * coef[0])
        likelihood = coef[0] * scale ** 2 + coef[1] * scale + coef[2]
        fold_results.append(likelihood)
        scales.append(scale)
        print '[i] Fold %d, objective: %.5f' % (i + 1, likelihood)
        print '    [i] Scale: %.2f' % scale
    print '[+] CV objective: %.4f +/- %.4f' % (np.mean(fold_results), np.std(fold_results))
    print '[i] Scale: %.2f +/- %.2f' % (np.mean(scales), np.std(scales))
    return (np.mean(fold_results), np.std(fold_results))
        
def evaluate_tasep_single_scale_fold(vector, folds, fold, seed = 101317, client_url = None) :
    n_folds = len(folds)
    ndims = len(vector)
    genes = []
    for j in xrange(n_folds) :
        if j != fold :
            for g in folds[j] :
                genes.append(g)
    fit = fitter.SingleScaleGeneGroupFitter(genes, ndims = ndims, client_url = client_url)
    objective = -fit.get_objective(vector, srand = seed)
    print '[i] Fold %d, objective: %.5f' % (fold + 1, objective)

def evaluate_tasep_single_scale_cv(results, folds, seed = None, client_url = None) :
    n_folds = len(results)
    fold_results = []
    for i in xrange(n_folds) :
        ndims = len(results[i].vector)
        genes = []
        for j in xrange(n_folds) :
            if j != i :
                for g in folds[j] :
                    genes.append(g)
        fit = fitter.SingleScaleGeneGroupFitter(genes, ndims = ndims, client_url = client_url)
        vector = np.zeros((ndims + 1, ))
        vector[0] = math.log(results[i].proposed_scale)
        vector[1:] = results[i].vector[:]
        if seed is None :
            objective = -fit.get_objective(vector, srand = results[i].srand)
        else :
            objective = -fit.get_objective(vector, srand = seed)
        print '[i] Fold %d, objective: %.5f' % (i + 1, objective)
        fold_results.append(objective)
    print '[+] CV objective: %.4f +/- %.4f' % (np.mean(fold_results), np.std(fold_results))

def get_zhang_occupancies(forest) :
    '''
    Use the Zhang model to obtain codon occupancies for all genes in the forest.
    :param forest: the forest, for which occupancies should be computed.
    '''
    model = zhang.ZhangModel()
    results = {}
    genes = forest.keys()
    for gene in genes :
        occupancy = model.simulate(forest[gene])
        results[gene] = occupancy
    return results

def read_shah_output(mrna_filename, simulation_filename, total_time) :
    '''
    Process Shah model output into ribosome occupancy profiles.
    :param mrna_filename: name of the file containing gene and transcript descriptions.
    :param simulation_filename: name of the file containing simulation results.
    :param total_time: the total time passed since simulation start.
    '''
    fin = open(mrna_filename, 'rb')
    mrna_lines = fin.readlines()
    fin.close()
    fin = open(simulation_filename, 'rb')
    simulation_lines = fin.readlines()
    fin.close()
    mrna_lines = mrna_lines[1:]
    total_time = float(total_time)
    n_mrna, n_simulation = len(mrna_lines), len(simulation_lines)
    print '[i] Simulation: %d' % n_simulation
    print '[i] mRNA: %d' % n_mrna
    print '[i] Time: %.2f' % total_time

    results = {}
    for i in range(n_mrna) :
        mrna_line = mrna_lines[i].split()
        simulation_line = simulation_lines[i].split()
        simulation_line.append(0)
        name = mrna_line[0]
        copies = int(mrna_line[2])
        counts = np.array([int(x) for x in simulation_line])
        profile = counts / (total_time * copies)
        results[name] = profile
    return results
