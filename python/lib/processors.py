'''
Routines for processing data.

Alexey Gritsenko
25-04-2014
'''

from SimpleNamespace import *
import Bio.Seq
import Bio.SeqIO
from progressbar import ProgressBar
import numpy as np
import copy
import sys
import tools

def make_feature_record(seq, array, array_overlapping, array_ambiguous, feature_length, overlapping, ambiguous, total) :
    '''
    Creates a feature record for internal use.
    TODO : add a list of parameters.
    '''
    rec = SimpleNamespace()
    rec.seq = seq
    rec.array = array
    rec.array_overlapping = array_overlapping
    rec.array_ambiguous = array_ambiguous
    rec.feature_length = feature_length
    rec.overlapping = overlapping
    rec.ambiguous = ambiguous
    rec.total = total
    
    return rec

def _changes_cmp(a, b) :
    '''
    A function for comparing to change records.
    :param a : first change record.
    :param b : second change record.
    '''
    a = a.release.split('-')
    b = b.release.split('-')
    a = int(a[0])
    b = int(b[0])

    return a - b

def _group_changes(changes) :
    '''
    Group a list of changes together based on their release.
    :param changes : a list of changes to be grouped.
    '''
    changes = copy.deepcopy(changes)
    changes.sort(_changes_cmp)
    groups = []
    i = 0
    j = 0
    while i < len(changes) :
        while j < len(changes) and changes[i].release == changes[j].release :
            j = j + 1
        groups.append(changes[i:j])
        i = j

    return groups

def _update_alignment_coordinates_ingolia(alg, references, adapter) :
    '''
    Update read alignment coordinates for the Ingolia dataset.
    :param alg : alignment to be updated.
    :param references : a dictionary of references.
    :param adapter : adapter sequence.
    '''
    adapter_length = len(adapter)
    read_seq = alg.seq.upper()
    ref_name = alg.reference
    if not references.has_key(ref_name) :
        return
    ref_seq = references[ref_name].seq
    ref_length = len(ref_seq)
    read_length = len(read_seq)
    adapter = copy.deepcopy(adapter)

    mismatches = []
    mismatches_reference = []
    mismatches_adapter = []
    if not alg.is_reverse :
        prefix_mismatches = 0
        for i in range(read_length) :
            if alg.begin + i >= ref_length :
                break
            if read_seq[i] != ref_seq[alg.begin + i] :
                prefix_mismatches += 1
            adapter_mismatches = 0
            for j in range(i + 1, read_length) :
                if read_seq[j] != adapter[j - i - 1] :
                    adapter_mismatches += 1
            mismatches.append(prefix_mismatches + adapter_mismatches)
            mismatches_reference.append(prefix_mismatches)
            mismatches_adapter.append(adapter_mismatches)
    else :
        adapter = Bio.Seq.Seq(adapter).reverse_complement().tostring()
        #print adapter
        #print read_seq
        #print ref_seq[alg.begin:alg.end]
        prefix_mismatches = 0
        for i in range(read_length) :
            if alg.end - 1 - i >= ref_length :
                break
            if read_seq[read_length - 1 - i] != ref_seq[alg.end - 1 - i] :
                prefix_mismatches += 1
            adapter_mismatches = 0
            for j in range(read_length - 1 - i) :
                if read_seq[j] != adapter[adapter_length - 1 - j] :
                    adapter_mismatches += 1
            mismatches.append(prefix_mismatches + adapter_mismatches)
            mismatches_reference.append(prefix_mismatches)
            mismatches_adapter.append(adapter_mismatches)
       #mismatches.reverse()
       #mismatches_reference.reverse()
       #mismatches_adapter.reverse()
    min_mismatches = np.min(mismatches)
    min_where = np.where(mismatches == min_mismatches)
    min_where = min_where[0]
    n_min = len(min_where)
    prefix = min_where[n_min - 1]
    alg.mismatches = min_mismatches
    alg.prefix_mismatches = mismatches_reference[prefix]
    alg.adapter_mismatches = mismatches_adapter[prefix]
    min_length = prefix
    for i in range(n_min - 1, 0, -1) :
        #print '%d : %d and %d' % (i, min_where[i - 1], min_where[i] - 1)
        if min_where[i - 1] != min_where[i] - 1 :
            break
        min_length = min_where[i - 1]
    alg.ambiguity_length = prefix - min_length
    if not alg.is_reverse :
        #print 'Forward'
        alg.end = alg.begin + prefix + 1
        alignment_length = alg.end - alg.begin
        alg.seq = read_seq[:alignment_length] + read_seq[alignment_length:].lower()
    else :
        #print 'Reverse'
        alg.begin = alg.end - prefix - 1
        alignment_length = alg.end - alg.begin
        alg.seq = read_seq[:-alignment_length].lower() + read_seq[-alignment_length:]
    #print 'Prefix : %d' % prefix
    #print alg.seq
    #print mismatches
    #print min_where
    #print '------'
    alg.maximum_length = alg.end - alg.begin

def apply_changes_to_ingolia_alignment(seqs, changes, groups, start_release, adapter = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA') :
    '''
    Update alignments according to a list of changes.
    Suitable only for the Ingolia dataset.
    :param seqs : reference sequences.
    :param changes : a list of changes to be applied.
    :param groups : a list of alignment groups to be updated.
    :param start_release : number of the release to start updating from.
    :param adapter : adapter (linker) sequences to match the tail of the read against.
    '''
    changes = _group_changes(changes)
    cnt = 0
    with ProgressBar(maxval = len(groups)) as progress :
        for group in groups :
            for alignment in group :
                diff_start, diff_stop = 0, 0
                alignment_reference = tools.chromosome2int[alignment.reference]
                start_pos, stop_pos = alignment.begin, alignment.end
                for change_group in changes :
                    for change in change_group :
                        change_release = change.release.split('-')
                        change_release = int(change_release[0])
                        if change_release < start_release :
                            continue
                        if alignment_reference == change.chromosome :
                            if change.type == 'Insertion' :
                                if change.start <= start_pos :
                                    diff_start = diff_start + len(change.new_seq)
                                if change.start < stop_pos :
                                    diff_stop = diff_stop + len(change.new_seq)
                            elif change.type == 'Deletion' :
                                if change.start <= start_pos :
                                    diff_start = diff_start + -len(change.old_seq)
                                if change.start < stop_pos :
                                    diff_stop = diff_stop + -len(change.old_seq)
                            elif change.type == 'Substitution' :
                                if change.start <= start_pos :
                                    diff_start = diff_start - len(change.old_seq) + len(change.new_seq)
                                if change.start < stop_pos :
                                    diff_stop = diff_stop - len(change.old_seq) + len(change.new_seq)

                #if diff_start != 0 or diff_stop != 0 :
                #    continue
                alignment.begin += diff_start
                alignment.end += diff_stop
                #print 'Diff: %d %d' % (diff_start, diff_stop)
                _update_alignment_coordinates_ingolia(alignment, seqs, adapter)
            cnt += 1
            progress.update(cnt)

        return groups

def trim_fastq(input_filename, output_filename, trim_length = 21) :
    '''
    Trim FASTQ reads in a file.
    :param input_filename : name of the file containing the FASTQ reads to be trimmed.
    :param output_filename : name of the file to output trimmed reads to.
    :param trim_length : length to which all reads should be trimmed to.
    '''
    fin = open(input_filename, 'rb')
    fout = open(output_filename, 'w')
    for record in Bio.SeqIO.parse(fin, 'fastq') :
        Bio.SeqIO.write(record[:trim_length], fout, 'fastq')
    fin.close()
    fout.close()

def group_gff_records_by_name(records) :
    '''
    Group records by name.
    :param records : a list of GFF records to be combined into groups (based on the gene name).
    '''
    genes = {}
    for record in records :
        if record.type == 'CDS' or record.type == 'five_prime_UTR' or record.type == 'three_prime_UTR' :
            name = record.name
            if '_' in name :
                parts = name.split('_')
                name = parts[0]
            if genes.has_key(name) :
                genes[name].features.append(record)
            else :
                gene_record = SimpleNamespace()
                gene_record.features = [record]
                genes[name] = gene_record
    return genes

def combine_gff_records_into_genes(records, references, extension = 0) :
    '''
    Creates a single joint record for 5- and 3-prime UTRs and CDS.
    :param records : a dictionary of GFF record groups.
    :param references : dictionary of reference sequences.
    :param extension : extend genes with so many nucleotides on both sides.
    '''
    constructed = {}
    genes = records.keys()
    for gene in genes :
        features = records[gene].features
        features.sort(feature_cmp)
        start = float('Inf')
        stop = -float('Inf')
        cds_feature = None
        for feature in features :
            start, stop = min(start, feature.start), max(stop, feature.stop)
            if feature.type == 'CDS' :
                cds_feature = feature
        if cds_feature is None :
            continue
        ref_name = cds_feature.reference
        if not references.has_key(ref_name) :
            ref_length = sys.maxint
        else :
            ref_seq = references[ref_name].seq
            ref_length = len(ref_seq)
        start = max(0, start - extension)
        stop = min(ref_length, stop + extension)
        construct = SimpleNamespace()
        construct.name = gene
        construct.description = cds_feature.description
        construct.reference = ref_name
        construct.strand = cds_feature.strand
        construct.type = 'gene'
        construct.phase = '.'
        construct.source = ''
        construct.start = start
        construct.stop = stop
        constructed[gene] = construct
    return constructed

def extract_sequences_for_gene_records(records, references) :
    '''
    Extract sequences from GFF record groups.
    :param records : GFF gene records.
    :param references : a dictionary of genomic references.
    '''
    missing_reference = set()
    fasta_records = []
    genes = records.keys()
    for gene in genes :
        record = records[gene]
        ref_name = record.reference
        if not references.has_key(ref_name) :
            missing_reference.add(ref_name)
            continue
        ref_seq = references[ref_name].seq
        gene_seq = ref_seq[record.start : record.stop]
        if record.strand == '-' :
            gene_seq = Bio.Seq.Seq(gene_seq).reverse_complement().tostring()
        gene_fasta_record = Bio.SeqRecord.SeqRecord(Bio.Seq.Seq(gene_seq, Bio.Alphabet.IUPAC.unambiguous_dna), id = gene, name = gene, description = '')
        fasta_records.append(gene_fasta_record)
    print 'Missing references: %s' % (', '.join(missing_reference))
    return fasta_records

def unify_gff_record_groups(genes) :
    '''
    Unify GFF record groups.
    :param genes : a dictionary of GFF record groups to be sorted.
    '''
    for gene in genes.values() :
        gene.features.sort(feature_cmp)
    return genes

def convert_references_to_fasta(references) :
    '''
    Convert references to FASTA sequences.
    :param references : a dictionary of genomic references.
    '''
    names = references.keys()
    fasta = []
    for ref in names :
        entry = references[ref]
        record = Bio.SeqRecord.SeqRecord(Bio.Seq.Seq(entry.seq, Bio.Alphabet.IUPAC.unambiguous_dna), id = ref, name = ref, description = entry.description)
        fasta.append(record)
    return fasta

def restore_original_alignment(alignemnt, gene_records) :
    '''
    Restore read alignments to genes to genomic coordinates.
    :param alignment : a list of alignments to be restored.
    :param gene_records : gene records used to recompute the coordinates.
    '''
    restored_alignments = []
    for alg in alignemnt :
        restored = copy.copy(alg)
        aligned_to = gene_records[alg.reference]
        restored.reference = aligned_to.reference
        restored.was_reverse = restored.is_reverse
        restored.was_reference_reverse = aligned_to.strand == '-'
        if aligned_to.strand == '-' :
            aligned_to_len = aligned_to.stop - aligned_to.start
            restored.begin, restored.end = aligned_to_len - restored.end, aligned_to_len - restored.begin
            restored.is_reverse = not restored.is_reverse
            restored.seq = Bio.Seq.Seq(restored.seq).reverse_complement().tostring()
        
        restored.begin += aligned_to.start
        restored.end += aligned_to.start
        restored_alignments.append(restored)

    return restored_alignments

def extend_alignment(alignments, reads, references, adapter_original) :
    '''
    Extend trimmed alignments to the full length.
    :param alignments : list of alignments to be extended.
    :param reads : a list of reads to be matched to alignments.
    :param references : a dictionary of genomic reference sequences.
    :param adapter_original : adapter sequence.
    '''
    mismatch_histogram = {}
    adapter_length = len(adapter_original)
    total = 0
    for alg in alignments :
        read_name = alg.name
        read_seq = reads[read_name].seq.tostring()
        ref_name = alg.reference
        ref_seq = references[ref_name].seq
        read_length = len(read_seq)
        ref_length = len(ref_seq)
        start_length = len(alg.seq)
        adapter = adapter_original[:]
        
        mismatches = float('Inf')
        mismatches_p, mismatches_a = 0, 0
        prefix = 0
        if not alg.is_reverse :
            prefix_mismatches = 0
            for i in range(start_length - 1) :
                if read_seq[i] != ref_seq[alg.begin + i] :
                    prefix_mismatches += 1
            for i in range(start_length, read_length) :
                if alg.begin + i >=  ref_length :
                    break
                if read_seq[i] != ref_seq[alg.begin + i] :
                    prefix_mismatches += 1
                adapter_mismatches = 0
                for j in range(i + 1, read_length) :
                    if read_seq[j] != adapter[j - i - 1] :
                        adapter_mismatches += 1
                if mismatches > prefix_mismatches + adapter_mismatches :
                    mismatches = prefix_mismatches + adapter_mismatches
                    mismatches_p, mismatches_a = prefix_mismatches, adapter_mismatches
                    prefix = i
        else :
            read_seq = Bio.Seq.Seq(read_seq).reverse_complement().tostring()
            adapter = Bio.Seq.Seq(adapter).reverse_complement().tostring()
            start = read_length - start_length
            prefix_mismatches = 0
            for i in range(start + 1, start + start_length) :
                if read_seq[i] != ref_seq[alg.begin + i - start] :
                    prefix_mismatches += 1
            for i in range(start, -1, -1) :
                if alg.begin + i - start >=  ref_length :
                    break
                if read_seq[i] != ref_seq[alg.begin + i - start] :
                    prefix_mismatches += 1
                adapter_mismatches = 0
                for j in range(i) :
                    if read_seq[j] != adapter[adapter_length - i + j] :
                        adapter_mismatches += 1
                if mismatches > prefix_mismatches + adapter_mismatches :
                    mismatches = prefix_mismatches + adapter_mismatches
                    mismatches_p, mismatches_a = prefix_mismatches, adapter_mismatches
                    prefix = i
        
        if not mismatch_histogram.has_key(mismatches) : 
            mismatch_histogram[mismatches] = 1
        else :
            mismatch_histogram[mismatches] = mismatch_histogram[mismatches] + 1
        
        alg.mismatches = mismatches
        alg.mismatches_reference = mismatches_p
        alg.mismatches_adapter = mismatches_a
        if not alg.is_reverse :
            alg.end = alg.begin + prefix + 1
            alg.seq = read_seq[:prefix + 1] + read_seq[prefix + 1:].lower()
        else :
            alg.begin = alg.end - (read_length - prefix)
            alg.seq = read_seq[:prefix].lower() + read_seq[prefix:]

        total += 1

    return alignments

def clean_alignment_groups(groups) :
    '''
    Clean alignment groups (i.e. remove alignment with higher number of mismatches than the minimum).
    :param groups : list of groups to be cleaned.
    '''
    cleaned_groups = []
    for group in groups :
        mismatches = np.min([a.mismatches for a in group])
        cleaned_group = [a for a in group if a.mismatches == mismatches]
        cleaned_groups.append(cleaned_group)
    return cleaned_groups

def group_multiple_alignments(alignments) :
    '''
    Group multiple alignments.
    :param alignments : list of alignments to be grouped together.
    '''
    groupped_alignments = []
    group = []
    current_name = ''
    for alg in alignments :
        read_name = alg.name
        if read_name == current_name and current_name != '' :
            group.append(alg)
        else :
            if len(group) > 0 :
                mismatches = np.min([a.mismatches for a in group])
                # Clean up alignment first!
                group = [a for a in group if a.mismatches == mismatches]
                n_alg = len(group)
                selected = np.zeros((n_alg,), dtype = np.bool)
                group_cleaned = []
                for i in range(n_alg) :
                    if not selected[i] :
                        group_cleaned.append(group[i])
                        selected[i] = True
                        for j in range(i + 1, n_alg) :
                            if not selected[j] and group[i].begin == group[j].begin and group[i].end == group[j].end and group[i].reference == group[j].reference and group[i].is_reverse == group[j].is_reverse :
                                selected[j] = True
                groupped_alignments.append(group_cleaned)
            current_name = read_name
            group = [alg]
    if len(group) > 0 :
        groupped_alignments.append(group)
    return groupped_alignments

def check_alignment(alignments, references, adapter_original) :
    '''
    Check alignments against reference.
    :param alignments : alignments list.
    :param references : dictionary of reference sequences.
    :param adapter_original : adapter sequence.
    '''
    mismatch_histogram = {}
    chromosome_histogram = {}
    orientation_histogram = {}
    adapter_length = len(adapter_original)
    total = 0
    real_alignments = []
    if isinstance(alignments[0], list) :
        real_alignments = [alg for group in alignments for alg in group]
    else :
        real_alignments = alignments[:]
    for alg in real_alignments :
        read_seq = alg.seq
        ref_name = alg.reference
        if not references.has_key(ref_name) :
            continue
        ref_seq = references[ref_name].seq
        read_length = len(read_seq)
        ref_length = len(ref_seq)
        adapter = adapter_original[:]
        
        mismatches = 0
        mismatches_p, mismatches_a = 0, 0
        aligned_length = alg.end - alg.begin
        if not alg.is_reverse :
            for i in range(aligned_length) :
                if read_seq[i] != ref_seq[alg.begin + i] :
                    mismatches_p += 1
            for i in range(aligned_length, read_length) :
                if read_seq[i].upper() != adapter[i - aligned_length] :
                    mismatches_a += 1
            mismatches = mismatches_p + mismatches_a
        else :
            adapter = Bio.Seq.Seq(adapter).reverse_complement().tostring()
            start = read_length - aligned_length
            for i in range(start, start + aligned_length) :
                if read_seq[i] != ref_seq[alg.begin + i - start] :
                    mismatches_p += 1
            for i in range(start) :
                if read_seq[i].upper() != adapter[adapter_length - start + i] :
                    mismatches_a += 1
            mismatches = mismatches_p + mismatches_a
        total += 1
        
        if mismatches > 0 :
            if not chromosome_histogram.has_key(ref_name) :
                chromosome_histogram[ref_name] = 1
            else :
                chromosome_histogram[ref_name] = chromosome_histogram[ref_name] + 1
            if not orientation_histogram.has_key(alg.is_reverse) :
                orientation_histogram[alg.is_reverse] = 1
            else :
                orientation_histogram[alg.is_reverse] = orientation_histogram[alg.is_reverse] + 1
        if not mismatch_histogram.has_key(mismatches) : 
            mismatch_histogram[mismatches] = 1
        else :
            mismatch_histogram[mismatches] = mismatch_histogram[mismatches] + 1

    for mismatches, count in mismatch_histogram.iteritems() :
        print 'Mismatches: %d - %d (%5.2f%%)' % (mismatches, count, (100.0 * count) / total)
    
    print ''
    for chromosome, count in chromosome_histogram.iteritems() :
        print 'Chromosome: %s - %d' % (chromosome, count)

    print ''
    for orientation, count in orientation_histogram.iteritems() :
        print 'Orientation: %s - %d' % (str(orientation), count)

def produce_group_alignment_statistics(alignments) :
    '''
    Produce alignment statistics for grouped alignments.
    :param alignments : alignments to compute stats for.
    '''
    mismatch_histogram = {}
    multiplicity_histogram = {}
    total = len(alignments)
    for group in alignments :
        multiplicity = len(group)
        mismatches = group[0].mismatches
        if not mismatch_histogram.has_key(mismatches) :
            mismatch_histogram[mismatches] = 1
        else :
            mismatch_histogram[mismatches] = mismatch_histogram[mismatches] + 1

        if not multiplicity_histogram.has_key(multiplicity) :
            multiplicity_histogram[multiplicity] = 1
        else :
            multiplicity_histogram[multiplicity] = multiplicity_histogram[multiplicity] + 1

    for mismatches, count in mismatch_histogram.iteritems() :
        print 'Mismatches: %d - %d (%5.2f%%)' % (mismatches, count, (100.0 * count) / total)
    for multiplicity, count in multiplicity_histogram.iteritems() :
        print 'Multiplicity: %d - %d (%5.2f%%)' % (multiplicity, count, (100.0 * count) / total)

def filter_alignments_ingolia(alignments, mismatches = 2, min_length = 22, max_length = 32, max_nucA = 18, unique = False) :
    '''
    Filter alignments based on the number of mismatches, minimum and maximum alignment lengths and the length of the poly-A tail.
    :param alignments : alignments to be filtered out.
    :param mismatches : maximum allowed number mismatches.
    :param min_length : minimum allowed alignment length.
    :param max_length : maximum allowed alignment length.
    :param max_nucA : maximum number of allowed A nucleotides.
    :param unique : boolean flag, if true - only unique alignments are kept.
    '''
    result = []
    filtered = 0
    filtered_alg = 0
    for group in alignments :
        n_alignments = len(group)
        if unique and n_alignments > 1 :
            filtered += 1
            filtered_alg += n_alignments
            continue
        group_good = []
        for alg in group :
            # Too many mismatches
            if alg.mismatches > mismatches :
                filtered_alg = filtered_alg + 1
                continue
            cnt_nucA = 0
            # Too many As in the beginning of the read
            if not alg.is_reverse :
                for nuc in alg.seq[:24] :
                    if nuc == 'A' :
                        cnt_nucA += 1
            else :
                for nuc in alg.seq[-24:] :
                    if nuc == 'T' :
                        cnt_nucA += 1
            if cnt_nucA > max_nucA :
                filtered_alg = filtered_alg + 1
                continue
            # Read potentially too long
            if alg.maximum_length > max_length :
                filtered_alg = filtered_alg + 1
                continue
            # Read potentially too short
            if alg.maximum_length - alg.ambiguity_length < min_length :
                filtered_alg = filtered_alg + 1
                continue
            group_good.append(alg)

        if len(group_good) > 0 :
            result.append(group_good)
        else :
            filtered = filtered + 1

    return (result, filtered, filtered_alg)

def filter_alignments_mcmanus(alignments, mismatches, min_length, max_length, unique) :
    '''
    Filter alignments based on the number of mismatches, minimum and maximum alignment lengths.
    :param alignments : alignments to be filtered out.
    :param mismatches : maximum allowed number mismatches.
    :param min_length : minimum allowed alignment length.
    :param max_length : maximum allowed alignment length.
    :param unique : boolean flag, if true - only unique alignments are kept.
    '''
    result = []
    removed_groups = 0
    removed_alignments = 0
    for group in alignments :
        new_group = []
        if not unique or len(group) == 1 : 
            for alg in group :
                length = alg.end - alg.begin
                if alg.mismatches > mismatches or length > max_length or length < min_length :
                    removed_alignments += 1
                    continue
                new_group.append(alg)
        if len(new_group) == 0 :
            removed_groups += 1
        else :
            result.append(group)
    
    return (result, removed_groups, removed_alignments)

def map_alignment(seqs, alignments) :
    positions_for = {}
    positions_rev = {}
    for key in seqs.keys() :
        positions_for[key] = []
        positions_rev[key] = []
        length = len(seqs[key].seq)
        for l in range(length) :
            positions_for[key].append([])
            positions_rev[key].append([])

    for alg_group in alignments :
        for alg_id in range(len(alg_group)) :
            alg = alg_group[alg_id]
            if not seqs.has_key(alg.reference) :
                continue
            map = SimpleNamespace()
            map.group = alg_group
            map.alignment_id = alg_id
            map.num_covering_features = 0
            if not alg.is_reverse :
                positions_for[alg.reference][alg.begin].append(map)
            else :
                positions_rev[alg.reference][alg.begin].append(map)

    return (positions_for, positions_rev)

def cover_alignment_array(feature, alg, kernel, is_overlapping = False, is_ambiguous = False, array = None, array_overlapping = None, array_ambiguous = None, weight = 1.0, scale = 1.0, simulate = False) :
    weight /= float(scale)
    kernel = kernel(alg.begin, alg.end, weight)
    covered = 0
    start, stop = alg.begin, alg.end
    feature_start, feature_stop = feature.start, feature.stop
    if alg.begin >= feature_start and alg.end <= feature_stop :
        start, stop = alg.begin, alg.end
    elif alg.begin < feature_start and alg.end <= feature_stop and alg.end > feature_start :
        start, stop = feature_start, alg.end
    elif alg.begin >= feature_start and alg.end >= feature_stop and alg.begin < feature_stop :
        start, stop = alg.begin, feature_stop
    elif alg.begin <= feature_start and alg.end > feature_stop :
        start, stop = feature_start, feature_stop
    else : # read does not overlap with the feature
        start, stop = 0, 0
    
    if feature.strand == '+' :
        if not simulate :
            for i in range(start, stop) :
                array[i - feature_start] += kernel[i - alg.begin]
                if is_overlapping and array_overlapping is not None :
                    array_overlapping[i - feature_start] += kernel[i - alg.begin]
                if is_ambiguous and array_ambiguous is not None :
                    array_ambiguous[i - feature_start] += kernel[i - alg.begin]
        
        for i in range(start, stop) :
            covered += kernel[i - alg.begin]
    else :
        if not simulate :
            for i in range(start, stop) :
                array[i - feature_start] += kernel[alg.end - i - 1]
                if is_overlapping and array_overlapping is not None :
                    array_overlapping[i - feature_start] += kernel[alg.end - i - 1]
                if is_ambiguous and array_ambiguous is not None :
                    array_ambiguous[i - feature_start] += kernel[alg.end - i - 1]
        
        for i in range(start, stop) :
            covered += kernel[alg.end - i - 1]
  
    return covered

def cover_positions(genes, positions, kernel, delta = 50) :
    positions_for, positions_rev = positions
    for gene in genes.values() :
        features = gene.features
        for feature in features :
            if not positions_for.has_key(feature.reference) :
                continue
            length = len(positions_for[feature.reference])
            start = feature.start 
            stop = feature.stop
            if feature.strand == '+' :
                pos = positions_for
            else :
                pos = positions_rev
            start = max(start - delta, 0)
            stop = min(stop + delta, length)

            for i in range(start, stop) :
                for alg in pos[feature.reference][i] :
                    group = alg.group
                    id = alg.alignment_id
                    alignment_coverage = cover_alignment_array(feature, group[id], kernel, simulate = True)
                    if alignment_coverage > 0:
                        alg.num_covering_features += 1

    return (positions_for, positions_rev)

def feature_cmp(a, b) :
    if a.strand != b.strand :
        raise ValueError()

    strand = a.strand
    if strand == '+' :
        if a.start < b.start :
            return -1
        elif a.start > b.start :
            return +1
        elif a.stop > b.stop :
            return -1
        elif a.stop < b.stop :
            return +1
    else :
        if a.stop > b.stop : 
            return -1
        elif a.stop < b.stop :
            return +1
        elif a.start < b.start :
            return -1
        elif a.start > b.start :
            return +1
    return 0

def get_feature_seq(seqs, feature) :
    start = feature.start
    stop = feature.stop
    seq = seqs[feature.reference].seq[start:stop]
    if feature.strand == '-' :
        seq = Bio.Seq.Seq(seq).reverse_complement().tostring()
    return seq

def ribosome_asite_kernel_mcmanus(start, stop, weight = 1.0) :
    '''
    Kernel for ribosome occupancy events. Positions adjusted for the McManus dataset.
    :param start: start position of the read.
    :param stop: stop position of the kernel.
    :param weight: weight of the kernel.
    '''
    length = stop - start
    if length == 27 :
        offset_left = 11 + 1
    elif length == 28 :
        offset_left = 12 + 1
    elif length == 29 :
        offset_left = 13 + 1
    elif length >= 30 and length <= 33 :
        offset_left = 14 + 1
    else :
        offset_left = None
    
    offset_left += 3 # Move P-site to A-site
    kernel = np.zeros((length,))
    if offset_left is None :
        print (length, offset_left)
    kernel[offset_left] = weight

    return kernel

def ribosome_asite_kernel_ingolia(start, stop, weight = 1.0) :
    '''
    Kernel for ribosome occupancy events. Positions adjusted for the Ingolia dataset.
    :param start: start position of the read.
    :param stop: stop position of the kernel.
    :param weight: weight of the kernel.
    '''
    length = stop - start
    if length >= 27 and length <= 28 :
        offset_left = 12 + 1
    elif length >= 29 and length <= 31 :
        offset_left = 13 + 1
    elif length == 32 :
        offset_left = 12 + 1
    else :
        offset_left = None
    
    offset_left += 3 # Move P-site to A-site
    if offset_left is None :
        print (length, offset_left)
    kernel = np.zeros((length,))
    kernel[offset_left] = weight

    return kernel

def read_middle_kernel(start, stop, weight = 1.0) :
    '''
    Kernel for read middles. Covers the most central nucleotide.
    :param start: start position of the read.
    :param stop: stop position of the kernel.
    :param weight: weight of the kernel.
    '''
    length = stop - start
    kernel = np.zeros((length,))
    middle = int(length / 2)
    kernel[middle] = 1.0

    return kernel

def mrna_coverage_kernel(start, stop, weight = 1.0) :
    '''
    Kernel for read coverage events. Uses full length coverage.
    :param start: start position of the read.
    :param stop: stop position of the kernel.
    :param weight: weight of the kernel.
    '''
    length = stop - start
    kernel = np.ones((length,)) * weight
    return kernel

def ribosome_coverage_kernel(start, stop, weight = 1.0) :
    '''
    Kernel for read coverage events. Uses full length coverage.
    :param start: start position of the read.
    :param stop: stop position of the kernel.
    :param weight: weight of the kernel.
    '''
    length = stop - start
    kernel = np.ones((length,)) * weight
    return kernel

def make_feature_array(positions, feature, kernel, delta, assignment_kernel = None) :
    positions = positions[0] if feature.strand == '+' else positions[1]
    if not positions.has_key(feature.reference) :
        return KeyError()

    if assignment_kernel is None :
        assignment_kernel = kernel

    start, stop = feature.start, feature.stop
    positions = positions[feature.reference]

    feature_length = stop - start 
    array = np.zeros((feature_length, ))
    array_overlapping = np.zeros((feature_length, ))
    array_ambiguous = np.zeros((feature_length, ))
    total = 0 

    overlapping, ambiguous = 0, 0
    reference_length = len(positions)
    start_adj = max(0, min(reference_length, start - delta))
    stop_adj = max(0, min(reference_length, stop + delta))

    for i in range(start_adj, stop_adj) :
        for alg in positions[i] :
            id = alg.alignment_id
            group = alg.group
            count = 1
            n_alignments = len(group)
            n_covering_features = alg.num_covering_features

            is_overlapping = n_covering_features > 1
            is_ambiguous = n_alignments > 1
            scale = float(n_alignments)
            
            assigned = cover_alignment_array(feature, group[id], assignment_kernel, simulate = True)
            if assigned :
                covered = cover_alignment_array(feature, group[id], kernel, is_overlapping, is_ambiguous, array, array_overlapping, array_ambiguous, count, scale = scale)
                if covered > 0 :
                    total += covered
                    if is_overlapping :
                        overlapping += covered
                    if is_ambiguous :
                        ambiguous += covered

    if feature.strand == '-':
        array = array[::-1] 
        array_overlapping = array_overlapping[::-1]
        array_ambiguous = array_ambiguous[::-1]

    return (array, array_overlapping, array_ambiguous, overlapping, ambiguous, total)

def convert_to_mrna_array(seqs, genes, positions, kernel, delta = 50, assignment_kernel = None) :
    if assignment_kernel is None :
        assignment_kernel = kernel
    records = {}
    positions_for, positions_rev = positions
    for gene in genes.values() :
        cds_rec = gene.features 
        reference = cds_rec[0].reference
        if not seqs.has_key(reference) :
            continue

        cds_rec.sort(feature_cmp)
        feature_length, feature_seq, = 0, ''
        feature_array, feature_array_overlapping, feature_array_ambiguous = np.zeros((0,)), np.zeros((0,)), np.zeros((0,))
        overlapping, ambiguous, total = 0, 0, 0
        name = cds_rec[0].name
        n_recs = len(cds_rec)
        for i in range(n_recs) :
            rec = cds_rec[i]
            rec_array, rec_array_overlapping, rec_array_ambiguous, rec_overlapping, rec_ambiguous, rec_total = make_feature_array(positions, rec, kernel, delta, assignment_kernel = assignment_kernel)
            rec_seq = get_feature_seq(seqs, rec)

            feature_seq += rec_seq
            feature_array = np.concatenate((feature_array, rec_array))
            feature_array_overlapping = np.concatenate((feature_array_overlapping, rec_array_overlapping))
            feature_array_ambiguous = np.concatenate((feature_array_ambiguous, rec_array_ambiguous))
            
            rec_length = rec.stop - rec.start
            feature_length += rec_length
            overlapping += rec_overlapping
            ambiguous += rec_ambiguous
            total += rec_total
        
        gene_record = SimpleNamespace()
        gene_record.mrna = make_feature_record(feature_seq, feature_array, feature_array_overlapping, feature_array_ambiguous, feature_length, overlapping, ambiguous, total)
        records[name] = gene_record

    return records
