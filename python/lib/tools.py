'''
Implements a range of helper functions used throughout the project.

Alexey Gritsenko
31-10-2013
'''

import reader
import processors
import profile
import zhang
import scipy.stats
import scipy.optimize
import numpy as np
import copy
import math

from lib.SimpleNamespace import SimpleNamespace

class ParallelTimeout(Exception) :
    '''
    A custom class for parallel timeout exception.
    '''
    pass

def obtain_fit_confidence_intervals(model, left, right) :
    '''
    Obtain confidence intervals for the fit.
    '''
    ci = {}
    levels = [0.95, 0.90, 0.85, 0.8, 0.75, 0.70, 0.65, 0.60, 0.55, 0.50, 0.45, 0.40, 0.35, 0.30, 0.25, 0.20, 0.15, 0.10, 0.05]
    for alpha in levels :
        stats = model.stats(alpha = alpha)
        stats = stats['Density']
        for key, value in stats.items() :
            if 'HPD interval' in key :
                ci[alpha] = value
                break
    left_arr, right_arr = np.zeros((model.n_segments,), dtype = np.int), np.zeros((model.n_segments,), dtype = np.int)
    for i, id in enumerate(model.leaves) :
        left_arr[i] = left[id]
        right_arr[i] = right[id]
    return (left_arr, right_arr, ci)

def fit_segment_density_pymc(tree, dbname = None, dbtype = 'sqlite') :
    '''
    Given a tree with segment measurements, fit per codon densities that maximize the trees likelihood. Fit using MCMC (pyMC).
    :param tree : the segment tree to be approximated.
    :param dbname : name of the database to which the results should be saved.
    :param dbtype : type of the database to which the results should be saved.
    '''
    import pymc
    n_segments = len(tree.tree)
    parent, is_leaf, length, left, right = {}, {}, {}, {}, {}
    mu, sigma = {}, {}
    ends = set()
    for i in range(n_segments) :
        parent[i] = None
        is_leaf[i] = True
        node = tree.tree[i]
        left_i, right_i, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
        mu[i] = ratio_a / math.log(math.exp(1.0), 2)
        sigma[i] = node_sigma
        length[i] = right_i - left_i + 1
        left[i], right[i] = left_i, right_i
        ends.add(right[i])
    
    for i in range(n_segments) :
        for j in range(n_segments) :
            if i == j :
                continue
            if left[j] >= left[i] and right[j] <= right[i] :
                if parent[j] is None or length[parent[j]] > length[i] :
                    parent[j] = i
                    is_leaf[i] = False
    
    n_codons = tree.length_codons
    codons = -np.ones((n_codons,))
    for i in range(n_segments) :
        if is_leaf[i] :
            codons[left[i]:right[i] + 1] = i
    id = 0
    i = 0
    while i < n_codons :
        while i < n_codons and codons[i] >= 0 :
            i += 1
        if i >= n_codons :
            break
        j = i
        while j < n_codons and codons[j] < 0 and not j in ends :
            j += 1
        if j in ends and codons[j] < 0 :
            j += 1
        id -= 1
        left[id], right[id], length[id], parent[id], is_leaf[id] = i, j - 1, j - i, None, True
        i = j
    for i in range(n_segments) :
        for j in range(id, 0) :
            if left[j] >= left[i] and right[j] <= right[i] :
                if parent[j] is None or length[parent[j]] > length[i] :
                    parent[j] = i
                    is_leaf[i] = False

    children = {}
    for i in length :
        if i >= 0 :
            children[i] = []
            for j in length :
                if is_leaf[j] :
                    if left[j] >= left[i] and right[j] <= right[i] :
                        children[i].append(j)
    for id in length :
        if is_leaf[id] :
            print 'ID: %d, [%d, %d]' % (id, left[id], right[id])
    print ''
    for id in length :
        if not is_leaf[id] :
            print 'ID: %d, [%d, %d]' % (id, left[id], right[id])
            print children[id]

    def defined_model(mu, sigma, length, children, is_leaf) :
        # Bookkeeping
        leaves = [id for id in length if is_leaf[id]]
        n_segments = len(leaves)
        leaf_length = np.zeros((n_segments, ))
        id2pos = {}
        for pos, id in enumerate(leaves) :
            leaf_length[pos] = length[id]
            id2pos[id] = pos
        for id in children :
            new_list = []
            for id2 in children[id] :
                new_list.append(id2pos[id2])
            children[id] = new_list
        positive_ids = [id for id in length if id >= 0]
        n_positive_ids = len(positive_ids)
        real_mu, real_sigma = np.zeros((n_positive_ids, )), np.zeros((n_positive_ids, ))
        for i, id in enumerate(positive_ids) :
            real_mu[i] = mu[id]
            real_sigma[i] = sigma[id]
        
        # Actual model
        tau = 1.0 / real_sigma ** 2
        x = pymc.Uniform('Density', lower = 0.0, upper = 100.0, size = n_segments)
        @pymc.observed
        def segmentize(value = 0.0, x = x) :
            out = np.empty(n_positive_ids)
            for i, id in enumerate(positive_ids) :
                out[i] = np.dot(x[children[id]], leaf_length[children[id]]) / np.sum(leaf_length[children[id]])
            return pymc.lognormal_like(out, real_mu, tau)
        return locals()
    model = pymc.MCMC(defined_model(mu, sigma, length, children, is_leaf), db = dbtype, dbname = dbname)
    return left, right, model

def fit_segment_density(tree) :
    '''
    Given a tree with segment measurements, fit per codon densities that maximize the trees likelihood.
    :param tree : the segment tree to be approximated.
    '''
    n_segments = len(tree.tree)
    parent, is_leaf, length, left, right = {}, {}, {}, {}, {}
    ends = set()
    mean = {}
    for i in range(n_segments) :
        parent[i] = None
        is_leaf[i] = True
        node = tree.tree[i]
        left_i, right_i, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
        mean[i] = math.exp(ratio_a)
        length[i] = right_i - left_i + 1
        left[i], right[i] = left_i, right_i
        ends.add(right[i])
    
    for i in range(n_segments) :
        for j in range(n_segments) :
            if i == j :
                continue
            if left[j] >= left[i] and right[j] <= right[i] :
                if parent[j] is None or length[parent[j]] > length[i] :
                    parent[j] = i
    for i in range(n_segments) :
        is_leaf[parent[i]] = False
    
    n_codons = tree.length_codons
    codons = -np.ones((n_codons,))
    for i in range(n_segments) :
        if is_leaf[i] :
            codons[left[i]:right[i] + 1] = i
    #print codons
    id = 0
    i = 0
    while i < n_codons :
        while i < n_codons and codons[i] >= 0 :
            i += 1
        if i >= n_codons :
            break
        j = i
        while j < n_codons and codons[j] < 0 and not j in ends :
            j += 1
        if j in ends and codons[j] < 0 :
            j += 1
        id -= 1
        left[id], right[id], length[id], parent[id], is_leaf[id] = i, j - 1, j - i, None, True
        mean[id] = 0.5
        i = j
    for i in range(n_segments) :
        for j in range(id, 0) :
            if left[j] >= left[i] and right[j] <= right[i] :
                if parent[j] is None or length[parent[j]] > length[i] :
                    parent[j] = i

#    for i in parent :
#        if parent[i] is not None :
#            print '%d : [%d, %d] : parent %d' % (i, left[i], right[i], parent[i])
#        else :
#            print '%d : [%d, %d] : parent -' % (i, left[i], right[i])

    def objective(x, ids, left, right, n_codons, tree) :
        density = np.zeros((n_codons,))
        for i in range(len(ids)) :
            density[left[ids[i]] : right[ids[i]] + 1] = x[i]
        return -tree.evaluate(0.0, density)
    
    ids = [id for id in is_leaf.keys() if is_leaf[id]]
    n_vars = len(ids)
    start = np.ones((n_vars,))
    for i in range(n_vars) :
        start[i] = mean[ids[i]]
    fit = scipy.optimize.minimize(objective, start, args = (ids, left, right, n_codons, tree), method = 'SLSQP', bounds = [(0.0, 1000.0)] * n_vars, options = {'maxiter' : 1000000, 'maxfun' : 1000000})
    values = fit.x
    density = np.zeros((n_codons,))
    for i in range(len(ids)) :
        density[left[ids[i]] : right[ids[i]] + 1] = values[i]
    return (density, [(left[id], right[id]) for id in ids])

def get_scale_estimate(forest) :
    '''
    Obtain scale estimate from data.
    :param forest : forest from which we should obtain the mRNA estiamtes.
    '''
    number_of_mrna_molecules = 36139
    number_of_ribosomes = 2 * 10 ** 5
    fraction_of_active_ribosomes = 0.85
    
    mrna = {}
    length = {}
    genes = forest.keys()
    for gene in genes :
        length_cur = forest[gene].length_codons
        node = forest[gene].tree[0]
        if node[0] == 0 and node[1] == length_cur - 1 :
            mrna[gene] = 2 ** node[4]
            length[gene] = length_cur * 3
        else :
            pass
            #print '[!] Skipping gene %s' % gene
    mrna_total = np.sum(mrna.values(), dtype = np.double)
    
    coding_transcriptome_length = 0.0
    for gene in mrna.keys() :
        coding_transcriptome_length += (length[gene] * mrna[gene]) / mrna_total
    coding_transcriptome_length *= number_of_mrna_molecules
   
    K = (number_of_ribosomes * fraction_of_active_ribosomes) / coding_transcriptome_length
    C = 1.0 / K
    lnC = np.log(C)
    print '[i] Transcriptome length: %.10g' % coding_transcriptome_length
    print '[i] Number of active ribosomes: %.10g' % (number_of_ribosomes * fraction_of_active_ribosomes)
    print '[+] Scaling factor K: %.10g' % K
    print '[+] Scaling factor C: %.10g' % C
    print '[+] Scaling factor ln(C): %.10g' % lnC

    return lnC

def select_entries(density, genes) :
    '''
    Return a subselection of the dictionary.
    :param dict density: the dictionary.
    :param genes: a list of genes to select.
    '''
    out = {}
    for gene in genes :
        if density.has_key(gene) :
            out[gene] = density[gene]
    return out

def select_entries_inverse(density, genes) :
    '''
    Return a list of genes that are in the provided gene list.
    :param dict density: the dictionary.
    :param genes: list of genes to remove from the dictionary.
    '''
    out = {}
    for gene in density.keys() :
        if not gene in genes :
            out[gene] = density[gene]
    return out

def overlap_gene_lists(a, b) :
    '''
    Return overlap of two gene sets.
    :param a: Set a
    :param b: Set b
    '''
    return list(set(a) & set(b))

def load_arrays_tree(rib_file  = '../results/mcmanus-reads/rib_density.out', mrna_file = '../results/mcmanus-reads/mrna_density.out') :
    '''
    Load default ribosome and mRNA density arrays.
    :param string rib_file: Ribosome density filename.
    :param string mrna_file: mRNA density filename.
    '''
    # Read
    rib = reader.read_pickle(rib_file)
    mrna = reader.read_pickle(mrna_file)
    
    return (mrna, rib)

def stats_on_alignment_reads(alignment, mismatches = 2, min_length = 27, max_length = 33, unique = False, should_copy = True, filter = None) :
    '''
    Computes simple statistics on the alignment reads.
    :param alignment: a list of read alignments.
    :param mismatches: maximum number of mismatches allowed.
    :param min_length: minimum allowed alignment length.
    :param max_length: maximum allowed alignment length.
    :param unique: if true, all ambiguous reads (reads with multiple alignments) are filtered out.
    :param should_copy: if True - copy alignment before filtering it.
    :param filter: which filter function to use (default - McManus)
    '''
    if filter is None :
        filter = processors.filter_alignments_mcmanus
    if should_copy :
        alignment = copy.deepcopy(alignment)
    alignment, removed_groups, removed_alg = filter(alignment, mismatches, min_length, max_length, unique = unique)
    count, count_ambiguous, count_alignments, min_length, max_length, max_mismatches = 0, 0, 0, 99, -1, -1
    for group in alignment :
        count += 1
        n_alignments = len(group)
        count_alignments += n_alignments
        for alg in group :
            cur_mismatches = alg.mismatches
            if cur_mismatches > max_mismatches :
                max_mismatches = cur_mismatches
            max_length = max(max_length, alg.end - alg.begin)
            min_length = min(min_length, alg.end - alg.begin)
        if n_alignments > 1 :
            count_ambiguous += 1
    print 'Count: %d' % count
    print 'Count ambiguous: %d' % count_ambiguous
    print 'Count unique: %d' % (count - count_ambiguous)
    print 'Count alignments: %d' % count_alignments
    print 'Min. length: %d' % min_length
    print 'Max. length: %d' % max_length
    print 'Max. mismatches: %d' % max_mismatches

def stats_on_cds_assigned_reads(alignment, reference, records, kernel, filter, mismatches = 2, min_length = 27, max_length = 33, unique = False, should_copy = True) : 
    '''
    :param alignment : a list of read alignments.
    :param reference : genome reference sequence.
    :param records : GFF records of CDSes.
    :param kernel : kernel used for read assginment.
    :param filter : filter used for read filtering.
    :param mismatches : maximum number of mismatches allowed.
    :param min_length : minimum allowed alignment length.
    :param max_length : maximum allowed alignment length.
    :param unique : if true, all ambiguous reads (reads with multiple alignments) are filtered out.
    :param should_copy : whether we should copy the array before messing with it.
    '''
    genes = processors.group_gff_records_by_name(records)
    genes = processors.unify_gff_record_groups(genes)
    if should_copy :
        alignment = copy.deepcopy(alignment)
    filtered_alignment, removed_groups, removed_alg = filter(alignment, mismatches, min_length, max_length, unique = unique)
    del alignment
    positions = processors.map_alignment(reference, filtered_alignment)
    del filtered_alignment
    positions = processors.cover_positions(genes, positions, kernel)
    covered, overlapping = 0.0, 0.0
    for strand in positions :
        chromosomes = strand.keys()
        for chr in chromosomes :
            array = strand[chr]
            for pos in array :
                for entry in pos :
                    id = entry.alignment_id
                    num_covering_features = entry.num_covering_features
                    if num_covering_features == 0 :
                        continue
                    group = entry.group
                    group_size = len(group)
                    group_weight = 1.0 / float(group_size)
                    covered += group_weight
                    if num_covering_features > 1 :
                        overlapping += group_weight
    print 'Covered: %.2f' % covered
    print 'Overlapping: %.2f' % overlapping

def get_read_count_from_array(array) :
    '''
    Obtain read count by summing over genes in mRNA/Ribosome arrays.
    '''
    acc = 0.0
    genes = array.keys()
    for gene in genes :
        acc += array[gene].mrna.total
    return acc

def get_cds_array(record, which = 'summary') :
    '''
    Returns the CDS regions from the gene array record of the offset mRNA.
    :param record: feature record to obtain array from.
    :param which: type of array to obtain, can be 'summary' (default), 'overlapping' or 'ambiguous'.
    '''
    array = record.mrna.array
    if which == 'overlapping' :
        array = record.mrna.array_overlapping
    elif which == 'ambiguous' :
        array = record.mrna.array_ambiguous
    elif which == 'sequence' :
        array = record.mrna.seq
    return array

def get_codon_array(record, which = 'summary') :
    '''
    Returns the CDS regions from the gene array record of the offset mRNA at codon resolution.
    :param record: feature record to obtain codon array from.
    :param which: type of array to obtain, can be 'summary' (default), 'overlapping' or 'ambiguous'.
    '''
    array = record.mrna.array
    if which == 'overlapping' :
        array = record.mrna.array_overlapping
    elif which == 'ambiguous' :
        array = record.mrna.array_ambiguous
    elif which == 'sequence' :
        array = record.mrna.seq

    start_prefix = record.start_prefix if record.has_key('start_prefix') else 0 # This way we support the storage formats of density version 1 (Ingolia dataset) and density version 2.
    length = len(array) - start_prefix 
    length_codons = length / 3
    if which != 'sequence' :
        array_codons = np.zeros((length_codons, ))
    else :
        array_codons = np.zeros((length_codons, ), dtype = 'S3')

    if which == 'sequence' :
        for i in range(0, length_codons) :
            array_codons[i] = array[start_prefix + 3 * i : start_prefix + 3 * (i + 1)]
    else :
        for i in range(0, length_codons) :
            array_codons[i] = np.sum(array[start_prefix + 3 * i : start_prefix + 3 * (i + 1)])
    return array_codons

def get_cds_count(record) :
    '''
    Return coverage sum for the CDS of an mRNA record.
    '''
    cds = get_cds_array(record)
    return np.sum(cds)

def get_mean_read_length(alignment, should_filter = True) :
    '''
    Compute mean read length from read alignments.
    :param alignment: list of read alignments.
    :param should_filter: a boolean flag specifying whether the alignments should be filtered.
    '''
    if should_filter :
        alignment, removed_groups, removed_alg = reader.filter_alignments(alignment)

    total_count, total_length = 0.0, 0.0
    for group in alignment :
        read_count = float(group[1])
        total_count += read_count
        alignment_count = float(len(group[2]))
        for alg in group[2] :
            read_length = alg.stop - alg.start
            total_length += read_length * (read_count / alignment_count)

    return total_length / total_count

def correlate_profiles(profiles_a, profiles_b, reliable = [], only_nonzero = True, log = False):
    '''
    Compute correlations between gene profiles.
    :param profiles_a: dictionary with a first set profiles.
    :param profiles_b: dictionary with a second set of profiles.
    :param reliable: a list of genes that are measured reliably.
    :param only_nonzero: if true, only non-zero coverage array positions are used to compute correlations.
    :param log: if True, then values are log-transformed prior to correlation. Implies only_zero = True.
    '''
    genes_a = profiles_a.keys()
    genes_b = profiles_b.keys()
    genes = overlap_gene_lists(genes_a, genes_b)
    n_genes = len(genes)
    corr = []
    reliable = set(reliable)
    out_genes = []
    if log :
        only_nonzero = True
    for i in range(n_genes) :
        gene = genes[i]
        if len(reliable) > 0 and (not gene in reliable) :
            continue
        array_a, array_b = profiles_a[gene], profiles_b[gene]
        positions = np.logical_and(np.negative(np.isnan(array_a)), np.negative(np.isnan(array_b)))
        if only_nonzero :
            nz_positions = np.logical_and(array_a > 0, array_b > 0)
            positions = np.logical_and(positions, nz_positions)
        if np.sum(positions) <= 1 :
            continue
        if not log :
            res = scipy.stats.pearsonr(array_a[positions], array_b[positions])
        else :
            res = scipy.stats.pearsonr(np.log(array_a[positions]), np.log(array_b[positions]))
        r = res[0]
        if np.isnan(r) or isinstance(r, np.ndarray) :
            continue
        corr.append(r)
        out_genes.append(gene)
    corr = np.array(corr)
    return (out_genes, corr)

def get_sigma_for_length(length, sigmas) :
    '''
    Result 
    :param length: segment length for which the estimate has to be computed.
    :param sigmas: a list with length group biological variance estimates.
    '''
    n_sigmas = len(sigmas)
    result = None
    for i in range(n_sigmas) :
        min_length, max_length, sigma, group_size = sigmas[i]
        if length >= min_length and length < max_length:
            result = sigma
            break
    return result

def fit_segment_density(tree) :
    '''
    Given a tree with segment measurements, fit per codon densities that maximize the trees likelihood.
    :param tree : the segment tree to be approximated.
    '''
    n_segments = len(tree.tree)
    parent, is_leaf, length, left, right = {}, {}, {}, {}, {}
    ends = set()
    mean = {}
    for i in range(n_segments) :
        parent[i] = None
        is_leaf[i] = True
        node = tree.tree[i]
        left_i, right_i, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
        mean[i] = math.exp(ratio_m)
        length[i] = right_i - left_i + 1
        left[i], right[i] = left_i, right_i
        ends.add(right[i])
    
    for i in range(n_segments) :
        for j in range(n_segments) :
            if i == j :
                continue
            if left[j] >= left[i] and right[j] <= right[i] :
                if parent[j] is None or length[parent[j]] > length[i] :
                    parent[j] = i
    for i in range(n_segments) :
        is_leaf[parent[i]] = False
    
    n_codons = tree.length_codons
    codons = -np.ones((n_codons,))
    for i in range(n_segments) :
        if is_leaf[i] :
            codons[left[i]:right[i] + 1] = i
    #print codons
    id = 0
    i = 0
    while i < n_codons :
        while i < n_codons and codons[i] >= 0 :
            i += 1
        if i >= n_codons :
            break
        j = i
        while j < n_codons and codons[j] < 0 and not j in ends :
            j += 1
        if j in ends and codons[j] < 0 :
            j += 1
        id -= 1
        left[id], right[id], length[id], parent[id], is_leaf[id] = i, j - 1, j - i, None, True
        mean[id] = 0.5
        i = j
    for i in range(n_segments) :
        for j in range(id, 0) :
            if left[j] >= left[i] and right[j] <= right[i] :
                if parent[j] is None or length[parent[j]] > length[i] :
                    parent[j] = i

#    for i in parent :
#        if parent[i] is not None :
#            print '%d : [%d, %d] : parent %d' % (i, left[i], right[i], parent[i])
#        else :
#            print '%d : [%d, %d] : parent -' % (i, left[i], right[i])

    def objective(x, ids, left, right, n_codons, tree) :
        density = np.zeros((n_codons,))
        for i in range(len(ids)) :
            density[left[ids[i]] : right[ids[i]] + 1] = x[i]
        return -tree.evaluate(0.0, density)
    
    ids = [id for id in is_leaf.keys() if is_leaf[id]]
    n_vars = len(ids)
    start = np.ones((n_vars,))
    for i in range(n_vars) :
        start[i] = mean[ids[i]]
    fit = scipy.optimize.minimize(objective, start, args = (ids, left, right, n_codons, tree), method = 'SLSQP', bounds = [(0.0, 1000.0)] * n_vars, options = {'maxiter' : 1000000, 'maxfun' : 1000000})
    values = fit.x
    density = np.zeros((n_codons,))
    for i in range(len(ids)) :
        density[left[ids[i]] : right[ids[i]] + 1] = values[i]
    return (density, [(left[id], right[id]) for id in ids])

def build_coverage_profiles(mrna, rib, n_mrna = 1.0, n_rib = 1.0, type = 'mean') :
    '''
    :param mrna: dictionary of mRNA coverage counts.
    :param rib: dictionary of ribosome coverage counts.
    :param n_mrna: number of exon-mapped mRNA reads.
    :param n_rib: number of exon-mapped ribosome reads.
    :param type: type e to construct, can be 'mean' or 'profile'. 
    '''
    genes_mrna = mrna.keys()
    genes_rib = rib.keys()
    genes = overlap_gene_lists(genes_mrna, genes_rib)
    profiles = {}
    for gene in genes :
        profiles[gene] = profile.make_coverage_profile(mrna[gene], rib[gene], n_mrna, n_rib, type = type)
    return profiles

def build_occupancy_profiles(mrna, rib, n_mrna = 1.0, n_rib = 1.0, type = 'mean') :
    '''
    :param mrna: dictionary of mRNA occupancy counts.
    :param rib: dictionary of ribosome occupancy counts.
    :param n_mrna: number of exon-mapped mRNA reads.
    :param n_rib: number of exon-mapped ribosome reads.
    :param type: type e to construct, can be 'mean' or 'profile'. 
    '''
    genes_mrna = mrna.keys()
    genes_rib = rib.keys()
    genes = overlap_gene_lists(genes_mrna, genes_rib)
    profiles = {}
    for gene in genes :
        profiles[gene] = profile.make_occupancy_profile(mrna[gene], rib[gene], n_mrna, n_rib, type = type)
    return profiles

def get_RFM_rates() :
    '''
    Return RFM translation elongation rates.
    '''
    global codon2int

    result = {}
    rates = 1.0 / zhang.RFM_times
    for codon in codon2int.keys() :
        result[codon] = rates[codon2int[codon]]
    return result

def initialize_codon_dictionary() :
    '''
    Initializes the modules codon dictionary. It can be used to convert codon string representation into numeric representation.
    '''
    global codon2int

    codon2int = {}
    nucleotides = ['A', 'T', 'C', 'G']
    counter = 0
    for nuc1 in nucleotides :
        for nuc2 in nucleotides :
            for nuc3 in nucleotides :
                codon = nuc1 + nuc2 + nuc3
                codon2int[codon] = counter
                counter += 1

def initialize_amino_acid_dictionary() :
    '''
    Initialized the modules amino acid dictionaries.
    Includes:
    a) codon to amino acid dictionary.
    '''
    global codon2int
    global codon2amino
    
    codon2amino = {
        'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
        'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
        'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
        'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
        'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
        'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
        'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
        'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
        'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
        'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
        'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
        'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
        'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
        'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
        'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
        'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W'}
   
    items = codon2amino.items()
    for codon, amino in items :
        codon_int = codon2int[codon]
        codon2amino[codon_int] = amino

def initialize_chromosome_dictionary() :
    '''
    Automatic initialization of a dictionary translating chromosome names to integers.
    '''
    global chromosome2int
    chromosome2int = {
        'chrI' : 1,
        'chrII' : 2,
        'chrIII' : 3,
        'chrIV' : 4,
        'chrV' : 5,
        'chrVI' : 6,
        'chrVII' : 7,
        'chrVIII' : 8,
        'chrIX' : 9,
        'chrX' : 10,
        'chrXI' : 11,
        'chrXII' : 12,
        'chrXIII' : 13,
        'chrXIV' : 14,
        'chrXV' : 15,
        'chrXVI' : 16,
        'chrmt' : 100,
        '2-micron' : 101
        }

initialize_chromosome_dictionary()
initialize_codon_dictionary()
initialize_amino_acid_dictionary()

