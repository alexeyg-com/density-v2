from lib.SimpleNamespace import SimpleNamespace
import lib.zhang
import lib.simulator
import lib.tree

import numpy as np
try :
    import pylab
    import matplotlib.pyplot as plot
    import matplotlib.mlab as mlab
    from matplotlib.lines import Line2D
except :
    pass
import scipy.stats
import scipy.cluster
import math
import statsmodels.api as sm
import timeit
import json
import copy

import lib.evaluator
try :
    import lib.david
except :
    pass

from IPython.parallel import Client

from IPython.parallel import Client

def plot_ecoli_tai(rates) :
    codons = [codon for codon in lib.tools.codon2int if codon not in ['TGA', 'TAA', 'TAG']]
    amino2codon = {}
    for codon, amino in lib.tools.codon2amino.iteritems() :
        if isinstance(codon, int) :
            continue
        if not amino2codon.has_key(amino) :
            amino2codon[amino] = [codon]
        else :
            amino2codon[amino].append(codon)
    codons.sort(key = lambda x : lib.tools.codon2amino[x])
    aminos = [lib.tools.codon2amino[codon] for codon in codons]
    codons = np.array(codons)
    n_codons = len(codons)
    data = np.zeros((n_codons, ), dtype = np.float)
    for i, codon in enumerate(codons) :
        data[i] = rates[codon]
    f, ax = plot.subplots(1, 1)
    ax.bar(range(n_codons), data, width = 1.0)
    ax.set_xticks(np.array(range(n_codons)) + 0.5)
    ax.set_xticklabels(codons, rotation = 90, ha = 'center')
    ax.set_xlim(0, n_codons)
    ax.tick_params(bottom = 'off', top = 'off', left = 'off', right = 'off')
    _, ymax = ax.get_ylim()
    ax.set_ylim(0, ymax)
    
    offset = 0.3
    y_off = 0.2
    left = 0
    for i in range(1, n_codons + 1) :
        if i == n_codons or aminos[i - 1] != aminos[i] :
            right = i
            print left, right
            line = ax.plot([left + offset, right - offset], [-y_off, -y_off], linewidth = 2, color = 'k')
            line[0].set_clip_on(False)
            mid = (right + left) / 2.0
            ax.text(mid, -0.25, aminos[i - 1], va = 'top', ha = 'center', fontsize = 13, fontweight = 'bold')
            left = right

    f.subplots_adjust(bottom = 0.23)
    f.set_size_inches(19, 4, forward = True)
    f.tight_layout()
    f.subplots_adjust(bottom = 0.23, right = 0.94)
    plot.show()

def find_genes_with_large_differences(res, forest, window = 60) :
    genes = res.names
    results = []
    for gene in genes :
        n_codons = len(res.density[gene])
        reconstructed_density, segments = lib.tools.fit_segment_density(forest[gene])
        density = res.density[gene]
        segmented_density = np.zeros((n_codons, ))
        for left, right in segments :
            segmented_density[left : right + 1] = np.mean(density[left : right + 1])
        max_diff = -float('inf')
        pos = None
        for offset in range(n_codons - window + 1) :
            diff = abs(np.sum(reconstructed_density[offset : offset + window] - segmented_density[offset : offset + window]))
            if diff > max_diff :
                max_diff = diff
                pos = (offset, offset + window)
        results.append((gene, max_diff, pos))
    return results

def normalize_index(index) :
    res = copy.deepcopy(index)
    amino2codon = {}
    for codon, amino in lib.tools.codon2amino.iteritems() :
        if isinstance(codon, int) :
            continue
        if not amino2codon.has_key(amino) :
            amino2codon[amino] = [codon]
        else :
            amino2codon[amino].append(codon)
   
    for amino in amino2codon :
        if amino == '*' :
            continue
        W = -float('inf')
        for codon in amino2codon[amino] :
            W = max(W, index[codon])
        for codon in amino2codon[amino] :
            res[codon] = res[codon] / W
    return res

def get_codon_frequency() :
    res = {}
    res['TTT'] = 26.1
    res['TCT'] = 23.5
    res['TAT'] = 18.8
    res['TGT'] = 8.1
    res['TTC'] = 18.4
    res['TCC'] = 14.2
    res['TAC'] = 14.8
    res['TGC'] = 4.8
    res['TTA'] = 26.2
    res['TCA'] = 18.7
    res['TAA'] = 1.1
    res['TGA'] = 0.7
    res['TTG'] = 27.2
    res['TCG'] = 8.6
    res['TAG'] = 0.5
    res['TGG'] = 10.4

    res['CTT'] = 12.3
    res['CCT'] = 13.5
    res['CAT'] = 13.6
    res['CGT'] = 6.4
    res['CTC'] = 5.4
    res['CCC'] = 6.8
    res['CAC'] = 7.8
    res['CGC'] = 2.6
    res['CTA'] = 13.4
    res['CCA'] = 18.3
    res['CAA'] = 27.3
    res['CGA'] = 3.0
    res['CTG'] = 10.5
    res['CCG'] = 5.3
    res['CAG'] = 12.1
    res['CGG'] = 1.7

    res['ATT'] = 30.1
    res['ACT'] = 20.3
    res['AAT'] = 35.7
    res['AGT'] = 14.2
    res['ATC'] = 17.2
    res['ACC'] = 12.7
    res['AAC'] = 24.8
    res['AGC'] = 9.8
    res['ATA'] = 17.8
    res['ACA'] = 17.8
    res['AAA'] = 41.9
    res['AGA'] = 21.3
    res['ATG'] = 20.9
    res['ACG'] = 8.0
    res['AAG'] = 30.8
    res['AGG'] = 9.2

    res['GTT'] = 22.1
    res['GCT'] = 21.2
    res['GAT'] = 37.6
    res['GGT'] = 23.9
    res['GTC'] = 11.8
    res['GCC'] = 12.6
    res['GAC'] = 20.2
    res['GGC'] = 9.8
    res['GTA'] = 11.8
    res['GCA'] = 16.2
    res['GAA'] = 45.6
    res['GGA'] = 10.9
    res['GTG'] = 10.8
    res['GCG'] = 6.2
    res['GAG'] = 19.2
    res['GGG'] = 6.0
    return res

def get_elongation_rates(res) :
    codons = [codon for codon in lib.tools.codon2int.keys() if codon not in ['TGA', 'TAA', 'TAG']]
    result = {}
    for codon in codons :
        codon_int = lib.tools.codon2int[codon]
        result[codon] = lib.simulator.RateFunction.sigmoid(res.vector[codon_int])
    return result

def plot_codon_optimization_comparison(weights, legends) :
    codons = None
    for weight in weights : 
        if codons is None :
            codons = set(weight.keys())
        else :
            codons = codons & set(weight.keys())
    amino2codon = {}
    for codon, amino in lib.tools.codon2amino.iteritems() :
        if isinstance(codon, int) :
            continue
        if not amino2codon.has_key(amino) :
            amino2codon[amino] = [codon]
        else :
            amino2codon[amino].append(codon)
    codons = list(codons)
    codons.sort(key = lambda x : lib.tools.codon2amino[x])
    aminos = [lib.tools.codon2amino[codon] for codon in codons]
    codons = np.array(codons)
    n_codons = len(codons)
    n_weights = len(weights)
    data = np.zeros((n_weights, n_codons), dtype = np.float)
    f, ax = plot.subplots(1, 1)
    for i, weight, legend in zip(range(n_weights), weights, legends) :
        for j, codon in enumerate(codons) :
            data[i, j] = weight[codon]
#        for amino in amino2codon :
#            if amino == '*' :
#                continue
#            indices = [np.where(codons == c)[0] for c in amino2codon[amino]]
#            #print amino
#            #print amino2codon[amino]
#            #print indices
#            data[i, indices] /= np.max(data[i, indices])
#        #normalize by amino acid
#        #data[i, :] /= np.sum(data[i, :])
    cax = ax.pcolor(data, cmap = plot.cm.Blues, edgecolor = 'w', linewidth = 0.3)
    new_axis = f.add_axes([0.96, 0.23, 0.01, 0.72])
    cbar = f.colorbar(cax, orientation = 'vertical', cax = new_axis)
    cbar.set_label('Relative adaptiveness', fontsize = 14, labelpad = -50)
    for i in range(n_codons) :
        if i == 0 or i == n_codons or aminos[i - 1] != aminos[i] :
            ax.axvline(x = i - 1e-2, linewidth = 2, color = 'w')
    for i in range(n_weights) :
        if i > 0 :
            ax.axhline(y = i - 1e-2, linewidth = 2, color = 'w')
    offset = 0.3
    left = 0
    for i in range(1, n_codons + 1) :
        if i == n_codons or aminos[i - 1] != aminos[i] :
            right = i
            print left, right
            line = ax.plot([left + offset, right - offset], [-0.5, -0.5], linewidth = 2, color = 'k')
            line[0].set_clip_on(False)
            mid = (right + left) / 2.0
            ax.text(mid, -0.6, aminos[i - 1], va = 'top', ha = 'center', fontsize = 13, fontweight = 'bold')
            left = right

    ax.set_xticks(np.array(range(n_codons)) + 0.5)
    ax.set_xticklabels(codons, rotation = 90, ha = 'center')
    ax.set_yticks(np.array(range(n_weights)) + 0.5)
    ax.set_yticklabels(legends, rotation = 90)
    ax.set_xlim(0, n_codons)
    ax.set_ylim(0, n_weights)
    ax.tick_params(bottom = 'off', top = 'off', left = 'off', right = 'off')
    f.set_size_inches(19, 4, forward = True)
    f.tight_layout()
    f.subplots_adjust(bottom = 0.23, right = 0.94)
    plot.show()

def plot_model_comparison_and_density(res_init_genes, res_init, forest) :
    genes = list(set(res_init_genes.names) & set(res_init.names))
    J_init, J_init_genes = [], []
    density_init, density_init_genes = [], []
    for gene in genes :
        J_init.append(res_init.J[gene])
        J_init_genes.append(res_init_genes.J[gene])
        density_init.append(np.mean(res_init.density[gene]))
        density_init_genes.append(np.mean(res_init_genes.density[gene]))
    f, ax = plot.subplots(1, 3)
    J_init, J_init_genes = np.log2(J_init), np.log(J_init_genes)
    density_init, density_init_genes = np.log2(density_init), np.log(density_init_genes)
    ax[0].scatter(J_init, J_init_genes, color = 'k', alpha = 0.1)
    ax[1].scatter(density_init, density_init_genes, color = 'k', alpha = 0.1)
    ax[0].set_xlabel('$\\mathrm{TASEP}^\\mathrm{init}$ segment trees', fontsize = 16)
    ax[0].set_ylabel('$\\mathrm{TASEP}^\\mathrm{init}$ full-CDS', fontsize = 16)
    r, p = scipy.stats.spearmanr(J_init, J_init_genes)
    ax[0].text(0.55, 0.03, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[0].transAxes, color = 'black', fontsize = 15)
    r, p = scipy.stats.pearsonr(J_init, J_init_genes)
    ax[0].text(0.55, 0.10, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[0].transAxes, color = 'black', fontsize = 15)
    
    ax[0].set_title('PPR, $\\log_2$', fontsize = 15, fontweight = 'bold')
    ax[1].set_xlabel('$\\mathrm{TASEP}^\\mathrm{init}$ segment trees', fontsize = 16)
    ax[1].set_ylabel('$\\mathrm{TASEP}^\\mathrm{init}$ full-CDS', fontsize = 16)
    ax[1].set_title('Gene-level avg. density, $\\log_2$', fontsize = 15, fontweight = 'bold')
    r, p = scipy.stats.spearmanr(density_init, density_init_genes)
    ax[1].text(0.55, 0.03, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[1].transAxes, color = 'black', fontsize = 15)
    r, p = scipy.stats.pearsonr(density_init, density_init_genes)
    ax[1].text(0.55, 0.10, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[1].transAxes, color = 'black', fontsize = 15)
        
    data, simulation = [], []
    colors = []
    bad = []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if right - left + 1 != length_codons :
                continue
            measured_segment_density = 2 ** ratio_a
            simulated_segment_density = np.mean(res_init_genes.density[gene][left : right + 1])
            data.append(measured_segment_density)
            simulation.append(simulated_segment_density)
            if abs(np.log2(measured_segment_density) - np.log2(simulated_segment_density)) < 10 :
                color = 0
            else :
                color = 1
                bad.append(gene)
            colors.append(color)
    data, simulation = np.log2(data), np.log2(simulation)
    ax[2].scatter(data, simulation, color = 'k', alpha = 0.1, c = colors)
    ax[2].set_xlabel('Measured density ratio', fontsize = 16)
    ax[2].set_ylabel('$\\mathrm{TASEP}^\\mathrm{init}$ full-CDS', fontsize = 16)
    r, p = scipy.stats.spearmanr(data, simulation)
    ax[2].text(0.55, 0.03, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[2].transAxes, color = 'black', fontsize = 15)
    r, p = scipy.stats.pearsonr(data, simulation)
    ax[2].text(0.55, 0.10, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[2].transAxes, color = 'black', fontsize = 15)
    ax[2].set_title('Gene-level avg. density, $\\log_2$', fontsize = 15, fontweight = 'bold')
    
    f.set_size_inches(18, 6, forward = True)
    f.tight_layout()
    plot.show()
    return bad

def plot_model_comparison(res_init, res_elong, qq = False) :
    genes = list(set(res_init.names) & set(res_elong.names))
    J_init, J_elong = [], []
    density_init, density_elong = [], []
    for gene in genes :
        J_init.append(res_init.J[gene])
        J_elong.append(res_elong.J[gene])
        density_init.append(np.mean(res_init.density[gene]))
        density_elong.append(np.mean(res_elong.density[gene]))
    f, ax = plot.subplots(1, 2)
    J_init, J_elong = np.log2(J_init), np.log(J_elong)
    density_init, density_elong = np.log2(density_init), np.log(density_elong)
    if qq :
        J_init.sort()
        J_elong.sort()
        density_init.sort()
        density_elong.sort()
    ax[0].scatter(J_init, J_elong, color = 'k', alpha = 0.1)
    ax[1].scatter(density_init, density_elong, color = 'k', alpha = 0.1)
    ax[0].set_xlabel('$\\mathrm{TASEP}^\\mathrm{init}$', fontsize = 16)
    ax[0].set_ylabel('$\\mathrm{TASEP}^\\mathrm{elong}$', fontsize = 16)
    r, p = scipy.stats.spearmanr(J_init, J_elong)
    ax[0].text(0.55, 0.03, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[0].transAxes, color = 'black', fontsize = 15)
    r, p = scipy.stats.pearsonr(J_init, J_elong)
    ax[0].text(0.55, 0.10, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[0].transAxes, color = 'black', fontsize = 15)
    
    ax[0].set_title('PPR, $\\log_2$', fontsize = 15, fontweight = 'bold')
    ax[1].set_xlabel('$\\mathrm{TASEP}^\\mathrm{init}$', fontsize = 16)
    ax[1].set_ylabel('$\\mathrm{TASEP}^\\mathrm{elong}$', fontsize = 16)
    ax[1].set_title('Gene-level avg. density, $\\log_2$', fontsize = 15, fontweight = 'bold')
    r, p = scipy.stats.spearmanr(density_init, density_elong)
    ax[1].text(0.55, 0.03, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[1].transAxes, color = 'black', fontsize = 15)
    r, p = scipy.stats.pearsonr(density_init, density_elong)
    ax[1].text(0.55, 0.10, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[1].transAxes, color = 'black', fontsize = 15)
    
    f.set_size_inches(15, 6, forward = True)
    f.tight_layout()
    plot.show()

def _plot_density_example(ax, gene, forest, results, legends, segment = None, fit = None, colors = ['g', 'r', 'b'], highlight_codons = ['GAC', 'TTG', 'CCA', 'CAA', 'GCC', 'GGT', 'GAT', 'TTT', 'CAG', 'GTG', 'ACG', 'CCT', 'CGA'], show_labels = True, force_ymax = None) :
    #f, ax = plot.subplots(2, 1)
    n_codons = forest[gene].length_codons
    x = range(n_codons)
    if segment is None :
        segment = (1, n_codons)
    show_left, show_right = segment[0] - 1, segment[1]

    #ax[0].set_xlim(-1, n_codons)
    if fit is None :
        reconstructed_density, segments = lib.tools.fit_segment_density(forest[gene])
        ax[1].plot(x, reconstructed_density, linewidth = 2, label = 'Reconstruction', color = 'k')
    else :
        ci_left, ci_right, ci = fit
        segments = zip(ci_left, ci_right)
        levels = [0.1, 0.5, 0.9]
        density_hi, density_low = np.zeros((n_codons, )), np.zeros((n_codons, ))
        for k, alpha in enumerate(levels) :
            for i, segment in enumerate(segments) :
                left, right = segment
                density_low[left : right + 1] = ci[alpha][i][0]
                density_hi[left : right + 1] = ci[alpha][i][1]
            if k == len(levels) - 1 :
                ax[1].fill_between(x, density_hi, density_low, color = '0.1', alpha = 0.15)
            else :
                ax[1].fill_between(x, density_hi, density_low, color = '0.1', alpha = 0.15, linewidth = 0.001)
        import matplotlib.patches as mpatches
        patch = mpatches.Rectangle([0, 0], 1e-5, 1e-5, facecolor = '0.1', alpha = 0.15, edgecolor = None, linewidth = 0.001, label = 'Reconstruction')
        ax[1].add_patch(patch)

    for result, legend, color in zip(results, legends, colors) :
        if result.has_key('initiation') :
            init = result.initiation[gene]
            init = lib.simulator.RateFunction.sigmoid(init)
            print '%25s : %.10f' % (legend, init)
        density = result.density[gene]
        density_segments = np.zeros((n_codons, ))
        for left, right in segments :
            density_segments[left : right + 1] = np.mean(density[left : right + 1])
        scale = result.scale
        ax[0].plot(x, density, linewidth = 1.5, alpha = 0.60, color = color, label = legend)
        ax[1].plot(x, density_segments * scale, linewidth = 1.5, alpha = 0.60, color = color, label = legend)
    if show_labels :
        ax[1].legend(loc = 'best', ncol = 3, fontsize = 14, columnspacing = 0.1, bbox_to_anchor = (1.32, -0.1))
        ax[0].set_ylabel('Simulated\nrib. occupancy', fontsize = 15)
        ax[1].set_ylabel('Scaled avg.\nrib. occupancy', fontsize = 15)
    
    ax[0].set_xlim(show_left, show_right)
    ax[1].set_xlim(show_left, show_right)
    
    seq = lib.tree.get_codon_sequence(forest[gene])
    conditions = np.zeros((n_codons, ), dtype = np.bool)
    for codon in highlight_codons :
        conditions = np.logical_or(conditions, seq == codon)
    ymin0, ymax0 = ax[0].get_ylim()
    ymin1, ymax1 = ax[1].get_ylim()
    ax[0].set_ylim(ymin0, ymax0)
    if force_ymax is not None :
        ax[0].set_ylim(ymin0, force_ymax)
    ax[1].set_ylim(ymin1, ymax1)
    xt = []
    xt_labels = []
    for i in range(n_codons) :
        if conditions[i] and i < show_right and i > show_left :
            #ax[1].plot([i, i + 1], [ymin, ymin], color = 'gray', linewidth = 5.5)
            line = ax[0].plot([i, i], [-0.004, ymax0], color = 'gray', linewidth = 1, linestyle = '--')
            xt.append(i)
            xt_labels.append(seq[i])
            #line[0].set_clip_on(False)
            line[0].set_clip_on(True)
            ax[1].plot([i, i], [ymin1, ymax1], color = 'gray', linewidth = 1, linestyle = '--')
            #line = ax[0].axvline(x = i, linewidth = 1.0, color = '0.1', alpha = 0.6, linestyle = '--')
    ax[0].set_xticks(xt)
    ax[0].set_xticklabels(xt_labels, rotation = 90, va = 'top', ha = 'center', fontsize = 11, fontweight = 'bold')
    ax[0].set_title('%s' % gene, fontsize = 13, fontweight = 'bold')
    xlabels = np.linspace(show_left, show_right, 5)
    ax[1].set_xticks([int(t) for t in xlabels])
    #ax[0].set_yticks([])
    #ax[1].set_yticks([])
    for tick in ax[0].yaxis.get_major_ticks():
        #tick.label.set_visible(False)
        tick.label.set_fontsize(10)
    for tick in ax[1].yaxis.get_major_ticks():
        #tick.label.set_visible(False)
        tick.label.set_fontsize(10)
    #f.set_size_inches(19, 4.5, forward = True)
    #f.tight_layout()
    #f.subplots_adjust(hspace = 0.20)
    #plot.show()

def plot_density_example(gene_a, gene_b, forest, results, legends, fit_a = None, fit_b = None, segment_a = None, segment_b = None) :
    f, ax = plot.subplots(2, 2)
    _plot_density_example(ax[:, 0], gene_a, forest, results, legends, segment_a, fit = fit_a, force_ymax = 0.02)
    _plot_density_example(ax[:, 1], gene_b, forest, results, legends, segment_b, fit = fit_b, show_labels = False)
    f.set_size_inches(19, 5.5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.25, bottom = 0.15)
    plot.show()

def plot_density_scatterplots_for_length_groups(results, legends, groups, forest, log2 = True) :
    n_results = len(results)
    n_groups = len(groups)
    f, ax = plot.subplots(n_results, n_groups + 1, sharey = 'row', sharex = True)
    for i in range(n_groups + 1) :
        if i < n_groups :
            group = groups[i]
            group_left, group_right = group[0], group[1]
        else :
            group = None
        for j, cur, result, legend in zip(range(n_results), ax[:, i], results, legends) :
            genes = list(set(result.density.keys()) & set(forest.keys()))
            data, simulation = [], []
            for gene in genes :
                length_codons = forest[gene].length_codons
                for node in forest[gene].tree :
                    left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
                    if group is None and length_codons != right - left + 1 :
                        continue
                    if group is not None and (right - left + 1 < group_left or right - left + 1 >= group_right) :
                        continue
                    measured_segment_density = 2 ** ratio_a
                    simulated_segment_density = np.mean(result.density[gene][left : right + 1])
                    data.append(measured_segment_density)
                    simulation.append(simulated_segment_density)
            data, simulation = np.array(data), np.array(simulation)
            if log2 :
                data, simulation = np.log2(data), np.log2(simulation)
            r, p = scipy.stats.pearsonr(data, simulation)
            cur.text(0.05, 0.12, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = cur.transAxes, color = 'black', fontsize = 9)
            r, p = scipy.stats.spearmanr(data, simulation)
            cur.text(0.05, 0.02, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = cur.transAxes, color = 'black', fontsize = 9)
            cur.scatter(data, simulation, color = 'k', alpha = 0.1)
            if j == n_results - 1 :
                for tick in cur.xaxis.get_major_ticks():
                    tick.label.set_fontsize(7)
                if i == 5 :
                    if not log2 :
                        cur.set_xlabel('Measured density ratio', fontsize = 13)
                    else :
                        cur.set_xlabel('Measured density ratio, $\\log_2$', fontsize = 13)
            else :
                for tick in cur.xaxis.get_major_ticks():
                    tick.label.set_visible(False)
            if j == 0 :
                if group is not None :
                    cur.set_title('Length $%d\\leq l <%d$' % (group_left, group_right), fontsize = 10, fontweight = 'bold')
                else :
                    cur.set_title('Full-CDS', fontsize = 10, fontweight = 'bold')
            if i == 0 :
                cur.set_ylabel(legend, fontweight = 'bold', fontsize = 10)
                #if not log2 :
                #    cur.set_ylabel('Measured density ratio', fontsize = 9)
                #else :
                #    cur.set_ylabel('Measured density ratio, $\\log_2$', fontsize = 9)
                for tick in cur.yaxis.get_major_ticks():
                    tick.label.set_fontsize(7)
    if log2 :
        f.text(0.005, 0.5, 'Simulated density, $\\log_2$', va = 'center', rotation = 'vertical', fontsize = 13)
    else :
        f.text(0.005, 0.5, 'Simulated density', va = 'center', rotation = 'vertical', fontsize = 13)
    f.set_size_inches(19, 7, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.1, wspace = 0.15, left = 0.045)
    plot.show()

def plot_density_scatterplots(results, legends, forest, log2 = True) :
    n_results = len(results)
    f, ax = plot.subplots(1, n_results, sharey = True)
    for cur, result, legend in zip(ax, results, legends) :
        genes = list(set(result.density.keys()) & set(forest.keys()))
        data, simulation = [], []
        for gene in genes :
            for node in forest[gene].tree :
                left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
                measured_segment_density = 2 ** ratio_a
                simulated_segment_density = np.mean(result.density[gene][left : right + 1])
                data.append(measured_segment_density)
                simulation.append(simulated_segment_density)
        data, simulation = np.array(data), np.array(simulation)
        if log2 :
            data, simulation = np.log2(data), np.log2(simulation)
        r, p = scipy.stats.pearsonr(data, simulation)
        cur.text(0.55, 0.10, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = cur.transAxes, color = 'black', fontsize = 13)
        print legend
        print '[i] Pearson : r = %.3f (p-value %e)' % (r, p)
        r, p = scipy.stats.spearmanr(data, simulation)
        cur.text(0.55, 0.03, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = cur.transAxes, color = 'black', fontsize = 13)
        print '[i] Spearman : r = %.3f (p-value %e)' % (r, p)
        print ''
        cur.scatter(simulation, data, color = 'k', alpha = 0.1)
        if not log2 :
            cur.set_xlabel('Simulated density', fontsize = 15)
        else :
            cur.set_xlabel('Simulated density, $\\log_2$', fontsize = 15)
        cur.set_title(legend, fontweight = 'bold', fontsize = 16)
    if not log2 :
        ax[0].set_ylabel('Measured density ratio', fontsize = 15)
    else :
        ax[0].set_ylabel('Measured density ratio, $\\log_2$', fontsize = 15)
    f.set_size_inches(19, 4.6, forward = True)
    f.tight_layout()
    plot.show()

def plot_reconstruction_example(gene, mrna_1, rib_1, mrna_2, rib_2, forest, n_mrna_a = 10859192.0, n_mrna_b = 10911163.0, n_rib_a = 15983167.0, n_rib_b = 16173551.0) :
    f, ax = plot.subplots(1, 1, sharex = True)
    mrna_1, rib_1 = lib.tools.get_codon_array(mrna_1[gene]) / n_mrna_a, lib.tools.get_codon_array(rib_1[gene]) / n_rib_a
    mrna_2, rib_2 = lib.tools.get_codon_array(mrna_2[gene]) / n_mrna_a, lib.tools.get_codon_array(rib_2[gene]) / n_rib_b
    codon_length = len(mrna_1)
    tree = forest[gene].tree
    n_segments = len(tree)
    depth = np.zeros((n_segments,), dtype = np.int)
    for i in range(n_segments) :
        left_i, right_i, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = tree[i]
        for j in range(n_segments) :
            if i != j :
                left_j, right_j, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = tree[j]
                if left_i <= left_j and right_i >= right_j :
                    depth[j] += 1
    x = range(codon_length)
    width = 1.0
    ax.bar(x, rib_1, color = 'r', alpha = 0.45, edgecolor = 'r')
    ax.bar(x, -mrna_1, color = 'b', alpha = 0.45, edgecolor = 'b')
    ax.bar(x, rib_2, color = 'r', alpha = 0.45, edgecolor = 'r')
    ax.bar(x, -mrna_2, color = 'b', alpha = 0.45, edgecolor = 'b')
    ax2 = ax.twinx()
    max_depth = np.max(depth)
    segment_tree = np.zeros((max_depth + 1, codon_length))
    segment_tree[:] = float('nan')
    tree = np.array(tree)
    y = np.max(np.max(rib_1), np.max(rib_2))
    alpha = 0.9
    y /= 2.0
    step = y / (1.6 * max_depth)
    frac = 0.1
    ys = []
    for d in range(max_depth + 1) :
        layer = tree[depth == d]
        n_layer = len(layer)
        ys.append(y)
        for i in range(n_layer) :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = layer[i]
            segment_tree[d, left : right + 1] = ratio_a
            ax2.plot([left, right], [y, y], linewidth = 1.5, color = 'k', alpha = alpha)
            ax2.plot([left, left], [y - frac * step, y + frac * step], linewidth = 1.5, color = 'k', alpha = alpha)
            ax2.plot([right, right], [y - frac * step, y + frac * step], linewidth = 1.5, color = 'k', alpha = alpha)
        alpha -= 0.1
        y -= step
    _, segments = lib.tools.fit_segment_density(forest[gene])
    left_segments = []
    for segment in segments :
        found = False
        for i in range(n_segments) :
            left_i, right_i, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = tree[i]
            if left_i == segment[0] and right_i == segment[1] :
                found = True
                break
        if not found :
            left_segments.append(segment)
    left_segments = np.array(left_segments)
    n_left_segments = len(left_segments)
    left_segments_depth = np.zeros((n_left_segments, ), dtype = np.int)
    for i in range(n_left_segments) :
        left, right = left_segments[i]
        for j in range(n_segments) :
            left_j, right_j, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = tree[j]
            if left_j <= left and right_j >= right :
                left_segments_depth[i] += 1
    y = np.max(np.max(rib_1), np.max(rib_2))
    alpha = 0.9
    y /= 2.0
    for d in range(max_depth + 1) :
        layer = left_segments[left_segments_depth == d]
        for left, right in layer :
            ax2.plot([left, right], [y, y], linewidth = 1.5, color = 'k', alpha = alpha, linestyle = ':')
            ax2.plot([left, left], [y - frac * step, y + frac * step], linewidth = 1.5, color = 'k', alpha = alpha)
            ax2.plot([right, right], [y - frac * step, y + frac * step], linewidth = 1.5, color = 'k', alpha = alpha)
        alpha -= 0.1
        y -= step
    ax.set_xlim(-1, codon_length)
    #ax.axes.get_xaxis().set_visible(False)
    ax.set_xlabel('Position', fontsize = 16)
    ax2.set_xlim(-1, codon_length)
    ymin, ymax = ax.get_ylim()
    ax.set_ylim(ymin, ymax * 0.7)
    ax2.set_ylim(ax.get_ylim())
    ax2.set_yticks(ys)
    ax2.set_yticklabels(range(max_depth + 1))
    ax2.set_ylabel('Tree depth', fontsize = 15)
    ax2.tick_params(right = 'off')
    ax.tick_params(top = 'off', bottom = 'off')
    ax.set_title('Gene: %s' % gene, fontweight = 'bold')
    ax.axes.get_yaxis().set_ticks([])
    ax.set_ylabel('mRNA and ribosome density', fontsize = 16)
    ax2.text(233, 0.7 * 0.27 * ymax, '$x_{\\left[l_j, r_j\\right]}$', ha = 'center', va = 'top', fontsize = 19, bbox = {'facecolor' : 'white', 'alpha' : 0.92, 'pad' : 2, 'edgecolor' : 'white' })
    ax2.text(77, 0.7 * 0.20 * ymax, '$x_{\\left[l_k, r_k\\right]}$', ha = 'center', va = 'top', fontsize = 19, bbox = {'facecolor' : 'white', 'alpha' : 0.92, 'pad' : 2, 'edgecolor' : 'white' })
    f.set_size_inches(19, 5, forward = True)
    f.tight_layout()
    plot.show()

def evaluate_square_only(occupancy, tree, scale) :
    import lib.tree
    n_segments = len(tree)
    if not isinstance(occupancy, list) :
        n = lib.tree.CumSum(occupancy)
        segment_occupancy = []
        for i in xrange(n_segments) :
            node = tree[i]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
            segment_occupancy.append(n[left, right])
    else :
        segment_occupancy = occupancy
    
    likelihood = 0.0
    for i in xrange(n_segments) :
        node = tree[i]
        left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
        mu = ratio_average_log / math.log(math.exp(1.0), 2)
        length = right - left + 1
        mean_occupancy = segment_occupancy[i] / float(length)
        lnx = np.log(mean_occupancy)
        p =  -1.0 / (2 * sigma ** 2) * (lnx - mu + scale) ** 2
        likelihood += p
    return likelihood

def look_for_genes(res_a, res_b, forest, codons = ['GAC', 'TTG', 'CCA', 'CAA', 'GCC', 'GGT', 'GAT', 'TTT', 'CAG', 'GTG', 'ACG', 'CCT', 'CGA']) :
    genes = list(set(res_a.fitness.keys()) & set(res_b.fitness.keys()) & set(forest.keys()))
    scale_a, scale_b = np.log(res_a.scale), np.log(res_b.scale)
    results = []
    for gene in genes :
        seq = lib.tree.get_codon_sequence(forest[gene])
        length = len(seq)
        conditions = np.zeros((length, ), dtype = np.bool)
        for codon in codons :
            conditions = np.logical_or(conditions, seq == codon)
        avg = np.mean(conditions, dtype = np.float)
        total = np.sum(conditions, dtype = np.int)
        diff = res_a.fitness[gene] - res_b.fitness[gene] 
        likelihood_a, likelihood_b = evaluate_square_only(res_a.density[gene], forest[gene].tree, scale_b), evaluate_square_only(res_b.density[gene], forest[gene].tree, scale_a)
        diff_square = likelihood_a - likelihood_b
        init_a, init_b = lib.simulator.RateFunction.sigmoid(res_a.initiation[gene]), lib.simulator.RateFunction.sigmoid(res_b.initiation[gene])
        results.append((gene, diff, diff_square, avg, total, (init_a - init_b) / max(init_a, init_b)))
    return results

def try_srands(result, gene, n_srands = 10) :
    initiation = lib.simulator.RateFunction.sigmoid(result.initiation[gene])
    vector = result.vector
    res_all = []
    for i in range(n_srands) :
        print '[i] Srand = %d' % i
        srand = i + 1
        res = lib.persistent.simulate_gene(gene, initiation, vector, srand)
        res_all.append(res)
    return res_all

def plot_gene_fitness_difference(res_a, res_b, forest, plot_square = False, codons = ['GAC', 'TTG', 'CCA', 'CAA', 'GCC', 'GGT', 'GAT', 'TTT', 'CAG', 'GTG', 'ACG', 'CCT', 'CGA']) :
    genes = list(set(res_a.fitness.keys()) & set(res_b.fitness.keys()))
    differences, differences_square = [], []
    frequency = []
    scale_a, scale_b = np.log(res_a.scale), np.log(res_b.scale)
    t = []
    ff = []
    for gene in genes :
        seq = lib.tree.get_codon_sequence(forest[gene])
        length = len(seq)
        conditions = np.zeros((length, ), dtype = np.bool)
        for codon in codons :
            conditions = np.logical_or(conditions, seq == codon)
        freq = np.mean(conditions, dtype = np.float)
        t.append(np.sum(conditions, dtype = np.int))
        frequency.append(freq)
        diff = res_a.fitness[gene] - res_b.fitness[gene]
        differences.append(diff)
        likelihood_a, likelihood_b = evaluate_square_only(res_a.density[gene], forest[gene].tree, scale_b), evaluate_square_only(res_b.density[gene], forest[gene].tree, scale_a)
        differences_square.append(likelihood_a - likelihood_b)
    #f, ax = plot.subplots(1, 1)
    if plot_square :
        plot_differences = differences_square
    else :
        plot_differences = differences
    #ax.scatter(frequency, plot_differences)
    print scipy.stats.spearmanr(t, plot_differences)
    print scipy.stats.spearmanr(frequency, plot_differences)
    #ax.set_ylabel('Objective differences', fontsize = 16)
    #ax.set_xlabel('Fraction of significant codons', fontsize = 16)
    #f.tight_layout()
    #plot.show()
    return genes, differences, differences_square, t, frequency

def plot_ribosome_data(gene, mrna_1, rib_1, mrna_2, rib_2, results, legends, forest, fit = None, n_mrna_a = 10859192.0, n_mrna_b = 10911163.0, n_rib_a = 15983167.0, n_rib_b = 16173551.0) :
    from matplotlib import gridspec
    f = plot.figure()
    #f, ax = plot.subplots(2, 1, sharex = True)
    gs = gridspec.GridSpec(2, 1, height_ratios = [1.5, 1])
    ax = np.array([plot.subplot(gs[0]), plot.subplot(gs[1])])
    mrna_1, rib_1 = lib.tools.get_codon_array(mrna_1[gene]) / n_mrna_a, lib.tools.get_codon_array(rib_1[gene]) / n_rib_a
    mrna_2, rib_2 = lib.tools.get_codon_array(mrna_2[gene]) / n_mrna_a, lib.tools.get_codon_array(rib_2[gene]) / n_rib_b
    codon_length = len(mrna_1)
    tree = forest[gene].tree
    n_segments = len(tree)
    depth = np.zeros((n_segments,), dtype = np.int)
    for i in range(n_segments) :
        left_i, right_i, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = tree[i]
        for j in range(n_segments) :
            if i != j :
                left_j, right_j, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = tree[j]
                if left_i <= left_j and right_i >= right_j :
                    depth[j] += 1
    x = range(codon_length)
    width = 1.0
    ax[0].bar(x, rib_1, color = 'r', alpha = 0.45, edgecolor = 'r')
    ax[0].bar(x, -mrna_1, color = 'b', alpha = 0.45, edgecolor = 'b')
    ax[0].bar(x, rib_2, color = 'r', alpha = 0.45, edgecolor = 'r')
    ax[0].bar(x, -mrna_2, color = 'b', alpha = 0.45, edgecolor = 'b')
    ax2 = ax[0].twinx()
    max_depth = np.max(depth)
    segment_tree = np.zeros((max_depth + 1, codon_length))
    segment_tree[:] = float('nan')
    tree = np.array(tree)
    y = np.max(np.max(rib_1), np.max(rib_2))
    alpha = 0.9
    y /= 2.0
    step = y / (1.6 * max_depth)
    frac = 0.1
    ys = []
    for d in range(max_depth + 1) :
        layer = tree[depth == d]
        n_layer = len(layer)
        ys.append(y)
        for i in range(n_layer) :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = layer[i]
            segment_tree[d, left : right + 1] = ratio_a
            ax2.plot([left, right], [y, y], linewidth = 1.5, color = 'k', alpha = alpha)
            ax2.plot([left, left], [y - frac * step, y + frac * step], linewidth = 1.5, color = 'k', alpha = alpha)
            ax2.plot([right, right], [y - frac * step, y + frac * step], linewidth = 1.5, color = 'k', alpha = alpha)
        alpha -= 0.1
        y -= step
    ax[0].set_xlim(-1, codon_length)
    ax[1].set_xlim(-1, codon_length)
    ax[0].axes.get_xaxis().set_visible(False)
    ax2.set_xlim(-1, codon_length)
    ax2.set_ylim(ax[0].get_ylim())
    ax2.set_yticks(ys)
    ax2.set_yticklabels(range(max_depth + 1))
    ax2.set_ylabel('Tree depth', fontsize = 15)
    ax2.tick_params(right = 'off')
    ax[0].tick_params(top = 'off', bottom = 'off')
    #ax.set_title('Gene: %s' % gene, fontweight = 'bold')
    ax[0].axes.get_yaxis().set_ticks([])
    ax[0].set_ylabel('mRNA and ribosome density', fontsize = 16)
    ax3 = f.add_axes([.25, 0.80, .45, .11])
    ax3.set_title('Segment tree constraints', fontweight = 'bold', fontsize = 13)
    ax3.tick_params(top = 'off', bottom = 'off', left = 'off', right = 'off')
    ax3.set_ylabel('Tree depth', fontsize = 13)
    data = np.ma.masked_array(segment_tree)
    cax = ax3.imshow(data, aspect = 'auto', interpolation = 'nearest')
    cbar = f.colorbar(cax, shrink = 1.3)
    cbar.ax.tick_params(labelsize = 10)
    cbar.set_label('Density ratio, $\\log_2$', fontsize = 10, labelpad = -55)
    #ax.fill_between([188, 208], [0.0001, 0.]

    ax[1].tick_params(top = 'off', bottom = 'off', left = 'off', right = 'off')
    if fit is None :
        reconstructed_density, segments = lib.tools.fit_segment_density(forest[gene])
        ax[1].plot(x, reconstructed_density, linewidth = 2, label = 'Reconstruction', color = 'k')
    else :
        left, right, ci = fit
        segments = zip(left, right)
        #levels = [0.95, 0.90, 0.85, 0.80, 0.75, 0.70, 0.65, 0.60, 0.55, 0.50, 0.45, 0.40, 0.35, 0.30, 0.25, 0.20, 0.15, 0.1]
        levels = [0.1, 0.5, 0.9]
        density_hi, density_low = np.zeros((codon_length, )), np.zeros((codon_length, ))
        for k, alpha in enumerate(levels) :
            for i, segment in enumerate(segments) :
                left, right = segment
                density_low[left : right + 1] = ci[alpha][i][0]
                density_hi[left : right + 1] = ci[alpha][i][1]
            if k == len(levels) - 1 :
                ax[1].fill_between(x, density_hi, density_low, color = '0.1', alpha = 0.15)
            else :
                ax[1].fill_between(x, density_hi, density_low, color = '0.1', alpha = 0.15, linewidth = 0.001)
        import matplotlib.patches as mpatches
        patch = mpatches.Rectangle([0, 0], 1e-5, 1e-5, facecolor = '0.1', alpha = 0.15, edgecolor = None, linewidth = 0.001, label = 'Reconstruction')
        ax[1].add_patch(patch)
    params = { 'legend.fontsize' : 16 }
    plot.rcParams.update(params)
    ax[1].set_xlabel('Position', fontsize = 15)
    ax[1].set_ylabel('Scaled avg.\nribosome occupancy', fontsize = 15)
    if results is not None and legends is not None :
        for result, legend in zip(results, legends) :
            density = result.density[gene]
            density_segments = np.zeros((codon_length, ))
            for left, right in segments :
                density_segments[left : right + 1] = np.mean(density[left : right + 1])
            scale = result.scale
            ax[1].plot(x, density_segments * scale, linewidth = 1.5, label = legend, alpha = 0.60)
    ax[1].legend(loc = 'best', ncol = 3, fontsize = 13, columnspacing = 0.1)
    ax4 = f.add_axes([0, 0, 1, 1])
    ax4.set_frame_on(False)
    ax4.axes.get_yaxis().set_visible(False)
    ax4.axes.get_xaxis().set_visible(False)
    ax4.set_xlim(0, codon_length)
    ax4.set_ylim(0, 1)
    ax4.axvspan(99.8875, 116.808, ymin = 0.0964, ymax = 0.6464, alpha = 0.10, color = 'blue')
    ax4.fill_betweenx([0.6464, 0.796], [99.8875, 129.09], [116.808, 136.185], alpha = 0.05, color = 'blue')
    ax4.text(26.74, 0.94, 'A', fontsize = 14, fontweight = 'bold', va = 'top', ha = 'left')
    ax4.text(26.74, 0.41, 'C', fontsize = 14, fontweight = 'bold', va = 'top', ha = 'left')
    ax4.text(81, 0.92, 'B', fontsize = 14, fontweight = 'bold', va = 'top', ha = 'left')
    f.set_size_inches(19, 7, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.05)
    plot.show()

def plot_occupancy(genes, forest, results, legends, plot_segments = False, plot_reconstruction = False, log_scale = False, highlight_codons = ['GAC', 'TTG', 'CCA', 'CAA', 'GCC', 'GGT', 'GAT', 'TTT', 'CAG', 'GTG', 'ACG', 'CCT', 'CGA']) :
    if not isinstance(genes, list) :
        genes = [genes]

    global cur_pos
    cur_pos = 0
    n_genes = len(genes)
    gene = genes[cur_pos]

    def key_event(e) :
        global cur_pos

        if e.key == 'right' :
            if cur_pos < n_genes - 1 :
                cur_pos += 1
        elif e.key == 'left' :
            if cur_pos > 0 :
                cur_pos -= 1
        else :
            return
        gene = genes[cur_pos]

        ax.cla()
        reconstructed_density, segments = lib.tools.fit_segment_density(forest[gene])
        n_codons = forest[gene].length_codons
        x = range(n_codons)
        if plot_reconstruction :
            if log_scale :
                ax.plot(x, np.log(reconstructed_density), linewidth = 2.5, label = 'Reconstruction', color = 'k')
            else :
                ax.plot(x, reconstructed_density, linewidth = 2.5, label = 'Reconstruction', color = 'k')
        
        print '[i] Gene: %s' % gene
        for result, legend in zip(results, legends) :
            if result.has_key('initiation') :
                init = result.initiation[gene]
                init = lib.simulator.RateFunction.sigmoid(init)
                print '%25s : %.10f' % (legend, init)
            density = result.density[gene]
            density_segments = np.zeros((n_codons, ))
            for left, right in segments :
                density_segments[left : right + 1] = np.mean(density[left : right + 1])
            scale = result.scale
            plot_density = density if not plot_segments else density_segments
            if not log_scale :
                ax.plot(x, plot_density * scale, linewidth = 1.5, label = legend)
            else :  
                ax.plot(x, np.log(plot_density * scale), linewidth = 1.5, label = legend)

        params = { 'legend.fontsize' : 16 }
        plot.rcParams.update(params)
        ax.legend(loc = 'best', ncol = 3)
        ax.set_xlabel('Position', fontsize = 16)
        ax.set_ylabel('Ribosome occupancy/density', fontsize = 16)
        ax.set_title(gene, fontweight = 'bold', fontsize = 16)
        ax.set_xlim(0, n_codons - 1)
        seq = lib.tree.get_codon_sequence(forest[gene])
        length = len(seq)
        conditions = np.zeros((length, ), dtype = np.bool)
        for codon in highlight_codons :
            conditions = np.logical_or(conditions, seq == codon)
        ymin, _ = ax.get_ylim()
        for i in range(length) :
            if conditions[i] :
                ax.plot([i, i + 1], [ymin, ymin], color = 'gray', linewidth = 5.5)
        f.tight_layout()
        f.canvas.draw()

    f, ax = plot.subplots(1, 1)
    f.canvas.mpl_connect('key_press_event', key_event)
    event = SimpleNamespace()
    event.key = 'left'
    key_event(event)
    f.set_size_inches(19, 4.5, forward = True)
    plot.show()

def get_values(forest, type = 'ratio') :
    genes = forest.keys()
    values = {}
    for gene in genes :
        for node in forest[gene].tree :
            length_codons = forest[gene].length_codons
            for node in forest[gene].tree :
                left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
                length = right - left + 1
                if length_codons == length :
                    if type == 'ratio' :
                        values[gene] = 2 ** ratio_a
                    elif type == 'mrna' :
                        values[gene] = 2 ** mrna_a
                    elif type == 'rib' :
                        values[gene] = 2 ** rib_a
    return values

def check_groups(forest, groups, values, type = 'ratio', log_value = False, xlabel = '', density = False, n_bins = 60) :
    genes = forest.keys()
    n_groups = len(groups)
    if n_groups == 10 :
        f, ax = plot.subplots(3, 4)
        n_rows, n_cols = 3, 4
    else :
        f, ax = plot.subplot(1, n_groups)
        n_rows, n_cols = 1, n_groups
    k = 0
    row, col = 0, 0
    while k < n_groups :
        cur = ax[row][col]
        M, value = [], []
        for gene in genes :
            if not gene in values :
                continue
            for node in forest[gene].tree :
                left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
                length = right - left + 1
                if not (length >= groups[k][0] and length < groups[k][1]) :
                    continue
                if type == 'ratio' :    
                    M.append(ratio_m)
                elif type == 'mrna' :
                    M.append(mrna_m)
                elif type == 'rib' :
                    M.append(rib_m)
                if not log_value :
                    value.append(values[gene])
                else :
                    value.append(np.log2(values[gene]))
        if not density :
            cur.scatter(value, M, alpha = 0.1, color = 'k', s = 30)
            quant = scipy.stats.mstats.mquantiles(value, [0.01, 0.99])
            cur.set_xlim(quant[0], quant[1])
        else :
            H, xedges, yedges = np.histogram2d(value, M, bins = n_bins)
            H = np.rot90(H)
            H = np.flipud(H)
            H_masked = np.ma.masked_where(H == 0, H)
            im = cur.pcolormesh(xedges, yedges, H_masked)
            cbar = f.colorbar(im, ax = cur)
            cbar.ax.tick_params(labelsize = 8)
        cur.set_ylim(-3, 3)
        cur.set_title('$%d\\leq L<%d$' % (int(groups[k][0]), int(groups[k][1])), fontweight = 'bold', fontsize = 16)
        cur.set_ylabel('M, $\\log_2$', fontsize = 16)
        if xlabel == '' :
            if not log_value :
                label = ''
            else :
                label = '$\\log_2$'
        else :
            label = xlabel
            if log_value :
                label = label + ', $\\log_2$'
        cur.set_xlabel(label, fontsize = 16)
        k += 1
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    f.set_size_inches(21, 12, forward = True)
    f.tight_layout()
    f.subplots_adjust(wspace = 0.18, hspace = 0.37)
    plot.show()

def cluster_pars_profiles(profiles, tai, tasep, method = 'single', metric = 'euclidean', fixed_codon = 'GAA', highlight_codons = [], blue_codons = [], log2 = False, plot_diff = False) :
    codons = lib.tools.codon2int.keys()
    codons = [codon for codon in codons if not np.any(np.isnan(profiles[codon]))]
    
    values = [profiles[codon] for codon in codons]
    linkage = scipy.cluster.hierarchy.linkage(values, method = method, metric = metric)
    f = plot.figure()
    ax = f.add_subplot(2, 1, 2)
    dend = scipy.cluster.hierarchy.dendrogram(linkage, labels = codons, color_threshold = float('inf'))
    cluster_codons = dend['ivl']
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(12)
    n_codons = len(codons)
    for i in range(n_codons) :
        codon = codons[i]
        if codon == fixed_codon :
            ax.get_xticklabels()[i].set_color('red')
        elif codon in highlight_codons :
            ax.get_xticklabels()[i].set_color('green')
        elif codon in blue_codons :
            ax.get_xticklabels()[i].set_color('blue')
    
    #cluster_codons = [codon for codon in lib.tools.codon2int.keys() if codon not in ['TGA', 'TAA', 'TAG']]
    #diff = [abs(tai[codon] - tasep[codon]) for codon in cluster_codons]
    #pairs = zip(diff, cluster_codons)
    #pairs.sort(key = lambda x : x[0])
    #cluster_codons = [codon[1] for codon in pairs]
    ax = f.add_subplot(2, 1, 1)
    x = range(n_codons)
    if not log2 :
        if not plot_diff :
            ax.plot(x, [tai[codon] for codon in cluster_codons], 'k--', label = 'tAI rates')
            ax.plot(x, [tasep[codon] for codon in cluster_codons], 'k', label = 'TASEP rates')
            ax.set_ylabel('Elongation rates', fontsize = 16)
        else :
            ax.plot(x, [tai[codon] - tasep[codon] for codon in cluster_codons], 'k--', label = 'Diff.')
            ax.set_ylabel('Elongation rate diff. (tAI - TASEP)', fontsize = 16)
    else :
        if not plot_diff :
            ax.plot(x, [math.log(tai[codon], 2) for codon in cluster_codons], 'k--', label = 'tAI rates')
            ax.plot(x, [math.log(tasep[codon], 2) for codon in cluster_codons], 'k', label = 'TASEP rates')
            ax.set_ylabel('Elongation rates, $\\log_2$', fontsize = 16)
        else :
            ax.plot(x, [tai[codon] - tasep[codon] for codon in cluster_codons], 'k--', label = 'Diff. of logs')
            ax.set_ylabel('Elongation rate diff. (tAI - TASEP), $\\log_2$', fontsize = 16)
    ax.legend(loc = 'upper left')
    ax.set_xticks(x)
    ax.set_xticklabels(cluster_codons, rotation = 90)
    for i in range(n_codons) :
        codon = codons[i]
        if codon == fixed_codon :
            ax.get_xticklabels()[i].set_color('red')
        elif codon in highlight_codons :
            ax.get_xticklabels()[i].set_color('green')
        elif codon in blue_codons :
            ax.get_xticklabels()[i].set_color('blue')
    f.set_size_inches(19, 8, forward = True)
    f.tight_layout()
    plot.show()

def plot_pars_rates_correlation(tai, tasep, profiles, prefix = 6, postfix = 6, log2 = False, type = 'pearson') :
    codons = lib.tools.codon2int.keys()
    codons = [codon for codon in codons if not np.any(np.isnan(profiles[codon]))]
    profile_length = len(profiles['AAA'])
    blank_feature = {}
    tai = copy.deepcopy(tai)
    tasep = copy.deepcopy(tasep)
    for codon in codons :
        blank_feature[codon] = 0
        if log2 :
            tai[codon] = math.log(tai[codon], 2)
            tasep[codon] = math.log(tasep[codon], 2)
    tasep_corr, tai_corr = [], []
    for i in range(profile_length) :
        feature = copy.deepcopy(blank_feature)
        for codon in codons :
            feature[codon] += abs(profiles[codon][i])
        if type == 'pearson' or type == 'Pearson' :
            r_tai, p = scipy.stats.pearsonr([feature[codon] for codon in codons], [tai[codon] for codon in codons])
            r_tasep, p = scipy.stats.pearsonr([feature[codon] for codon in codons], [tasep[codon] for codon in codons])
        elif type == 'spearman' or type == 'Spearman' :
            r_tai, p = scipy.stats.spearmanr([feature[codon] for codon in codons], [tai[codon] for codon in codons])
            r_tasep, p = scipy.stats.spearmanr([feature[codon] for codon in codons], [tasep[codon] for codon in codons])
        tasep_corr.append(r_tasep)
        tai_corr.append(r_tai)
    f, ax = plot.subplots(1, 1)
    ax.plot(range(profile_length), tasep_corr, 'k', label = 'TASEP rates')
    ax.plot(range(profile_length), tai_corr, 'k--', label = 'tAI rates')
    ax.set_xlabel('Position, nt', fontsize = 16)
    ax.set_ylabel('Corr. coeff.', fontsize = 16)
    ax.set_xticks(range(prefix + postfix + 3))
    ax.set_xticklabels(range(-prefix, 3 + postfix + 1))
    ax.legend(loc = 'lower left')
    f.tight_layout()
    plot.show()

def regress_pars_scores(scores, results, forest, prefix = 6, postfix = 6) :
    codons = lib.tools.codon2int.keys()
    tai = lib.tools.get_RFM_rates()
    fitted = {}
    profiles = {}
    for codon in codons :
        fitted[codon] = lib.simulator.RateFunction.sigmoid(results.vector[lib.tools.codon2int[codon]])
    
    for codon in codons :
        profiles[codon] = []
    genes = list(set(results.names) & set(scores.keys()))
    for gene in genes :
        length_pars = len(scores[gene])
        length_seq = forest[gene].length_codons * 3
        if length_pars != length_seq :
            print '[i] Skipping %s (length mismatch PARS - %d, CDS - %d)' % (gene, length_pars, length_seq)
            continue
        seq_codon = lib.tree.get_codon_sequence(forest[gene])
        codon_length = len(seq_codon)
        for i in range(codon_length) :
            if i * 3 < prefix or (codon_length - i - 1) * 3 < postfix :
                continue
            start, stop = 3 * i - prefix, 3 * (i + 1) + postfix
            sub_array = scores[gene][start : stop]
            codon = seq_codon[i]
            profiles[codon].append(sub_array)
    for codon in codons :
        profiles[codon] = np.mean(profiles[codon], axis = 0)
    codons = [codon for codon in codons if not np.any(np.isnan(profiles[codon]))]

    from regression import LinearRegression
    reg = LinearRegression()
    tai_results = reg.cross_val([profiles[codon] for codon in codons], [tai[codon] for codon in codons], n_fold = 10)
    fitted_results = reg.cross_val([profiles[codon] for codon in codons], [fitted[codon] for codon in codons], n_fold = 10)
    difference_results = reg.cross_val([profiles[codon] for codon in codons], [tai[codon] - fitted[codon] for codon in codons], n_fold = 10)
    print '[i] tAI   : %.3f +/- %.3f' % (np.mean(tai_results), np.std(tai_results))
    print '[i] TASEP : %.3f +/- %.3f' % (np.mean(fitted_results), np.std(fitted_results))
    print '[i] Diff  : %.3f +/- %.3f' % (np.mean(difference_results), np.std(difference_results))

    return tai, fitted, profiles

def write_model_rates(results_init, results_elong, filename) :
    fout = open(filename, 'wb')
    genes = list(set(results_init.names) | set(results_elong.names))
    fout.write('Translation initiation and protein production rates (PPRs) for the TASEP-init and TASEP-elong models.\n')
    fout.write('"Effective initiation rate" was estimated as the number of successful initiation events divided by the total simulation time, whereas "initiation rate" gives the rate of initiation attempts.')
    fout.write('\n')
    fout.write('Gene\tTASEP-init initiation rate\tTASEP-init effective initiation rate\tTASEP-init PPR\tTASEP-elong initiation rate\tTASEP-elong effective initiation rate\tTASEP-elong PPR\n')
    for gene in genes :
        init_initiation_rate = lib.simulator.RateFunction.sigmoid(results_init.initiation[gene])
        init_effective_initiation_rate = results_init.effective_init[gene]
        init_production_rate = results_init.J[gene]
        elong_initiation_rate = lib.simulator.RateFunction.sigmoid(results_elong.initiation[gene])
        elong_effective_initiation_rate = results_elong.effective_init[gene]
        elong_production_rate = results_elong.J[gene]
        fout.write('%s\t%e\t%e\t%e\t%e\t%e\t%e\n' % (gene, init_initiation_rate, init_effective_initiation_rate, init_production_rate, elong_initiation_rate, elong_effective_initiation_rate, elong_production_rate))
    fout.close()

def read_pars_scores(filename_scores, filename_coordinates) :
    fin = open(filename_coordinates, 'rb')
    lines = fin.readlines()
    fin.close()
    coordinates = {}
    for line in lines :
        args = line.split('\t')
        gene, type, start, stop = args[0], args[1], int(args[2]), int(args[3])
        if type != 'CDS' :
            continue
        if not coordinates.has_key(gene) :
            coordinates[gene] = [(start, stop)]
        else :
            coordinates[gene].append((start, stop))

    fin = open(filename_scores, 'rb')
    lines = fin.readlines()
    fin.close()
    results = {}
    for line in lines :
        args = line.split('\t')
        gene = args[0]
        if not coordinates.has_key(gene) :
            continue
        length = int(args[1])
        pars = args[2].split(';')
        array = np.array([float(num) for num in pars])
        parts = []
        for start, stop in coordinates[gene] :
            parts.append(array[start - 1:stop])
        array = np.concatenate(parts)
        results[gene] = array
    return results

def plot_pars_score_background(forest, scores, prefix = 6, postfix = 6) :
    background = []
    genes = list(set(forest.keys()) & set(scores.keys()))
    for gene in genes :
        length_pars = len(scores[gene])
        length_seq = forest[gene].length_codons * 3
        if length_pars != length_seq :
            print '[i] Skipping %s (length mismatch PARS - %d, CDS - %d)' % (gene, length_pars, length_seq)
            continue
        codon_length = forest[gene].length_codons
        for i in range(codon_length) :
            if i * 3 < prefix or (codon_length - i - 1) * 3 < postfix :
                continue
            start, stop = 3 * i - prefix, 3 * (i + 1) + postfix
            sub_array = scores[gene][start : stop]
            background.append(sub_array)
    mean = np.mean(background, axis = 0)
    sem = scipy.stats.sem(background, axis = 0)
    f, ax = plot.subplots(1, 1)
    width = 0.35
    x = np.array(range(3 + prefix + postfix)) * width * 1.3 + 0.5 * width
    ax.bar(x, mean, width, yerr = sem, color = 'b', alpha = 0.45)
    ax.set_xlim(0, np.max(x) + 1.5 * width)
    ax.set_ylim(0, 0.6)
    ax.set_xticks(x + width / 2.0)
    ax.set_xticklabels(range(-prefix, 3 + postfix + 1))
    ax.set_xlabel('Position, nt', fontsize = 16)
    ax.set_ylabel('Average PARS score', fontsize = 16)
    print 'Mean: ', mean
    print 'SED: ', sem

    f.tight_layout()
    plot.show()

def plot_pars_score(forest, scores, prefix = 6, postfix = 6, highlight_codons = [], normalize = False) :
    profiles = {}
    codons = lib.tools.codon2int.keys()
    for codon in codons :
        profiles[codon] = []
    genes = list(set(forest.keys()) & set(scores.keys()))
    background = []
    for gene in genes :
        length_pars = len(scores[gene])
        length_seq = forest[gene].length_codons * 3
        if length_pars != length_seq :
            print '[i] Skipping %s (length mismatch PARS - %d, CDS - %d)' % (gene, length_pars, length_seq)
            continue
        seq_codon = lib.tree.get_codon_sequence(forest[gene])
        codon_length = len(seq_codon)
        for i in range(codon_length) :
            if i * 3 < prefix or (codon_length - i - 1) * 3 < postfix :
                continue
            start, stop = 3 * i - prefix, 3 * (i + 1) + postfix
            sub_array = scores[gene][start : stop]
            codon = seq_codon[i]
            profiles[codon].append(sub_array)
            background.append(sub_array)
    freq = {}
    for codon in codons :
        freq[codon] = len(profiles[codon]) / float(len(background))
        profiles[codon] = (np.mean(profiles[codon], axis = 0), scipy.stats.sem(profiles[codon], axis = 0))
    background = np.mean(background, axis = 0)
    
    f, ax = plot.subplots(8, 8)
    width = 0.35
    x = np.array(range(3 + prefix + postfix)) * width * 1.3 + 0.5 * width
    row, col = 0, 0
    for codon in codons :
        cur = ax[row][col]
        mean, std = profiles[codon]
        if np.all(np.isnan(mean)) or np.all(np.isnan(std)) :
            f.delaxes(cur)
        else :
            if col == 0 :
                cur.set_ylabel('Avg. PARS', fontsize = 13)
            if row == 7 :
                cur.set_xlabel('Position, nt', fontsize = 13)
            if not normalize :
                cur.bar(x, mean, width, color = 'b', alpha = 0.45)
            else :
                cur.bar(x, mean / background, width, color = 'b', alpha = 0.45)
            cur.set_xlim(0, np.max(x) + 1.5 * width)
            if not normalize :
                cur.set_ylim(-0.6, 1.9)
            else :
                cur.set_ylim(-3, 6.5)
            if codon not in highlight_codons :
                cur.set_title('%s (%.2f%%)' % (codon, freq[codon] * 100), fontweight = 'bold', fontsize = 9)
            else :
                cur.set_title('%s (%.2f%%)' % (codon, freq[codon] * 100), fontweight = 'bold', fontsize = 9, color = 'green')
            cur.set_xticks(x + width / 2.0)
            cur.set_xticklabels(range(-prefix, 3 + postfix + 1))
            for tick in cur.xaxis.get_major_ticks():
                tick.label.set_fontsize(8) 
            for tick in cur.yaxis.get_major_ticks():
                tick.label.set_fontsize(8)

        col += 1
        if col >= 8 : 
            col = 0 
            row += 1

    f.set_size_inches(21, 12, forward = True)
    f.tight_layout()
    f.subplots_adjust(wspace = 0.18, hspace = 0.45)
    plot.show()
   
    result = copy.deepcopy(profiles)
    for codon in codons :
        result[codon] = result[codon][0] / background
    return result

def plot_occupancy_average(results, array_a, array_b, shah, display_max = 500) :
    genes = results.names
    density = results.density
    shah_density = shah.density
    max_length = -1
    for gene in genes :
        length = len(density[gene])
        max_length = max(max_length, length)
    shah_max_length = -1
    for gene in shah_density :
        length = len(shah_density[gene])
        shah_max_length = max(shah_max_length, length)
    
    simulated_density = np.zeros((max_length,))
    real_density = np.zeros((max_length,))
    simulated_shah_density = np.zeros((shah_max_length,))
    density_count = np.zeros((max_length,))
    shah_density_count = np.zeros((shah_max_length,))
    for gene in genes :
        length = len(density[gene])
        simulated_density[:length] += density[gene] / np.mean(density[gene])
        density_count[:length] += np.ones(length)
        measured = lib.tools.get_codon_array(array_a[gene]) + lib.tools.get_codon_array(array_b[gene])
        real_density[:length] += measured / np.mean(measured)
    for gene in shah_density.keys() :
        length = len(shah_density[gene])
        #print '%s : len %d, max_len %d' % (gene, length, shah_max_length)
        simulated_shah_density[:length] += shah_density[gene] / np.mean(shah_density[gene])
        shah_density_count[:length] += np.ones(length)
    simulated_density /= density_count
    simulated_shah_density /= shah_density_count
    real_density /= density_count

    f, ax = plot.subplots(1, 1)
    x = range(max_length)
    ax.plot(x, simulated_density, color = 'b', linewidth = 1.5, label = 'TASEP density')
    ax.plot(x, real_density, color = 'r', linewidth = 1.5, label = 'Measured density')
    x = range(shah_max_length)
    ax.plot(x, simulated_shah_density, color = 'k', linewidth = 1.5, label = 'SMoPT density')
    ax.set_ylabel('Average normalized occupancy', fontsize = 16)
    ax.set_xlabel('Codon', fontsize = 16)
    ax.set_xlim((0, display_max))
    ax.set_ylim((0.8, 2.4))
    ax.legend()
    plot.show()

def plot_pwms(forest, pwm_length = 6, highlight_codons = [], normalize = False) :
    genes = forest.keys()
    nuc_to_int = {'A' : 0, 'C' : 1, 'T' : 2, 'G' : 3}
    colors = ['g', 'b', 'r', 'k']
    counts = {}
    codons = lib.tools.codon2int.keys()
    for codon in codons :
        counts[codon] = np.zeros((pwm_length, 4))
    
    for gene in genes :
        seq_codon = lib.tree.get_codon_sequence(forest[gene])
        codon_length = len(seq_codon)
        seq = ''.join(seq_codon)
        seq = [nuc_to_int[nuc] for nuc in seq]
        for i in range(codon_length) :
            codon = seq_codon[i]
            if 3 * i < pwm_length :
                continue
            prefix = seq[3 * i - pwm_length : 3 * i]
            for j in range(pwm_length) :
                counts[codon][j][prefix[j]] += 1
    
    global_normalizer = np.zeros((pwm_length, 4))
    for codon in codons :
        global_normalizer += counts[codon]
    normalizer = np.sum(global_normalizer, axis = 1)
    for i in range(4) :
        global_normalizer[:, i] = global_normalizer[:, i] / normalizer

    f, ax = plot.subplots(8, 8)
    width = 0.35
    x = np.array(range(pwm_length)) * width * 1.3 + 0.5 * width
    row, col = 0, 0
    for codon in codons :
        cur = ax[row][col]
        bottom = np.zeros((pwm_length, ))
        codon_counts = counts[codon]
        normalizer = np.sum(counts[codon], axis = 1)
        for i in range(4) :
            if not normalize :
                cur.bar(x, codon_counts[:, i] / normalizer, width, bottom = bottom / normalizer, color = colors[i], alpha = 0.45)
                bottom += codon_counts[:, i]
            else :
                cur.bar(x, (codon_counts[:, i] / normalizer) / global_normalizer[:, i], width, bottom = bottom, color = colors[i], alpha = 0.45)
                bottom += (codon_counts[:, i] / normalizer) / global_normalizer[:, i]
        cur.set_xlim(0, np.max(x) + 1.5 * width)
        if codon not in highlight_codons :
            cur.set_title('%s' % codon, fontweight = 'bold', fontsize = 10)
        else :
            cur.set_title('%s' % codon, fontweight = 'bold', fontsize = 10, color = 'green')
        cur.set_xticks(x + width / 2.0)
        cur.set_xticklabels(range(1, pwm_length + 1))
        for tick in cur.xaxis.get_major_ticks():
            tick.label.set_fontsize(8) 
        for tick in cur.yaxis.get_major_ticks():
            tick.label.set_fontsize(8)

        col += 1
        if col >= 8 : 
            col = 0 
            row += 1

    f.set_size_inches(21, 12, forward = True)
    f.tight_layout()
    plot.show()
    return counts

def plot_simulated_density(results, gene) :
    f, ax = plot.subplots(1, 1)
    width = 0.5
    array = results.density[gene]
    length = len(array)
    ind = np.array(range(length)) * width
    ax.bar(ind, array, width, color = 'b', alpha = 0.45)
    ax.set_xlim(0, length * width)
    ax.set_xlabel('Codon', fontsize = 16)
    ax.set_ylabel('Ribosome occupancy probability', fontsize = 16)
    f.tight_layout()
    plot.show()

def plot_ribosome_density(density, gene) :
    f, ax = plot.subplots(1, 1)
    width = 0.5
    array = density[gene].mrna.array
    length = len(array)
    ind = np.array(range(length)) * width
    ax.bar(ind, array, width, color = 'b', alpha = 0.45)
    ax.set_xlim(0, length * width)
    ax.set_xlabel('Nucleotide', fontsize = 16)
    ax.set_ylabel('Ribosome density', fontsize = 16)
    f.tight_layout()
    plot.show()

def plot_ribosome_density_replicates(density_a, density_b, gene) :
    f, ax = plot.subplots(1, 1)
    width = 0.5
    array_a = lib.tools.get_codon_array(density_a[gene])
    array_b = lib.tools.get_codon_array(density_b[gene])
    length = len(array_a)
    ind = np.array(range(length)) * width
    ax.bar(ind, array_a, width, color = 'b', alpha = 0.45, label = 'Replicate 1')
    ax.bar(ind, array_b, width, color = 'g', alpha = 0.45, label = 'Replicate 2')
    ax.set_xlim(0, length * width)
    ax.set_xlabel('Codon', fontsize = 16)
    ax.set_ylabel('Ribosome density', fontsize = 16)
    ax.set_title('%s' % gene, fontsize = 16, fontweight = 'bold')
    ax.legend(ncol = 2)
    f.tight_layout()
    plot.show()

def read_results(pattern, n_results = 5) :
    results = []
    for i in range(n_results) :
        res = lib.reader.read_pickle(pattern % i)
        results.append(res)
    return results

def _get_rates_from_results(results) :
    codons = [None] * 64
    for codon, codon_int in lib.tools.codon2int.iteritems() :
        codons[codon_int] = codon
    codons = np.array(codons)
    mask = np.logical_and(codons != 'TAG', np.logical_and(codons != 'TAA', codons != 'TGA'))
    n_results = len(results)
    results_rates = []
    for i in range(n_results) :
        res = results[i]
        rates = np.copy(res.vector)
        #rates = np.copy(res.best.x[1:])
        for j in range(len(rates)) :
            rates[j] = lib.simulator.RateFunction.sigmoid(rates[j])
        rates = rates[mask]
        results_rates.append(rates)
    avg_rates = scipy.stats.mstats.gmean(results_rates)
    return np.log2(avg_rates), codons[mask]

def _get_initiation_rates_from_results(result, genes = None) :
    if genes is None :
        genes = sorted(result.initiation.keys())
    n_genes = len(genes)
    init = np.zeros((n_genes,))
    for i in range(n_genes) :
        gene = genes[i]
        init[i] = lib.simulator.RateFunction.sigmoid(result.initiation[gene])
    return np.log2(init)

def plot_elongation_rate_comparison_histogram(results_ingolia, results_mcmanus, skip_stop = True, color_codon = 'GAA', threshold = 0.005) :
    rates = lib.tools.get_RFM_rates()
    codons = np.array(rates.keys())
    values = np.array([math.log(rates[codon], 2) for codon in codons])
    sortme = np.array([codons, values], dtype = np.dtype(object)).transpose()
    sortme = np.array(sorted(sortme, key = lambda x : -x[1])).transpose()
    codons = sortme[0, :]
    values = sortme[1, :]
    n_results = len(results_ingolia)
    results_rates_ingolia, results_rates_mcmanus = [], []
    for i in range(n_results) :
        cur_rates_ingolia, cur_rates_mcmanus = np.zeros((64,)), np.zeros((64,))
        for j in range(len(codons)) :
            codon = codons[j]
            cur_rates_ingolia[j] = math.log(lib.simulator.RateFunction.sigmoid(results_ingolia[i].vector[lib.tools.codon2int[codon]]), 2)
            cur_rates_mcmanus[j] = math.log(lib.simulator.RateFunction.sigmoid(results_mcmanus[i].vector[lib.tools.codon2int[codon]]), 2)
        results_rates_ingolia.append(cur_rates_ingolia)
        results_rates_mcmanus.append(cur_rates_mcmanus)
    results_rates_ingolia = np.array(results_rates_ingolia)
    results_rates_mcmanus = np.array(results_rates_mcmanus)
    ingolia_mean = np.mean(results_rates_ingolia, axis = 0)
    ingolia_std = np.std(results_rates_ingolia, axis = 0)
    mcmanus_mean = np.mean(results_rates_mcmanus, axis = 0)
    mcmanus_std = np.std(results_rates_mcmanus, axis = 0)
    if skip_stop :
        mask = np.logical_not(np.logical_or(np.logical_or(codons == 'TAG', codons == 'TAA'), codons == 'TGA'))
    else :
        mask = np.ones((len(codons),), dtype = np.bool)
    ingolia_mean, mcmanus_mean = ingolia_mean[mask], mcmanus_mean[mask]
    ingolia_std, mcmanus_std = ingolia_std[mask], mcmanus_std[mask]
    values = values[mask]
    codons = codons[mask]

    f, ax = plot.subplots(1, 1)
    width = 0.5
    n = len(ingolia_mean)
    ind = np.array(range(n)) * 1.5
    ingolia_rect = ax.bar(ind + width, ingolia_mean, width, color = 'b', alpha = 0.45, zorder = 5)
    ax.errorbar(ind + 3.0 * width / 2.0, ingolia_mean, ingolia_std, 0, capsize = 4, ls = 'none', color = 'black', elinewidth = 2, zorder = 7)
    mcmanus_rect = ax.bar(ind + 2.0 * width, mcmanus_mean, width, color = 'g', alpha = 0.45, zorder = 5)
    ax.errorbar(ind + 5.0 * width / 2.0, mcmanus_mean, mcmanus_std, 0, capsize = 4, ls = 'none', color = 'black', elinewidth = 2, zorder = 7)
    ax.plot(ind + width + width, values, color = 'orange', marker = 'x', linewidth = 1.5)
    ax.set_xlim(0, n * 1.5 + width)
    ax.set_ylim(-12.2, 0)
    ax.xaxis.set_ticks_position('top')
    ax.set_xticks(ind + width + width)
    ax.set_xticklabels(codons, rotation = 90, va = 'bottom')
    xax = ax.get_xaxis()
    xax.set_tick_params(pad = 4)
    ax.set_ylabel('Elongation rates, $\log_2$', fontsize = 16, labelpad = -8)
    ax.legend((ingolia_rect, mcmanus_rect), ('Ingolia dataset', 'Mcmanus dataset'), loc = 'lower left')
    f.tight_layout()
    f.subplots_adjust(top = 0.9)
    f.set_size_inches(19, 4.5, forward = True)
    
    pylab.show()

def plot_rate_comparison(results_ingolia, results_mcmanus, results_initiation_ingolia, results_initiation_mcmanus) :
    f, ax = plot.subplots(1, 2)
    rates_ingolia, codons = _get_rates_from_results(results_ingolia)
    rates_mcmanus, codons = _get_rates_from_results(results_mcmanus)
    #m, b = pylab.polyfit(rates_ingolia, rates_mcmanus, 1)
    #mcmanus_rates_predicted = pylab.polyval([m, b], rates_ingolia)
    #ax[0].plot(rates_ingolia, mcmanus_rates_predicted, color = 'b', alpha = 0.75)
    
    ax[0].scatter(rates_ingolia, rates_mcmanus, color = 'k')
    ax[0].set_xlabel('Ingolia elongation rates, $\\log_2$', fontsize = 16)
    ax[0].set_ylabel('McManus elongation rates, $\\log_2$', fontsize = 16)
    
    r, p = scipy.stats.spearmanr(rates_ingolia, rates_mcmanus)
    print '[i] Spearman correlation elongation: %.5f (p-value %g)' % (r, p)
    ax[0].text(0.57, 0.10, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[0].transAxes, color = 'black', fontsize = 16)
    r, p = scipy.stats.pearsonr(rates_ingolia, rates_mcmanus)
    print '[i] Pearson correlation elongation: %.5f (p-value %g)' % (r, p)
    ax[0].text(0.57, 0.05, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[0].transAxes, color = 'black', fontsize = 16)
    
    ax[0].set_xlim([-10, -2])
    ax[0].set_ylim([-12, -1])
    #for codon, x, y in zip(codons, rates_ingolia, rates_mcmanus) :
    #    plot.annotate(codon, xy = (x, y), xytext = (-5, 5), textcoords = 'offset points', ha = 'right', va = 'bottom')

    initiation_ingolia = _get_initiation_rates_from_results(results_initiation_ingolia)
    initiation_mcmanus = _get_initiation_rates_from_results(results_initiation_mcmanus)
    #m, b = pylab.polyfit(initiation_ingolia, initiation_mcmanus, 1)
    #mcmanus_initiation_predicted = pylab.polyval([m, b], initiation_ingolia)
    #ax[1].plot(initiation_ingolia, mcmanus_initiation_predicted, color = 'b', alpha = 0.75)
    
    ax[1].scatter(initiation_ingolia, initiation_mcmanus, color = 'k')
    ax[1].set_xlabel('Ingolia initiation rates, $\\log_2$', fontsize = 16)
    ax[1].set_ylabel('McManus initiation rates, $\\log_2$', fontsize = 16)
    
    r, p = scipy.stats.spearmanr(initiation_ingolia, initiation_mcmanus)
    print '[i] Spearman correlation initiation: %.5f (p-value %g)' % (r, p)
    ax[1].text(0.57, 0.10, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[1].transAxes, color = 'black', fontsize = 16)
    r, p = scipy.stats.pearsonr(initiation_ingolia, initiation_mcmanus)
    print '[i] Pearson correlation initiation: %.5f (p-value %g)' % (r, p)
    ax[1].text(0.57, 0.05, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[1].transAxes, color = 'black', fontsize = 16)
    
    ax[1].set_xlim([-20.5, 0.5])
    ax[1].set_ylim([-20.5, 0.5])
    
    f.set_size_inches(12, 5, forward = True)
    f.tight_layout()
    plot.show()

def plot_rate_comparison_mcmanus(results_mcmanus_ingolia, results_mcmanus_shah, results_initiation_mcmanus_ingolia, results_initiation_mcmanus_shah) :
    f, ax = plot.subplots(1, 2)
    rates_mcmanus_ingolia, codons = _get_rates_from_results(results_mcmanus_ingolia)
    rates_mcmanus_shah, codons = _get_rates_from_results(results_mcmanus_shah)
    #m, b = pylab.polyfit(rates_mcmanus_ingolia, rates_mcmanus_shah, 1)
    #mcmanus_shah_rates_predicted = pylab.polyval([m, b], rates_mcmanus_ingolia)
    #ax[0].plot(rates_mcmanus_ingolia, mcmanus_shah_rates_predicted, color = 'b', alpha = 0.75)
    
    ax[0].scatter(rates_mcmanus_ingolia, rates_mcmanus_shah, color = 'k')
    ax[0].set_xlabel('Elongation rates, McManus-Ingolia, $\\log_2$', fontsize = 16)
    ax[0].set_ylabel('Elongation rates, Mcmanus-Shah, $\\log_2$', fontsize = 16)
    
    r, p = scipy.stats.spearmanr(rates_mcmanus_ingolia, rates_mcmanus_shah)
    print '[i] Spearman correlation elongation: %.5f (p-value %g)' % (r, p)
    ax[0].text(0.57, 0.10, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[0].transAxes, color = 'black', fontsize = 16)
    r, p = scipy.stats.pearsonr(rates_mcmanus_ingolia, rates_mcmanus_shah)
    print '[i] Pearson correlation elongation: %.5f (p-value %g)' % (r, p)
    ax[0].text(0.57, 0.05, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[0].transAxes, color = 'black', fontsize = 16)
    ax[0].set_xlim([-12, -1])
    ax[0].set_ylim([-12, -2])
    #for codon, x, y in zip(codons, rates_ingolia, rates_mcmanus) :
    #    plot.annotate(codon, xy = (x, y), xytext = (-5, 5), textcoords = 'offset points', ha = 'right', va = 'bottom')

    if results_initiation_mcmanus_ingolia is not None and results_initiation_mcmanus_shah is not None :
        genes = list(set(results_initiation_mcmanus_ingolia.initiation.keys()) & set(results_initiation_mcmanus_shah.initiation.keys()))
        initiation_mcmanus_ingolia = _get_initiation_rates_from_results(results_initiation_mcmanus_ingolia, genes)
        initiation_mcmanus_shah = _get_initiation_rates_from_results(results_initiation_mcmanus_shah, genes)
        #m, b = pylab.polyfit(initiation_mcmanus_ingolia, initiation_mcmanus_shah, 1)
        #mcmanus_shah_initiation_predicted = pylab.polyval([m, b], initiation_mcmanus_ingolia)
        #ax[1].plot(initiation_mcmanus_ingolia, mcmanus_shah_initiation_predicted, color = 'b', alpha = 0.75)
    
        ax[1].scatter(initiation_mcmanus_ingolia, initiation_mcmanus_shah, color = 'k')
    ax[1].set_xlabel('Initiation rates, McManus-Ingolia, $\\log_2$', fontsize = 16)
    ax[1].set_ylabel('Initiation rates, McManus-Shah, $\\log_2$', fontsize = 16)
    
    if results_initiation_mcmanus_ingolia is not None and results_initiation_mcmanus_shah is not None :
        r, p = scipy.stats.spearmanr(initiation_mcmanus_ingolia, initiation_mcmanus_shah)
        print '[i] Spearman correlation initiation: %.5f (p-value %g)' % (r, p)
        ax[1].text(0.57, 0.10, 'Spearman $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[1].transAxes, color = 'black', fontsize = 16)
        r, p = scipy.stats.pearsonr(initiation_mcmanus_ingolia, initiation_mcmanus_shah)
        print '[i] Pearson correlation initiation: %.5f (p-value %g)' % (r, p)
        ax[1].text(0.57, 0.05, 'Pearson    $r=%.2f$' % r, verticalalignment = 'bottom', horizontalalignment = 'left', transform = ax[1].transAxes, color = 'black', fontsize = 16)
    ax[1].set_xlim([-21, 1])
    ax[1].set_ylim([-21, -1])
    
    f.set_size_inches(12, 5, forward = True)
    f.tight_layout()
    plot.show()

def _count_offsets(offsets, length) :
    result = np.zeros((length,))
    for offset in offsets :
        result[offset] += 1
    return result / np.sum(result)

def plot_orf_offsets(offset_atg1, offset_atg2, offset_stop1, offset_stop2, lengths = None, save_name = None) :
    if lengths is None :
        lengths = range(27, 34)
    n_lengths = len(lengths)
    f, ax = plot.subplots(n_lengths, 2, sharex = False, sharey = True)
    width = 0.55
    max_length = np.max(lengths)
    ind = np.array(range(max_length)) * 2.5 * width + width
    ax[0][0].set_title('Start codon', fontsize = 16, fontweight = 'bold')
    ax[0][1].set_title('Stop codon', fontsize = 16, fontweight = 'bold')
    for i in range(n_lengths) :
        length = lengths[i]
        ax[i][0].set_xticks(ind + width)
        ax[i][1].set_xticks(ind + width)
        ax[i][0].set_xticklabels(range(max_length))
        ax[i][1].set_xticklabels(range(max_length))
        ax[i][0].set_xlim([0, np.max(ind) + 3 * width])
        ax[i][1].set_xlim([0, np.max(ind) + 3 * width])
        ax[i][0].set_ylabel('Length %d' % length, fontsize = 14, fontweight = 'bold')
       
        count_atg1, count_atg2 = _count_offsets(offset_atg1[length], max_length), _count_offsets(offset_atg2[length], max_length)
        count_stop1, count_stop2 = _count_offsets(offset_stop1[length], max_length), _count_offsets(offset_stop2[length], max_length)
        ax[i][0].bar(ind, count_atg1, width, color = 'b', alpha = 0.45, label = 'Replicate 1')
        ax[i][0].bar(ind + width, count_atg2, width, color = 'g', alpha = 0.45, label = 'Replicate 2')
        ax[i][1].bar(ind, count_stop1, width, color = 'b', alpha = 0.45)
        ax[i][1].bar(ind + width, count_stop2, width, color = 'g', alpha = 0.45)
        if i == 0 :
            ax[i][0].legend()
    ax[n_lengths - 1][0].set_xlabel('Codon start offset, nt', fontsize = 16)
    ax[n_lengths - 1][1].set_xlabel('Codon start offset, nt', fontsize = 16)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.20, wspace = 0.03)
    f.text(0.04, 0.5, 'Fraction of reads', va = 'center', rotation = 'vertical', fontsize = 16)
    width = 9.6
    height = 2.5
    f.set_size_inches(width * 2, height * n_lengths, forward = True)
    if save_name is not None :
        f.savefig(save_name)
    plot.show()

def compute_elongation_rate_correlations(pattern, n_results = 5, log2 = False) :
    results = []
    codons = [None] * 64
    for codon, codon_int in lib.tools.codon2int.iteritems() :
        codons[codon_int] = codon
    codons = np.array(codons)
    mask = np.logical_and(codons != 'TAG', np.logical_and(codons != 'TAA', codons != 'TGA'))
    for i in range(n_results) :
        res = lib.reader.read_pickle(pattern % i)
        rates = np.copy(res.vector)
        for j in range(len(rates)) :
            rates[j] = lib.simulator.RateFunction.sigmoid(rates[j])
        results.append(rates[mask])
    avg_correlation = 0
    avg_correlation_count = 0
    correlation = np.zeros((n_results, n_results))
    for i in range(n_results) :
        for j in range(i, n_results) :
            if not log2 :
                correlation[i][j] = scipy.stats.spearmanr(results[i], results[j])[0]
            else :
                correlation[i][j] = scipy.stats.pearsonr(np.log2(results[i]), np.log2(results[j]))[0]
            correlation[j][i] = correlation[i][j]
            if j > i :
                avg_correlation += correlation[i][j]
                avg_correlation_count += 1
    avg_correlation /= avg_correlation_count
    mean_rates = np.array(scipy.stats.mstats.gmean(results))
    print 'Average correlation: %.5f' % avg_correlation
    print 'Correlation matrix:'
    print correlation
    return mean_rates, mask

def evaluate_cross_validation(forest, folds, colors = None) :
    codons = lib.tools.codon2int.keys()
    n_folds = len(folds)
    fold_counts = []
    for i in range(n_folds) :
        codon_array = np.zeros((64,), dtype = np.float)
        fold_counts.append(codon_array)
    total_counts = np.zeros((64,), dtype = np.float)
    for i in range(n_folds) :
        for gene in folds[i] :
            seq = lib.tree.get_codon_sequence(forest[gene])
            for codon in codons :
                cmp = seq == codon
                tree = forest[gene]
                for node in tree.tree :
                    left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
                    if np.any(cmp[left:right + 1]) :
                        fold_counts[i][lib.tools.codon2int[codon]] += 1
                        total_counts[lib.tools.codon2int[codon]] += 1
    f, ax = plot.subplots(1, 1)
    width = 0.45
    if colors is None :
        colors = ['b', 'r', 'y', 'g', 'c', 'm']
    ind = np.array(range(64)) * 1.7 * width + width
    offset = np.zeros((64,), dtype = np.float)
    bars = []
    for i in range(n_folds) :
        bar = ax.bar(ind, fold_counts[i] / total_counts, width, bottom = offset / total_counts, color = colors[i], alpha = 0.45)
        offset += fold_counts[i]
        bars.append(bar)
    ax.set_xlim((0, np.max(ind) + 2 * width))
    ax.set_xticks(ind)
    codons_permute = [None] * 64
    for codon in codons :
        codons_permute[lib.tools.codon2int[codon]] = codon
    ax.set_xticklabels(codons_permute, rotation = 90)
    ax.set_ylabel('Fraction of segments', fontsize = 16)
    ax.set_xlabel('Codons', fontsize = 16)
    f.set_size_inches(19, 4.5, forward = True)
    f.tight_layout()
    plot.show()

def check_enrichment(names, values, mi, ma, cutoff = 0.05, is_initiation = False) :
    names = np.array(names)
    if is_initiation :
        values2 = []
        for gene in names :
            values2.append(values[gene])
        values = np.array(values2)
        for i in range(values.size) :
            values[i] = lib.simulator.RateFunction.sigmoid(values[i])
    else :
        values = np.copy(np.array(values))
    selected = np.logical_and(values >= mi, values < ma)
    print '[i] Genes selected: %d' % np.sum(selected)
    enrichment = lib.david.enrich(names[selected], names)
    for en in enrichment :
        if en.afdr < cutoff :
            print '%s : %.5f (%d genes)' % (en.term_name, en.afdr, en.list_hits)

def plot_limitation(relative_J, n_bins = 30, start_cutoff = 0.02, lower_cutoff = 0.08, middle_cutoff = 0.11, enrichment_text_lower = None, enrichment_text_middle = None, enrichment_text_upper = None) :
    f, ax = plot.subplots(1, 1)
    relative_J = np.array(relative_J)
    mi, ma = np.min(relative_J), np.max(relative_J)
    len_start, len_lower, len_middle, len_upper = start_cutoff - mi, lower_cutoff - start_cutoff, middle_cutoff - lower_cutoff, ma - middle_cutoff
    print len_start
    print len_lower
    print len_middle
    print len_upper
    n_start = int(round(n_bins * len_start / (len_start + len_lower + len_middle + len_upper)))
    n_lower = int(round(n_bins * len_lower / (len_start + len_lower + len_middle + len_upper)))
    n_middle = int(round(n_bins * len_middle / (len_start + len_lower + len_middle + len_upper)))
    n_upper = int(round(n_bins * len_upper / (len_start + len_lower + len_middle + len_upper)))
    bins_start = np.linspace(mi, start_cutoff, n_start, endpoint = False)
    bins_lower = np.linspace(start_cutoff, lower_cutoff, n_lower, endpoint = False)
    bins_middle = np.linspace(lower_cutoff, middle_cutoff, n_middle, endpoint = False)
    bins_upper = np.linspace(middle_cutoff, ma, n_upper)
    bins = np.concatenate((bins_start, bins_lower, bins_middle, bins_upper))
    n, bins, patches = ax.hist(relative_J, bins, normed = True)
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax.set_xlabel('Relative change in PPR', fontsize = 16)
    ax.set_ylabel('Fraction of genes', fontsize = 16)
    plot.setp(ax.get_yticklabels(), visible = False)
    for thisbin, thispatch in zip(bins, patches) :
        if thisbin < start_cutoff :
            continue
        elif thisbin < lower_cutoff :
            if enrichment_text_lower is not None :
                thispatch.set_facecolor('r')
        elif thisbin < middle_cutoff :
            if enrichment_text_middle is not None :
                thispatch.set_facecolor('y')
        else :
            if enrichment_text_upper is not None :
                thispatch.set_facecolor('g')
    xmax = ax.get_xlim()
    ymax = ax.get_ylim()
    ax.set_ylim(0.00, ymax[1] + 0.55)
    ax.set_xlim(-0.01, xmax[1] + 0.01)

    ymax = (ymax[0], ymax[1] + 0.55)
    
    if enrichment_text_lower is not None :
        pos_y = ymax[1] - 10.7
        ann = ax.annotate(enrichment_text_lower,
                  xy = (0.00, 0.9), xycoords = 'data',
                  xytext = (0.003, 14), textcoords = 'data',
                  size = 11, va = "top", ha = "left",
                  bbox = dict(boxstyle = "square", fc = "w", alpha = 0.45),
                  #arrowprops = dict(arrowstyle = "-|>",
                  #                connectionstyle = "arc3, rad=0.0",
                  #                fc = "w", lw = 1.5), 
                  )
        ann = ax.annotate('',
                xy = (0.073, 7.72), xycoords = 'data',
                xytext = (0.06, 7.72), textcoords = 'data',
                arrowprops = dict(arrowstyle = "-|>",
                                connectionstyle = "arc3, rad=0.0",
                                lw = 1.5)
                )
    
    if enrichment_text_middle is not None :
        pos_y = ymax[1] - 4.7
        ann = ax.annotate(enrichment_text_middle,
                  xy = (0.078, 15.2), xycoords = 'data',
                  xytext = (0.03, 20.5), textcoords = 'data',
                  size = 11, va = "top", ha = "left",
                  bbox = dict(boxstyle = "square", fc = "w", alpha = 0.45),
                  #arrowprops = dict(arrowstyle = "-|>",
                  #                connectionstyle = "arc3, rad=0.0",
                  #                fc = "w", lw = 1.5),
                  )
        ann = ax.annotate('',
                xy = (0.083, 20.075), xycoords = 'data',
                xytext = (0.066, 20.075), textcoords = 'data',
                arrowprops = dict(arrowstyle = "-|>",
                                connectionstyle = "arc3, rad=0.0",
                                lw = 1.5)
                )

    if enrichment_text_upper is not None :
        pos_y = ymax[1] - 0.7
        ann = ax.annotate(enrichment_text_upper,
                  xy = (0.13, 4), xycoords = 'data',
                  xytext = (0.168, pos_y), textcoords = 'data',
                  size = 11, va = "top", ha = "right",
                  bbox = dict(boxstyle = "square", fc = "w", alpha = 0.45),
                  #arrowprops = dict(arrowstyle = "-|>",
                  #                connectionstyle = "arc3, rad=0.0",
                  #                lw = 1.5),
                  )
        ann = ax.annotate('',
                xy = (0.13, 4), xycoords = 'data',
                xytext = (0.13, 23.58), textcoords = 'data',
                arrowprops = dict(arrowstyle = "-|>",
                                connectionstyle = "arc3, rad=0.0",
                                lw = 1.5)
                )
                
    
    f.set_size_inches(8, 6, forward = True)
    f.tight_layout()
    plot.show()

def plot_JQ(result) :
    genes = result.names
    Js, Qs, inits = [], [], []
    for gene in genes :
        Js.append(result.J[gene])
        Qs.append(result.Q[gene])
        inits.append(lib.simulator.RateFunction.sigmoid(result.initiation[gene]))
    f, ax = plot.subplots(1, 1)
    ax.scatter(np.log2(inits), np.log2(Qs))
    ax.set_xlabel('Initiation rate, $\\log_2$', fontsize = 16)
    ax.set_ylabel('Q, $\\log_2$', fontsize = 16)
    f.tight_layout()
    plot.show()

def _parallel_detemine_limitation(param) :
    import lib.persistent
    gene, rates, initiation_before, srand = param
    initiation_after = initiation_before * 1.1
    J_before, J_after = [], []
    Q_before, Q_after = [], []
    for sr in srand :
        coef_after, density_after, J, Q, effective_init = lib.persistent.simulate_gene(gene, initiation_before, rates, sr)
        J_before.append(J)
        Q_before.append(Q)
        coef_after, density_after, J, Q, effective_init = lib.persistent.simulate_gene(gene, initiation_after, rates, sr)
        J_after.append(J)
        Q_after.append(Q)
    J_before, J_after, Q_before, Q_after = np.mean(J_before), np.mean(J_after), np.mean(Q_before), np.mean(Q_after)
    percent_J = (J_after - J_before) / J_before
    percent_Q = (Q_after - Q_before) / Q_before
    return percent_J, percent_Q, J_before, J_after, Q_before, Q_after


def determine_limitation(result, factor = 1.1, srand = None, client_url = None) :
    import lib.persistent
    genes = result.names
    rates = result.vector
    if client_url is not None :
        client = Client(client_url)
    else :
        client = None
    if srand is None :
        srand = [result.srand]
    percent_Js, percent_Qs = [], []
    before_Js, after_Js = [], []
    before_Qs, after_Qs = [], []
    if client is None :
        for gene in genes :
            initiation_before = lib.simulator.RateFunction.sigmoid(result.initiation[gene])
            initiation_after = initiation_before * 1.1
            J_before, J_after = [], []
            Q_before, Q_after = [], []
            for sr in srand :
                coef_after, density_after, J, Q, effective_init = lib.persistent.simulate_gene(gene, initiation_before, rates, sr)
                J_before.append(J)
                Q_before.append(Q)
                coef_after, density_after, J, Q, effective_init = lib.persistent.simulate_gene(gene, initiation_after, rates, sr)
                J_after.append(J)
                Q_after.append(Q)
            J_before, J_after, Q_before, Q_after = np.mean(J_before), np.mean(J_after), np.mean(Q_before), np.mean(Q_after)
            percent_J = (J_after - J_before) / J_before
            percent_Q = (Q_after - Q_before) / Q_before
            print '%10s : J_rel = %.3f, Q_rel = %.3f' % (gene, percent_J, percent_Q)
            percent_Js.append(percent_J)
            percent_Qs.append(percent_Q)
            before_Js.append(J_before)
            after_Js.append(J_after)
            before_Qs.append(Q_before)
            after_Qs.append(Q_after)
    else :
        view = client.load_balanced_view()
        view.retries = 100
        params = []
        for gene in genes :
            initiation_before = lib.simulator.RateFunction.sigmoid(result.initiation[gene])
            params.append((gene, rates, initiation_before, srand))
        results = view.map(_parallel_detemine_limitation, params, block = True)
        for percent_J, percent_Q, J_before, J_after, Q_before, Q_after in results :
            percent_Js.append(percent_J)
            percent_Qs.append(percent_Q)
            before_Js.append(J_before)
            after_Js.append(J_after)
            before_Qs.append(Q_before)

    return (np.array(genes[:]), np.array(percent_Js), np.array(percent_Qs), np.array(before_Js), np.array(after_Js), np.array(before_Qs), np.array(after_Qs))

def check_densities(results, forest) :
    import lib.persistent
    vector = np.copy(results.vector)
    genes = results.names
    srand = results.srand

    for gene in genes :
        print '%s' % gene
        print '---------------'
        init = lib.simulator.RateFunction.sigmoid(results.initiation[gene])
        coef, density, J, Q, effective_init = lib.persistent.simulate_gene(gene, init, vector, srand)
        for node in forest[gene].tree :
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
            before, after = np.mean(results.density[gene][left:right + 1]), np.mean(density[left:right + 1])
            ratio = abs(before - after) / after
            print '%.8f -> %.8f (%.8f)' % (before, after, ratio)

def plot_shah_convergence(abundance_filename, filename_prefix, time_range, folds, forest, means = None, stds = None) :
    if means is None or stds is None :
        means, stds = [], []
        for time in time_range :
            shah = lib.evaluator.read_shah_output(abundance_filename, filename_prefix + str(time), time)
            mean, std = lib.evaluator.evaluate_profiles_cv(shah, folds, forest)
            means.append(mean)
            stds.append(std)
    f, ax = plot.subplots(1, 1)
    time_range, means, stds = np.array(time_range, dtype = np.float), np.array(means), np.array(stds)
    ax.plot(time_range, means, 'r-', linewidth = 1.5)
    ax.fill_between(time_range, means + stds, means - stds, facecolor = 'blue', alpha = 0.45)
    ax.set_ylabel('Objective, $\\psi$', fontsize = 16)
    ax.set_xlabel('Simulation time, sec', fontsize = 16)
    ax.set_xlim(np.min(time_range), np.max(time_range))
    f.tight_layout()
    plot.show()
    return (means, stds)

def attach_production_rates(results) :
    import lib.persistent
    genes = results.names
    results.J = {}
    results.Q = {}
    results.effective_init = {}
    srand = results.srand
    rates = np.copy(results.vector)
    for gene in genes :
        initiation = lib.simulator.RateFunction.sigmoid(results.initiation[gene])
        coef, density, J, Q, effective_init = lib.persistent.simulate_gene(gene, initiation, rates, srand)
        results.J[gene] = J
        results.Q[gene] = Q
        results.effective_init[gene] = effective_init
        print '%10s : J = %.8f, Q = %.8f, effective init. = %.8f (diff = %.8f)' % (gene, J, Q, effective_init, effective_init - J)

def plot_initiation_rates(results, n_bins = 50) :
    genes = results.names
    
    initiations = []
    for gene in genes :
        init = lib.simulator.RateFunction.sigmoid(results.initiation[gene])
        initiations.append(init)

    initiations = np.array(initiations)
    initiations = np.log2(initiations)
    f, ax = plot.subplots(1, 1)
    n, bins, patches = ax.hist(initiations, n_bins, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax.set_xlabel('Initiation rate, $\\log_2$', fontsize = 16)
    ax.set_ylabel('Fraction of genes', fontsize = 16)
    pylab.show()

def offer_cmp(a, b) :
    if a[1] + a[2] < b[1] + b[2] :
        return 1
    else :
        return -1

def offer_genes(results_a, results_b, results_shah, results_zhang) :
    found = []
    for gene in results_a.names :
        if gene in results_b.names and gene in results_shah.density and gene in results_zhang.density :
            a, b, shah, zhang = results_a.fitness[gene], results_b.fitness[gene], results_shah.fitness[gene], results_zhang.fitness[gene]
            if b > a and a > shah and shah > zhang :
                entry = (gene, b, a, shah, zhang)
                found.append(entry)
    print 'Found: %d entries' % len(found)
    found = list(sorted(found, cmp = offer_cmp))
    return found

def plot_coverage_poster(gene, cov_a = None, cov_b = None, y_max = 150) :
    f, ax = plot.subplots(1, 1, sharey = True)
    if cov_a is not None :
        cov_a = cov_a[gene]
        n_codons = len(cov_a)
    if cov_b is not None :
        cov_b = cov_b[gene]
        n_codons = len(cov_b)
    edges = np.array(range(1, n_codons + 2))
    centers = (edges[1:] + edges[:-1]) / 2.0
    if cov_a is not None and cov_b is not None :
        bad_ind = np.logical_or(np.logical_or(np.isinf(cov_a), np.isinf(cov_b)), np.logical_or(np.isnan(cov_a), np.isnan(cov_b)))
    elif cov_a is not None :
        bad_ind = np.logical_or(np.isinf(cov_a), np.isnan(cov_a))
    elif cov_b is not None :
        bad_ind = np.logical_or(np.isinf(cov_b), np.isnan(cov_b))
    if cov_a is not None :
        cov_a[bad_ind] = 0
    if cov_b is not None :
        cov_b[bad_ind] = 0
    
    if cov_a is not None :
        ax.hist(centers, edges, weights = cov_a, histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 1')
    if cov_b is not None :
        ax.hist(centers, edges, weights = cov_b, histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 2')
    
    ax.set_xlim(1, n_codons + 1)
    ax.set_ylim(0, y_max)
    ax.set_xlabel('Position', fontsize = 16)
    ax.set_ylabel('Ribosome coverage', fontsize = 16)
    if cov_a is not None and cov_b is not None :
        ax.legend(loc = 'best', ncol = 3)
    
    f.tight_layout()
    plot.show()

def plot_coverage(gene, cov_a, cov_b, results_a = None, results_b = None, results_shah = None, results_zhang = None, forest = None, L = 10, offset = -5, xmax = None, ymax = None, log_scale = False, frac = 0.04, xshift = 40) :
    f, ax = plot.subplots(1, 1, sharey = True)
    cov_a, cov_b = cov_a[gene], cov_b[gene]
    n_codons = len(cov_a)
    edges = np.array(range(1, n_codons + 2))
    centers = (edges[1:] + edges[:-1]) / 2.0
    
    bad_ind = np.logical_or(np.logical_or(np.isinf(cov_a), np.isinf(cov_b)), np.logical_or(np.isnan(cov_a), np.isnan(cov_b)))
    cov_a[bad_ind] = 0
    cov_b[bad_ind] = 0
    if not log_scale :
        ax.hist(centers, edges, weights = cov_a, histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 1')
        ax.hist(centers, edges, weights = cov_b, histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 2')
    else :
        ax.hist(centers, edges, weights = np.log2(cov_a), histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 1')
        ax.hist(centers, edges, weights = np.log2(cov_b), histtype = 'stepfilled', alpha = 0.45, label = 'Replicate 2')
    
    #cov = np.sqrt(cov_a * cov_b)
    #ax.hist(centers, edges, weights = cov, histtype = 'stepfilled', alpha = 0.45)
    ax.set_xlim(1, n_codons + 1)

    if results_zhang is not None :
        density = np.copy(results_zhang.density[gene])
        scale = results_zhang.scale
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(scale * coverage), linewidth = 1.5, color = 'g', label = 'Zhang model')
        else :
            ax.plot(centers, scale * coverage, linewidth = 1.5, color = 'g', label = 'Zhang model')
    
    if results_shah is not None :
        density = np.copy(results_shah.density[gene])
        scale = results_shah.scale
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(scale * coverage), linewidth = 1.5, color = 'k', label = 'SMoPT')
        else :
            ax.plot(centers, scale * coverage, linewidth = 1.5, color = 'k', label = 'SMoPT')
    
    if results_a is not None :
        density = np.copy(results_a.density[gene])
        scale = results_a.scale
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(scale * coverage), 'b', linewidth = 1.5, label = 'TASEP$^\\mathrm{init}$')
        else :
            ax.plot(centers, scale * coverage, 'b', linewidth = 1.5, label = 'TASEP$^\\mathrm{init}$')
    
    if results_b is not None :
        density = np.copy(results_b.density[gene])
        scale = results_b.scale
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(scale * coverage), 'r', linewidth = 1.5, label = 'TASEP$^\\mathrm{elong}$')
        else :
            ax.plot(centers, scale * coverage, 'r', linewidth = 1.5, label = 'TASEP$^\\mathrm{elong}$')
    
    if forest is not None :
        density, segments = lib.tools.fit_segment_density(forest[gene])
        coverage = np.zeros((n_codons, ))
        for i in range(n_codons) :
            coverage[i] = np.sum(density[max(i + offset, 0) : min(i + L + offset, n_codons)])
        if log_scale :
            ax.plot(centers, np.log2(coverage), 'orange', linewidth = 1.5, label = 'MAP reconstruction')
        else :
            ax.plot(centers, coverage, 'orange', linewidth = 1.5, label = 'MAP reconstruction')
   
    params = { 'legend.fontsize' : 16, 'legend.linewidth' : 1.5, 'xtick.major.pad' : -10}
    plot.rcParams.update(params)
    ax.legend(loc = 'best', ncol = 3)
    if ymax is not None :
        ax.set_ylim(0, ymax)
    if xmax is not None :
        ax.set_xlim(0, xmax)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 16)
    #plot.setp(ax.get_yticklabels(), visible = False)
    if log_scale :
        ax.set_ylabel('Ribosome coverage, $\\log_2$', fontsize = 16)
    else :
        ax.set_ylabel('Ribosome coverage', fontsize = 16)
    ax.set_xlabel('Position', fontsize = 16)
    
    ymax = ax.set_ylim()
    ymax = ymax[1]
    ymax_original = ymax

    if results_zhang is not None :
        ymax -= frac * ymax_original 
        ax.text(xshift, ymax, '$\\psi=%.2f$' % results_zhang.fitness[gene], fontsize = 18, color = 'g')
    if results_shah is not None :
        ymax -= frac * ymax_original 
        ax.text(xshift, ymax, '$\\psi=%.2f$' % results_shah.fitness[gene], fontsize = 18, color = 'k')
    if results_a is not None :
        ymax -= frac * ymax_original 
        ax.text(xshift, ymax, '$\\psi=%.2f$' % results_a.fitness[gene], fontsize = 18, color = 'b')
    if results_b is not None :
        ymax -= frac * ymax_original 
        ax.text(xshift, ymax, '$\\psi=%.2f$' % results_b.fitness[gene], fontsize = 18, color = 'r')
    
    f.tight_layout()
   # f.subplots_adjust(left = None, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)
    plot.show()

def plot_elongation_rates(results, additional = None, skip_stop = True, highlight = -1, color_codon = 'GAA', threshold = 0.05, threshold_std = 1.5) :
    rates = lib.tools.get_RFM_rates()
    codons = np.array(rates.keys())
    values = np.array([math.log(rates[codon], 2) for codon in codons])
    sortme = np.array([codons, values], dtype = np.dtype(object)).transpose()
    sortme = np.array(sorted(sortme, key = lambda x : -x[1])).transpose()
    codons = sortme[0, :]
    values = sortme[1, :]
    n_results = len(results)
    results_rates = []
    for i in range(n_results) :
        #cur_rates = np.copy(results[i].best.x[1:])
        cur_rates = np.zeros((64,))
        for j in range(len(codons)) :
            codon = codons[j]
            cur_rates[j] = math.log(lib.simulator.RateFunction.sigmoid(results[i].best.x[lib.tools.codon2int[codon] + 1]), 2)
        results_rates.append(cur_rates)

    if additional is not None :
        additional_rates = np.zeros((64,))
        for j in range(len(codons)) :
            codon = codons[j]
            additional_rates[j] = math.log(lib.simulator.RateFunction.sigmoid(additional.best.x[lib.tools.codon2int[codon] + 1]), 2)
        additional = additional_rates
    
    results_rates = np.array(results_rates)
    mean = np.mean(results_rates, axis = 0)
    std = np.std(results_rates, axis = 0)
    #std = scipy.stats.sem(results_rates, axis = 0)
    if skip_stop :
        good_ind = np.logical_not(np.logical_or(np.logical_or(codons == 'TAG', codons == 'TAA'), codons == 'TGA'))
        mean = mean[good_ind]
        std = std[good_ind]
        values = values[good_ind]
        codons = codons[good_ind]
        if additional is not None :
            additional = additional[good_ind]
        new_results_rates = []
        for i in range(n_results) :
            #results_rates[i] = results_rates[i][good_ind]
            new_results_rates.append(results_rates[i][good_ind])
        results_rates = new_results_rates
    results_rates = np.array(results_rates)
    median = np.median(results_rates, axis = 0)
    
    f, ax = plot.subplots(1, 1)
    width = 0.5
    n = len(mean)
    ind = np.array(range(n))
    ax.bar(ind + width, mean, width, color = 'b', alpha = 0.45, zorder = 5)
    std_low = np.log2(1 - 2 ** (std - mean))
    std_high = np.log2(1 + 2 ** (std - mean))
    #print sw
    #print std_high
    ax.errorbar(ind + 3.0 * width / 2.0, mean, std, 0, capsize = 4, ls = 'none', color = 'black', elinewidth = 2, zorder = 7)
    colors = ['b', 'k', 'y', 'g', 'c']
#    for i in range(n_results) :
#        if i == highlight :
#            ax.scatter(ind + width + width / 2.0, results_rates[i], color = 'r', zorder = 15)
#        else :
#            #ax.scatter(ind + width + width / 2.0, results_rates[i], color = 'k', zorder = 10)
#            ax.scatter(ind + width + width / 2.0, results_rates[i], color = colors[i], zorder = 10)
#    if highlight < 0 :
#        ax.scatter(ind + width + width / 2.0, median, color = 'r', zorder = 15)
    ax.plot(ind + width + width / 2.0, values, color = 'orange', marker = 'x', linewidth = 1.5)
    if additional is not None :
        ax.plot(ind + width + width / 2.0, additional, color = 'red', marker = 'x', linewidth = 1.5)
    ax.set_xlim(0, n +  width)
    ax.set_ylim(-12.2, 0)
    ax.xaxis.set_ticks_position('top')
    ax.set_xticks(ind + width + width / 2.0)
    ax.set_xticklabels(codons, rotation = 90, va = 'bottom')
    xax = ax.get_xaxis()
    xax.set_tick_params(pad = 4)
    ax.set_ylabel('Elongation rates, $\log_2$', fontsize = 16, labelpad = -8)
    #ax.set_xlabel('Codons', fontsize = 16)
    f.tight_layout()
    f.subplots_adjust(top = 0.9)
    f.set_size_inches(19, 4.5, forward = True)

    pvalues = []
    print 'Codon\ttAI-based\tMean\tStd. dev.\tMedian\tp-value'
    for i in range(n) :
        codon = codons[i]
        t, p = scipy.stats.ttest_1samp(results_rates[:, i], values[i])
        if p < threshold :
            ax.get_xticklabels()[i].set_color('green')
        elif std[i] < threshold_std :
            ax.get_xticklabels()[i].set_color('blue')
    for i in range(n) :
        t, p = scipy.stats.ttest_1samp(results_rates[:, i], values[i])
        color = 'black'
        if p < threshold :
            color = 'green'
        elif std[i] < threshold_std :
            color = 'blue'
        if codons[i] == color_codon :
            color = 'red'
        if color != 'black' :
            print '%s-%s\t%.3f\t%.3f\t%.3f\t%.3f\t%.2f' % (color, codons[i], values[i], mean[i], std[i], median[i], p)
        else :
            print '%s\t%.3f\t%.3f\t%.3f\t%.3f\t%.2f' % (codons[i], values[i], mean[i], std[i], median[i], p)

        #print '\\textcolor{%s}{%s} & $%.3f$ & $%.3f$ & $%.3f$ & $%.3f$ & $%.2f$' % (color, codons[i], values[i], mean[i], std[i], median[i], p),
    
    if color_codon is not None :
        color_codon = np.where(codons == color_codon)[0]
        ax.get_xticklabels()[color_codon].set_color('red')
    pylab.show()

def get_compute_time(genes, runtime, scale, number_of_solutions =  1) :
    times = []
    for gene in genes :
        for i in range(number_of_solutions) :
            times.append(runtime[gene])
    times = sorted(times)
    node_time = np.zeros((scale,))
    i = 0
    n = len(times)
    while i < n :
        j = 0
        while j < scale and i < n :
            node_time[j] += times[i]
            i += 1
            j += 1
    return np.max(node_time)

def plot_runtime_scaling(times_approximation, times_simulation, folds, scales = None, number_of_solutions = 1, xmin = 1, xmax = 400, ymax = 30) :
    app = {}
    sim = {}
    for i in range(len(times_approximation[0])) :
        app[times_approximation[0][i]] = times_approximation[1][i]
    for i in range(len(times_simulation[0])) :
        sim[times_simulation[0][i]] = times_simulation[1][i]
    runtime = {}
    genes = []
    for fold in folds :
        genes += fold
    for gene in genes :
         runtime[gene] = app[gene] + sim[gene]
    n_folds = len(folds)
    times = []
    for i in range(n_folds) :
        times.append([])
    if scales is None :
        scales = [1] + range(10, 1000, 50)
    for i in range(n_folds) :
        for scale in scales :
            times[i].append(get_compute_time(folds[i], runtime, scale, number_of_solutions) / 60.0)
    f, ax = plot.subplots(1, 1)
    for i in range(n_folds) :
        ax.plot(scales, times[i], label = 'Fold %d' % (i + 1))
    ax.set_xlabel('Number of engines', fontsize = 16)
    ax.set_ylabel('Runtime per fold, min', fontsize = 16)
    ax.legend(loc = 'upper right')
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(0, ymax)
    plot.show()

def count_codon_presence(forest, genes, color = None) :
    codon_count_genes = np.zeros((64,))
    codon_count_segments = np.zeros((64,))
    codons = lib.tools.codon2int.keys()
    n_genes = len(genes)
    n_segments = 0
    for gene in genes :
        seq = lib.tree.get_codon_sequence(forest[gene])
        n_segments += len(forest[gene].tree)
        for codon in codons :
            cmp = seq == codon
            if np.any(cmp) :
                codon_count_genes[lib.tools.codon2int[codon]] += 1
                tree = forest[gene]
                for node in tree.tree :
                    left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
                    if np.any(cmp[left:right + 1]) :
                        codon_count_segments[lib.tools.codon2int[codon]] += 1
    f, ax = plot.subplots(2, 1)
    width = 0.35
    ind = np.array(range(64))
    codon_count_genes = codon_count_genes / float(n_genes)
    codon_count_segments = codon_count_segments / float(n_segments)
    ax[0].bar(ind + width, codon_count_genes, width, color = 'b', alpha = 0.45)
    ax[1].bar(ind + width, codon_count_segments, width, color = 'b', alpha = 0.45)
    ax[0].set_xlim(0, 64 + 2 * width)
    ax[0].set_ylim(0, 1)
    ax[1].set_xlim(0, 64 + 2 * width)
    ax[1].set_ylim(0, 1)
    ax[0].set_xticks(ind + width + width / 2.0)
    ax[1].set_xticks(ind + width + width / 2.0)
    codons_permute = [None] * 64
    for codon in codons :
        codons_permute[lib.tools.codon2int[codon]] = codon
    ax[0].set_xticklabels(codons_permute, rotation = 90)
    ax[1].set_xticklabels(codons_permute, rotation = 90)
    if color is not None :
        color = np.where(np.array(codons_permute) == color)[0]
        ax[0].get_xticklabels()[color].set_color('red')
        ax[1].get_xticklabels()[color].set_color('red')
    ax[0].set_ylabel('Fraction of genes', fontsize = 16)
    ax[1].set_ylabel('Fraction of segments', fontsize = 16)
    ax[1].set_xlabel('Codons', fontsize = 16)
    f.set_size_inches(14, 5.0, forward = True)
    f.tight_layout()
    pylab.show()

def simulate_scaled(gene, initiation = None, srand = 101317) :
    import lib.persistent
    rates = lib.tools.get_RFM_rates()
    rate_vector = np.zeros((64,))
    for codon in rates.keys() :
        rate_vector[lib.tools.codon2int[codon]] = rates[codon]
    if initiation is None :
        initiation = np.percentile(rate_vector, 0.1)
    rate_vector_x1 = np.zeros((64,))
    rate_vector_x025 = np.zeros((64,))
    rate_vector_x05 = np.zeros((64,))
    for i in range(64) :
        rate_vector_x1[i] = lib.simulator.RateFunction.logit(rate_vector[i])
        rate_vector_x025[i] = lib.simulator.RateFunction.logit(rate_vector[i] * 0.25)
        rate_vector_x05[i] = lib.simulator.RateFunction.logit(rate_vector[i] * 0.5)
    coef_x1, density_x1, J_x1, _, _ = lib.persistent.simulate_gene(gene, initiation, rate_vector_x1, srand)
    coef_x025, density_x025, J_x025, _, _ = lib.persistent.simulate_gene(gene, initiation * 0.25, rate_vector_x025, srand)
    coef_x05, density_x05, J_x05, _, _ = lib.persistent.simulate_gene(gene, initiation * 0.5, rate_vector_x05, srand)
    x = range(1, len(density_x1) + 1)
    f, ax = plot.subplots(3, 1, sharex = True)
    ax[0].plot(x, density_x05, linewidth = 1.5)
    ax[1].plot(x, density_x1, linewidth = 1.5)
    ax[2].plot(x, density_x025, linewidth = 1.5)
    ax[0].set_ylabel('$\\times\\,1.00$', fontsize = 16)
    ax[1].set_ylabel('$\\times\\,0.50$', fontsize = 16)
    ax[2].set_ylabel('$\\times\\,0.25$', fontsize = 16)
    ax[0].set_title('Simulation with scaled rates (%s)' % gene, fontsize = 16, fontweight = 'bold')
    ax[2].set_xlabel('Codon position', fontsize = 16)
    ax[0].set_xlim(1, len(density_x1) + 1)
    ax[1].set_xlim(1, len(density_x1) + 1)
    ax[2].set_xlim(1, len(density_x1) + 1)
    f.text(0.07, 0.5, 'Ribosome occupancy', va = 'center', rotation = 'vertical', fontsize = 16)
    pylab.show()

def plot_time_distribution(results, n_bins = 40) :
    f, ax = plot.subplots(1, 1)
    n, bins, patches = ax.hist(results, bins = n_bins, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    ax.set_xlabel('Runtime, sec', fontsize = 16)
    #ax.set_xlabel('Fold speedup', fontsize = 16)
    ax.set_ylabel('Fraction of genes', fontsize = 16)
    plot.setp(ax.get_yticklabels(), visible = False)
    pylab.show()

def compare_densities(genes, segments, full, threshold = None) :
    import lib.persistent
    n = len(genes)
    selected_genes = []
    results = []
    for i in xrange(n) :
        gene = genes[i]
        tree = lib.persistent.forest[gene]
        segment_J = segments[i].J
        full_J = full[i].J
        n_nodes = len(tree.tree)
        diff = abs(full_J - segment_J)
        for j in xrange(n_nodes) :
            node = tree.tree[j]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, node_sigma = node
            
            if isinstance(segments[i].density, np.ndarray) :
                segment_density = np.mean(segments[i].density[left:right + 1])
            else :
                segment_density = segments[i].density[j] / (right - left + 1)
            
            if isinstance(full[i].density, np.ndarray) :
                full_density = np.mean(full[i].density[left:right + 1])
            else :
                full_density = full[i].density[j] / (right - left + 1)
            diff = max(diff, abs(segment_density - full_density))
            avg = (segment_density + full_density) * 0.5
        if threshold is None or diff >= threshold :
            selected_genes.append(gene)
            results.append(diff)
            print '%9s : %g (%.2f %%)' % (gene, diff, diff / avg * 100.0)
    return (selected_genes, results)

def compute_densities(genes, eps = None, srand = 101317) :
    import lib.persistent
    results = []
    for gene in genes :
        simulator = lib.persistent.simulators[gene]
        simulator.initiation = 0.2
        results.append(simulator.simulate(eps = eps, srand = srand))
    return (genes, results)

def profile_simulation(genes, n_reps = 3, burn = 1000, eps = 1e-3, srand = 101317) :
    import yep
    yep.start('simulator.prof')
    times = []
    cnt, n = 1, len(genes)
    for gene in genes :
        gene_time = np.mean(timeit.repeat(stmt = 'lib.persistent.simulators["%s"].simulate(srand = %d, eps = %g, burn = %d)' % (gene, srand, eps, burn), setup = 'import lib.persistent', number = 1, repeat = n_reps))
        times.append(gene_time)
        print '[%d/%d] %9s : %.4f' % (cnt, n, gene, gene_time)
        cnt += 1
    yep.stop()
    return (genes, times)

def time_execution(genes, n_reps = 3, burn = 1000, eps = 1e-3, srand = 101317) :
    times = []
    cnt, n = 1, len(genes)
    for gene in genes :
        gene_time = np.mean(timeit.repeat(stmt = 'lib.persistent.simulators["%s"].simulate(srand = %d, eps = %g, burn = %d)' % (gene, srand, eps, burn), setup = 'import lib.persistent', number = 1, repeat = n_reps))
        times.append(gene_time)
        print '[%d/%d] %9s : %.4f' % (cnt, n, gene, gene_time)
        cnt += 1
    return (genes, times)

def time_execution_init(genes, scale = 2.76, n_reps = 3, srand = 101317) :
    import numpy as np
    import timeit
    import lib.tools
    import lib.simulator
    import json
    times = []
    rates = lib.tools.get_RFM_rates()
    rate_vector = np.zeros((64,))
    for codon in rates.keys() :
        rate_vector[lib.tools.codon2int[codon]] = lib.simulator.RateFunction.logit(rates[codon])
    if not isinstance(genes, list) :
        genes = [genes]
    cnt, n = 1, len(genes)
    for gene in genes :
        gene_time = np.mean(timeit.repeat(stmt = "lib.approximation.approximate_initiation('%s', rates, %.15f, srand = %d)" % (gene, scale, srand), setup = "import lib.approximation; import json; import numpy as np; rates = np.array(json.loads('%s'));" % json.dumps(rate_vector.tolist()), number = 1, repeat = n_reps))
        times.append(gene_time)
        print '[%d/%d] %9s : %.4f' % (cnt, n, gene, gene_time)
        cnt += 1
    return (genes, times)

def plot_tree_size_histogram(forest, genes, max_value = 30, n_bins = 35) :
    meas = []
    for gene in genes :
        meas.append(len(forest[gene].tree))
    meas = sorted(meas)
    meas = np.array(meas)
    edges = range(max_value + 1)
    counts = []
    for i in range(max_value - 1) :
        counts.append(np.sum(meas == i + 1))
    counts.append(np.sum(meas >= max_value))
    f, ax = plot.subplots(1, 1)
    edges = np.array(edges)
    counts = np.array(counts)
    x = (edges[1:] + edges[:-1]) / 2.0
    n, bins, patches = ax.hist(x, bins = edges, weights = counts, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    
    n = meas.shape[0]
    per_bin = n / float(n_bins)
    int_pos_prev = 0
    pos = 0
    while pos < n :
        pos = min(per_bin + pos, n)
        int_pos = int(pos)
        if int_pos < n :
            val = meas[int_pos]
        else :
            val = meas[n - 1] + 1
        prev_val = meas[int_pos_prev]
        indices = (meas >= prev_val) * (meas < val)
        bin_data = meas[indices]
        if len(bin_data) > 0 :
            print '[%4d, %4d) : %d genes' % (prev_val, val, len(bin_data))
            if val <= max_value :
                ax.plot([val - 1, val - 1], [0, 1], 'k--')
        int_pos_prev = int_pos
    
    ax.set_ylim(0, 0.26)
    ax.set_xlabel('# of segments', fontsize = 16)
    ax.set_ylabel('Fraction of genes', fontsize = 16)
    pylab.show()
    return meas

def evaluate_approximation(params, x) :
    n = len(x)
    res = np.zeros((n, ))
    a, b, c, d, e = tuple(params)
    for i in range(n) :
        if x[i] > e :
            res[i] = c * x[i] + d
        else :
            res[i] = a  * x[i] / (b + x[i])
    return res

def plot_approximation_schematic_example(app, max_init = 0.0024, n_steps = 200, y_max = 0.1) :
    left, right, mid = app[0]
    guess_left, guess_right, density, params = app[1]
    left_points, right_points, left_values, right_values = app[2]

    init = np.linspace(1e-6, max_init, n_steps)
    f, ax = plot.subplots(2, 1)
    f_minus, f_plus = [], []
    for value in init :
        f_minus.append(params[0] * value / (params[1] + value))
        f_plus.append(params[2] * value + params[3])
    f_plus = np.array(f_plus)
    f_minus = np.array(f_minus)
    ax[1].plot(init[init <= left], f_minus[init <= left], 'g-', linewidth = 1.5)
    ax[1].plot(init[np.logical_and(init <= right, init > left)], f_minus[np.logical_and(init <= right, init > left)], 'g--', linewidth = 1.5)
   
    ax[1].plot(init[np.logical_and(init >= left, init < right)], f_plus[np.logical_and(init >= left, init < right)], 'b--', linewidth = 1.5)
    ax[1].plot(init[init >= right], f_plus[init >= right], 'b-', linewidth = 1.5)

    ax[1].plot([left, left], [0, 1], 'k--')
    ax[1].plot([right, right], [0, 1], 'k--')
    
    pos = params[0] * mid / (params[1] + mid)
    ax[1].plot([mid, mid], [0, pos], 'k--')
    pos = params[2] * mid  + params[3]
    ax[1].plot([mid, mid], [pos, 1.0], 'k--')
    
    ax[1].plot([left, right], [density, density], 'r--', linewidth = 1.5)
    #ax[1].annotate('$k_0 = l$', xy = (left, y_max / 10.0), xytext = (-10, 0), textcoords = 'offset points', ha = 'center', va = 'bottom', rotation = 'vertical', fontsize = 16)
    #ax[1].annotate('$k_0 = r$', xy = (right, y_max / 10.0), xytext = (-10, 0), textcoords = 'offset points', ha = 'center', va = 'bottom', rotation = 'vertical', fontsize = 16)
    #ax[1].annotate('$k_0 = m$', xy = (mid, y_max / 10.0), xytext = (-10, 0), textcoords = 'offset points', ha = 'center', va = 'bottom', rotation = 'vertical', fontsize = 16)
    ax[1].annotate('$A_j(h)$', xy = (left, density), xytext = (-30, 0), textcoords = 'offset points', ha = 'center', va = 'center', rotation = 'horizontal', fontsize = 16)

    pos = params[0] * (left / 2.0) / (params[1] + left / 2.0)
    ax[1].annotate('$f^-_j(k_0)$', xy = (left / 2.0, pos), xytext = (-15, 15), textcoords = 'offset points', ha = 'center', va = 'center', rotation = 10, fontsize = 16)
    x = (max_init + right) / 2.0
    pos = params[2] * x + params[3]
    ax[1].annotate('$f^+_j(k_0)$', xy = (x, pos), xytext = (10, 5), textcoords = 'offset points', ha = 'center', va = 'bottom', rotation = 'horizontal', fontsize = 16)

    pos = params[0] * mid / (params[1] + mid)
    ax[1].annotate('', xy = (mid, density), xycoords = 'data', xytext = (mid, pos), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
    pos = params[2] * mid + params[3]
    ax[1].annotate('', xy = (mid, density), xycoords = 'data', xytext = (mid, pos), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
    
    ax[1].scatter(left_points, left_values, c = 'green', alpha = 1.0, s = 50)
    ax[1].scatter(right_points, right_values, c = 'blue', alpha = 1.0, s = 50)
    ax[1].scatter([mid], [density], c = 'red', alpha = 1.0, s = 50)

    ax[1].set_ylim(0, y_max)
    ax[1].set_xlim(0, max_init)
    ax[1].tick_params(axis = 'both', which = 'major', labelsize = 16)
    ax[1].tick_params(axis = 'both', which = 'minor', labelsize = 16)
    ax[1].set_xlabel('Initiation rate, $k_0$', fontsize = 16)
    ax[1].tick_params(axis = 'y', which = 'both', left = 'off', right = 'off', labelleft = 'off')
    ax[1].set_xticks([left, mid, right])
    ax[1].set_xticklabels(['$u$', '$h$', '$v$'])
    
    smooth = np.array([ 1.0 * 0.03238989,  0.00102625 * 0.4,  0.00041602,  0.0515799,  0.01390564])
    abrupt = np.array([ 0.10130321,  0.00144485 * 0.8,  0.00058738,  0.04684752,  0.0009908])
    jump = np.array([  2.27403866,   2.88557514e-02,   2.70857665e-03, 8.55903307e-02,   5.18817107e-04])

    max_init = 0.002
    init = np.linspace(1e-6, max_init, n_steps)
    smooth_values = evaluate_approximation(smooth, init)
    abrupt_values = evaluate_approximation(abrupt, init)
    jump_values = evaluate_approximation(jump, init)
    
    params = { 'legend.fontsize' : 14, 'legend.linewidth' : 1.5 }
    plot.rcParams.update(params)
    ax[0].plot(init, jump_values, label = 'abrupt, jump', lw = 1.5)
    ax[0].plot(init, abrupt_values, label = 'abrupt', lw = 1.5)
    ax[0].plot(init, smooth_values, label = 'smooth', lw = 1.5)
    ax[0].tick_params(axis = 'x', which = 'both', bottom = 'off', top = 'off', labelbottom = 'off')
    ax[0].tick_params(axis = 'y', which = 'both', left = 'off', right = 'off', labelleft = 'off')
    ax[0].legend(loc = 'center right')
    
    #ax[0].set_xlabel('Initiation rate, $x$', fontsize = 16)
    #ax[0].set_ylabel('Avg. number of ribosomes, $A(x)$', fontsize = 16)
   
    ymax = ax[0].set_ylim()
    ax[0].annotate('(i)', xy = (0, ymax[1]), xytext = (10, -10), textcoords = 'offset points', ha = 'left', va = 'top', rotation = 'horizontal', fontsize = 16, fontweight = 'bold')
    ymax = ax[1].set_ylim()
    ax[1].annotate('(ii)', xy = (0, ymax[1]), xytext = (10, -10), textcoords = 'offset points', ha = 'left', va = 'top', rotation = 'horizontal', fontsize = 16, fontweight = 'bold')
    
    ax = f.add_axes( [0., 0., 1, 1] )
    ax.set_axis_off()
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.text(0.05, 0.5, 'Avg. number of ribosomes, $A_j(k_0)$', rotation = 'vertical', horizontalalignment = 'center', verticalalignment = 'center', fontsize = 16)

    f.tight_layout()
    f.subplots_adjust(left = 0.08, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)
    plot.show()

def plot_segment_densities(pairs = [('YGL103W', 5), ('YPR017C', 0), ('YGL256W', 0)], n_steps = 100, max_init = 0.005, srand = 101317) :
    import lib.persistent
    import lib.approximation
    rates = 1.0 / lib.zhang.RFM_times
    n_rates = len(rates)
    for i in range(n_rates) :
        rates[i] = lib.simulator.RateFunction.logit(rates[i])
    
    init = np.linspace(1e-6, max_init, n_steps)
    init_app = np.linspace(1e-6, max_init, n_steps * 10)

    f, ax = plot.subplots(1, 1)
    for gene, segment in pairs :
        approximation = lib.approximation.approximate_density(gene, rates, srand = srand)
        tree = lib.persistent.forest[gene]
        n_segments = len(tree.tree)
        data = np.zeros((n_steps,))
        for i in range(n_steps) :
            coef, density, J, Q, effective_init = lib.persistent.simulate_gene(gene, init[i], rates, srand)
            node = tree.tree[segment]
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
            data[i] = np.mean(density[left:right + 1])
    
        node = tree.tree[segment]
        left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
        text = '%s : [%d, %d]' % (gene, left + 1, right + 1)
        ax.plot(init, data, label = text, lw = 1.5) 
        text = '%s : [%d, %d], approx' % (gene, left + 1, right + 1)
        values = evaluate_approximation(approximation[segment], init_app)
        ax.plot(init_app, values, '--', label = text, lw = 1.5)
    
    ax.legend(loc = 'lower right')
    ax.set_xlim(np.min(init), np.max(init))
    ax.set_xlabel('Initiation rate, $k_0$', fontsize = 16)
    ax.set_ylabel('Avg. number of ribosomes, $A_j(k_0)$', fontsize = 16)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 16)
    plot.show()

def plot_initiation_fitness_dependency(genes = ['YLR197W', 'YCL037C', 'YGL206C'], scales = [[5.2037648997541073]] * 3, n_steps = 200, ylength = 400, srand = 101317) :
    import lib.persistent
    import lib.approximation
    rates = 1.0 / lib.zhang.RFM_times
    n_rates = len(rates)
    for i in range(n_rates) :
        rates[i] = lib.simulator.RateFunction.logit(rates[i])
    init = np.linspace(1e-6, 0.004, n_steps)
    n_genes = len(genes)
    fits, fits_approx = [], []
    for i in range(n_genes) :
        gene = genes[i]
        gene_scales = scales[i]
        approximation = lib.approximation.approximate_density(gene, rates, srand)
        print approximation
        n_scales = len(gene_scales)
        gene_fits = []
        gene_approx = []
        for j in range(n_scales) :
            gene_fits.append(np.zeros(n_steps))
            gene_approx.append(np.zeros(n_steps))
        for j in range(n_steps) :
            coef = lib.persistent.evaluate_fit(gene, init[j], rates, srand)
            for k in range(n_scales) :
                scale = gene_scales[k]
                likelihood = coef[0] * scale ** 2 + coef[1] * scale + coef[2]
                gene_fits[k][j] = likelihood
            coef = lib.approximation._evaluate_approximation(gene, approximation, init[j])
            for k in range(n_scales) :
                scale = gene_scales[k]
                likelihood = coef[0] * scale ** 2 + coef[1] * scale + coef[2]
                gene_approx[k][j] = likelihood
        fits.append(gene_fits)
        fits_approx.append(gene_approx)
    params = { 'legend.fontsize' : 16, 'legend.linewidth' : 1.5 }
    params = { 'legend.fontsize' : 16 }
    plot.rcParams.update(params)
    f, ax = plot.subplots(1, 1)
    for i in range(n_genes) :
        gene_scales = scales[i]
        n_scales = len(gene_scales)
        for j in range(n_scales) :
            #text = '$\\ln \\tilde{C}=%.2f$' % (scales[i][j])
            text = '%s' % (genes[i])
            ax.plot(init, fits[i][j], '-', label = text, linewidth = 1.5)
            #text = '$\\ln \\tilde{C}=%.2f$, approx' % (scales[i][j])
            text = '%s, approx' % (genes[i])
            ax.plot(init, fits_approx[i][j], '--', label = text, linewidth = 1.5)

    ax.legend(loc = 'best', ncol = 2, labelspacing = 0.2, columnspacing = 0.2)
    ymax = np.max([np.max(fits_approx), np.max(fits)]) + 30
    ax.set_ylim(ymax - ylength, ymax)
    ax.set_xlabel('Initiation rate, $k_0$', fontsize = 16)
    ax.set_ylabel('Objective, $\\psi$', fontsize = 16)
    ax.set_title('Genes: %s' % " ".join(genes), fontsize = 16, fontweight = 'bold')
    f.tight_layout()
    plot.show()

def plot_correlation_histograms(corr_mn, corr_pn, n_bins = 14) :
    f, ax = plot.subplots(1, 2, sharey = True)
    n, bins, patches = ax[0].hist(corr_mn, n_bins, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    median = np.median(corr_mn)
    ax[0].plot([median, median], [0, 10], 'r--', lw = 1.5)
    ax[0].text(median - 0.13, 0.8, 'Median $\\tilde{r}=%.2f$' % median, va = 'center', ha = 'left', rotation = 'vertical', fontsize = 16)
    ax[0].set_xlim(-0.5, 1)
    ax[0].set_ylim(0, 2.1)
    ax[0].set_xlabel('Pearson $r$', fontsize = 16)
    ax[0].set_title('Mean normalization', fontsize = 16, fontweight = 'bold')

    n, bins, patches = ax[1].hist(corr_pn, n_bins, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    median = np.median(corr_pn)
    ax[1].plot([median, median], [0, 10], 'r--', lw = 1.5)
    ax[1].text(median - 0.13, 0.8, 'Median $\\tilde{r}=%.2f$' % median, va = 'center', ha = 'left', rotation = 'vertical', fontsize = 16)
    ax[1].set_xlim(-0.5, 1)
    ax[1].set_ylim(0, 2.1)
    ax[1].axes.get_yaxis().set_visible(False)
    ax[1].set_xlabel('Pearson $r$', fontsize = 16)
    ax[1].set_title('Profile normalization', fontsize = 16, fontweight = 'bold')
    ax[0].set_ylabel('Fraction of genes', fontsize = 16)
    plot.setp(ax[0].get_yticklabels(), visible = False)

    f.subplots_adjust(left = None, bottom = 0.13, right = None, top = None, wspace = 0.05, hspace = None)
    pylab.show()

def plot_segment_length_distribution(forest, min_value = 20, max_value = 1024, histogram_bins = 25, std_bins = 10) :
    genes = forest.keys()
    f, ax = plot.subplots(1, 1)

    lengths, M = [], []
    for gene in genes :
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            length = right - left + 1
            lengths.append(length)
            M.append(ratio_m)

    pre_sort = np.array([lengths, M]).T
    pre_sort = np.array(sorted(pre_sort, key = lambda x : x[0])).T
    lengths = pre_sort[0]
    M = pre_sort[1]

    edges, counts = [], []
    step = float(max_value - min_value) / histogram_bins
    current_edge = min_value
    for i in range(histogram_bins) :
        next_edge = current_edge + step
        if i == histogram_bins - 1 :
            next_edge = np.max(lengths)
        lower = lengths < next_edge
        greater = lengths >= current_edge
        lower = lower.astype(np.int)
        greater = greater.astype(np.int)
        cnt = np.dot(lower, greater)
        counts.append(cnt)
        edges.append(current_edge)
        current_edge = next_edge
    edges.append(max_value)
    
    counts = np.array(counts)
    edges = np.array(edges)
    counts = counts / float(np.sum(counts))
    
    x = (edges[1:] + edges[:-1]) / 2.0
    n, bins, patches = ax.hist(x, bins = edges, weights = counts, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)

    ax.set_xscale('log')
    ax.set_xticks([32, 64, 128, 256, 512, 1000])
    labels = ['$2^5$', '$2^6$', '$2^7$', '$2^8$', '$2^9$', '$\\geq2^{10}$']
    ax.set_xticklabels(labels)

    n = lengths.shape[0]
    per_bin = n / float(std_bins)
    int_pos_prev = 0
    pos, bin_ind = 0, 1
    while pos < n :
        pos = min(per_bin + pos, n)
        int_pos = int(pos)
        if int_pos < n and bin_ind < std_bins :
            val = lengths[int_pos]
        else :
            val = lengths[n - 1] + 1
            int_pos = n - 1
            pos = n
        prev_val = lengths[int_pos_prev]
        indices = (lengths >= prev_val) * (lengths < val)
        bin_data = M[indices]
        print bin_data.shape

        sigma = np.std(bin_data) / math.sqrt(2)
        print '[%4d, %4d) : %.3f +/- %.3f' % (prev_val, val, np.mean(bin_data), sigma)
        mid = np.median(lengths[indices])
        ax.annotate('$\sigma = %.2f$' % sigma, xy = (mid, 0.20), xytext = (8, 0), textcoords = 'offset points', ha = 'center', va = 'bottom', rotation = 'vertical', fontsize = 18)
        
        int_pos_prev = int_pos
        bin_ind += 1
        ax.plot([val, val], [0, 1], 'k--')

    ax.set_ylim(0, 0.50)
    ax.set_xlim(min_value, max_value)
    ax.set_xlabel('Segment length', fontsize = 16)
    ax.set_ylabel('Fraction of segments', fontsize = 16)
    #ax.tick_params(axis = 'both', which = 'major', labelsize = 30)
    #ax.tick_params(axis = 'both', which = 'minor', labelsize = 30)
    ax.tick_params(axis = 'x', which = 'major', labelsize = 16)
    f.subplots_adjust(left = None, bottom = 0.13, right = None, top = None, wspace = 0.05, hspace = None)
    
    pylab.show()

def plot_ratio_error_historgrams_per_group(forest, groups) :
    genes = forest.keys()
    f, ax = plot.subplots(3, 4)
    
    M, lengths = [], []
    M_full = []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            length = right - left + 1
            M.append(ratio_m)
            lengths.append(length)
            if length == length_codons :
                M_full.append(ratio_m)
    M, lengths = np.array(M), np.array(lengths)

    row, col = 0, 0
    k = 1
    for left, right, sigma, group_size in groups :
        M_group = M[np.logical_and(lengths >= left, length < right)]
        lengths_group = lengths[np.logical_and(lengths >= left, length < right)]
        mean, sigma = np.mean(M_group), np.std(M_group)
        cur = ax[row][col]
        n, bins, patches = cur.hist(M_group, 20, normed = True, histtype = 'stepfilled')
        pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
        x = np.linspace(-3, 3, 100)
        cur.plot(x, mlab.normpdf(x, mean, sigma), 'b', linewidth = 2, alpha = 0.65, label = 'i.r.e., $\\sigma^\\mathrm{i.r.e.}_{%d}=%.2f$' % (k, sigma))
        cur.set_xlim(-1.2, 1.2)
        cur.set_ylim(0, 2.5)
        cur.axes.get_yaxis().set_visible(False)
        cur.set_xlabel('$\\log_2$ ratio', fontsize = 14)
        cur.set_title('Length $%d\\leq l<%d$' % (left, right), fontsize = 14, fontweight = 'bold')
        cur.legend()
        k += 1
        col += 1
        if col >= 4 :
            col = 0
            row += 1

    cur = ax[row][col]
    mean, sigma = np.mean(M), np.std(M)
    n, bins, patches = cur.hist(M, 20, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    x = np.linspace(-3, 3, 100)
    cur.plot(x, mlab.normpdf(x, mean, sigma), 'b', linewidth = 2, alpha = 0.65, label = 'i.r.e., $\\sigma^\\mathrm{i.r.e.}=%.2f$' % sigma)
    cur.set_xlim(-1.2, 1.2)
    cur.set_ylim(0, 2.5)
    cur.axes.get_yaxis().set_visible(False)
    cur.set_xlabel('$\\log_2$ ratio', fontsize = 14)
    cur.set_title('All segments', fontsize = 14, fontweight = 'bold')
    cur.legend()
    
    col += 1
    if col >= 4 :
        col = 0
        row += 1

    cur = ax[row][col]
    mean, sigma = np.mean(M_full), np.std(M_full)
    n, bins, patches = cur.hist(M_full, 20, normed = True, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'g', 'alpha', 0.45)
    x = np.linspace(-3, 3, 100)
    cur.plot(x, mlab.normpdf(x, mean, sigma), 'g', linewidth = 2, alpha = 0.65, label = 'i.r.e., $\\sigma^\\mathrm{i.r.e.}=%.2f$' % sigma)
    cur.set_xlim(-1.2, 1.2)
    cur.set_ylim(0, 2.5)
    cur.axes.get_yaxis().set_visible(False)
    cur.set_xlabel('$\\log_2$ ratio', fontsize = 14)
    cur.set_title('Full-CDS', fontsize = 14, fontweight = 'bold')
    cur.legend()

    #while col < 4 and row < 3 :
    #    cur = ax[row][col]
    #    f.delaxes(cur)
    #    col += 1
    #    if col >= 4 :
    #        col = 0
    #        row += 1
    
    f.set_size_inches(11.5, 8, forward = True)
    f.tight_layout()
    pylab.show()
    
def plot_ratio_error_historgram(forest) :
    genes = forest.keys()
    f, ax = plot.subplots(1, 1)
    
    M, M_all = [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                M.append(ratio_m)
            M_all.append(ratio_m)
    M, M_all = np.array(M), np.array(M_all)
    mean, sigma = np.mean(M), np.std(M)
    mean_all, sigma_all = np.mean(M_all), np.std(M_all)
    
    n, bins, patches = ax.hist(M, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    n, bins, patches = ax.hist(M_all, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'g', 'alpha', 0.45)

    x = np.linspace(-3, 3, 100)
    ax.plot(x, mlab.normpdf(x, mean, sigma), 'b', linewidth = 2, alpha = 0.65, label = 'Full-CDS, $\sigma^\\mathrm{i.r.e.}=%.2f$' % sigma)
    ax.plot(x, mlab.normpdf(x, mean_all, sigma_all), 'g', linewidth = 2, alpha = 0.65, label = 'All segments, $\sigma^\\mathrm{i.r.e.}=%.2f$' % sigma_all)
    ax.set_xlim(-1.5, 1.5)
    ax.set_ylim(0, 3.0)
    ax.axes.get_yaxis().set_visible(False)
    ax.set_xlabel('$\\log_2$ ratio', fontsize = 16)
    ax.legend()
    f.set_size_inches(4, 5.5, forward = True)
    f.tight_layout()
    
    pylab.show()
 
def plot_error_historgrams(forest) :
    genes = forest.keys()
    f, ax = plot.subplots(1, 2, sharey = True)
    
    M, M_all = [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                M.append(mrna_m)
            M_all.append(mrna_m)
    M, M_all = np.array(M), np.array(M_all)
    mean, sigma = np.mean(M), np.std(M)
    mean_all, sigma_all = np.mean(M_all), np.std(M_all)
    
    n, bins, patches = ax[0].hist(M, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    n, bins, patches = ax[0].hist(M_all, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'g', 'alpha', 0.45)

    x = np.linspace(-3, 3, 100)
    ax[0].plot(x, mlab.normpdf(x, mean, sigma), 'b', linewidth = 2, alpha = 0.65, label = 'Full-CDS, $\\sigma^\\mathrm{i.r.e.}=%.2f$' % sigma)
    ax[0].plot(x, mlab.normpdf(x, mean_all, sigma_all), 'g', linewidth = 2, alpha = 0.65, label = 'All segments, $\\sigma^\\mathrm{i.r.e.}=%.2f$' % sigma_all)
    ax[0].set_xlim(-1.2, 1.2)
    ax[0].set_ylim(0, 3.0)
    ax[0].set_title('mRNA density', fontsize = 16, fontweight = 'bold')
    ax[0].axes.get_yaxis().set_visible(False)
    ax[0].set_xlabel('$\\log_2$ ratio', fontsize = 16)
    ax[0].legend()
 
    print 'mRNA full-gene: %.5f +/- %.5f' % (mean, sigma)
    print 'mRNA segments: %.5f +/- %.5f' % (mean_all, sigma_all)  

    M, M_all = [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                M.append(rib_m)
            M_all.append(rib_m)
    M, M_all = np.array(M), np.array(M_all)
    mean, sigma = np.mean(M), np.std(M)
    mean_all, sigma_all = np.mean(M_all), np.std(M_all)
    
    n, bins, patches = ax[1].hist(M, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    n, bins, patches = ax[1].hist(M_all, 20, normed = 1, histtype = 'stepfilled')
    pylab.setp(patches, 'facecolor', 'g', 'alpha', 0.45)

    x = np.linspace(-3, 3, 100)
    ax[1].plot(x, mlab.normpdf(x, mean, sigma), 'b', linewidth = 2, alpha = 0.65, label = 'Full-CDS, $\\sigma^\\mathrm{i.r.e.}=%.2f$' % sigma)
    ax[1].plot(x, mlab.normpdf(x, mean_all, sigma_all), 'g', linewidth = 2, alpha = 0.65, label = 'All segments, $\\sigma^\\mathrm{i.r.e.}=%.2f$' % sigma_all)
    ax[1].set_xlim(-1.2, 1.2)
    ax[1].set_ylim(0, 3.0)
    ax[1].set_title('Ribosome density', fontsize = 16, fontweight = 'bold')
    ax[1].axes.get_yaxis().set_visible(False)
    ax[1].set_xlabel('$\\log_2$ ratio', fontsize = 16)
    ax[1].legend()

    print 'Rib full-gene: %.5f +/- %.5f' % (mean, sigma)
    print 'Rib segments: %.5f +/- %.5f' % (mean_all, sigma_all)  
    
    f.set_size_inches(8, 6.5, forward = True)
    f.tight_layout()
    pylab.show()

def regress_ma_lowess(m, a, color, frac = 0.33, it = 3, sorted = False) :
    a = np.atleast_1d(a) 
    m = np.atleast_1d(m) 
    color = np.atleast_1d(color)
    a = a[color == 0]
    m = m[color == 0]

    lowess = sm.nonparametric.lowess
    z = lowess(m, a, frac = frac, it = it, return_sorted = sorted)
    return z
    
def ma_plot_forest_original(forest, mrna_threshold = 128, rib_threshold = 128, output = None) :
    genes = forest.keys()
    f, ax = plot.subplots(1, 3, sharey = True)
    cmap = pylab.cm.get_cmap('RdYlBu_r')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                color = 1 if mrna_cnt < mrna_threshold else 0
                M.append(mrna_m)
                A.append(mrna_a)
                colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    ax[0].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[0].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[0].transAxes, linewidth = 1, color = 'k'))
    ax[0].set_xlim(-30.5, -13)
    ax[0].set_ylim(-3, 3)
    ax[0].set_title('mRNA density', fontsize = 16, fontweight = 'bold')
    ax[0].set_ylabel('M, $\\log_2$', fontsize = 16)

    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                color = 1 if rib_cnt < rib_threshold else 0
                M.append(rib_m)
                A.append(rib_a)
                colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    ax[1].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[1].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[1].transAxes, linewidth = 1, color = 'k'))
    ax[1].set_xlim(-30.5, -13)
    ax[1].set_ylim(-3, 3)
    ax[1].set_title('Ribosome density', fontsize = 16, fontweight = 'bold')
    ax[1].set_xlabel('A, $\\log_2$', fontsize = 16)

    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            if left == 0 and right == length_codons - 1 :
                color = 1 if mrna_cnt < mrna_threshold or rib_cnt < rib_threshold else 0
                M.append(ratio_m)
                A.append(ratio_a)
                colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    ax[2].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[2].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[2].transAxes, linewidth = 1, color = 'k'))
    ax[2].set_xlim(-7, 4)
    ax[2].set_ylim(-3, 3)
    ax[2].set_title('Density ratio', fontsize = 16, fontweight = 'bold')
    
    f.set_size_inches(14, 4.0, forward = True)
    f.tight_layout()
    f.subplots_adjust(left = None, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)

    if output is not None :
        plot.savefig(output)

    pylab.show()

def ma_plot_ratio_bias_correction(forest) :
    genes = forest.keys()
    cmap = pylab.cm.get_cmap('RdYlBu_r')
    f, ax = plot.subplots(1, 1)
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 0
            M.append(ratio_m)
            A.append(ratio_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax.scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax.add_line(Line2D([0, 1], [0.5, 0.5], transform = ax.transAxes, linewidth = 1, color = 'k'))
    ax.plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax.set_xlim(-5.5, 5.5)
    ax.set_ylim(-2.5, 2.5)
    ax.set_xlabel('A, $\\log_2$', fontsize = 16)
    ax.set_ylabel('M, $\\log_2$', fontsize = 16)
   
    #plot.tick_params(axis = 'both', which = 'major', labelsize = 16)
    #plot.tick_params(axis = 'both', which = 'minor', labelsize = 16)
    f.set_size_inches(6, 3.5, forward = True)
    f.tight_layout()
    f.subplots_adjust(left = None, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)
    pylab.show()

def ma_plot_bias_correction(forest, forest_corrected, mrna_threshold = 128, rib_threshold = 128) :
    genes = forest.keys()
    f, ax = plot.subplots(2, 3, sharey = 'row')
    cmap = pylab.cm.get_cmap('RdYlBu_r')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if mrna_cnt < mrna_threshold else 0
            M.append(mrna_m)
            A.append(mrna_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[0][0].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[0][0].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[0][0].transAxes, linewidth = 1, color = 'k'))
    ax[0][0].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[0][0].set_title('mRNA density', fontsize = 16, fontweight = 'bold')
    ax[0][0].set_ylabel('(i)', fontsize = 16, fontweight = 'bold', rotation = 'horizontal')
    ax[0][0].set_ylim(-2.5, 2.5)
    ax[0][0].set_xlim(-27.5, -13)
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if rib_cnt < rib_threshold else 0
            M.append(rib_m)
            A.append(rib_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[0][1].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[0][1].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[0][1].transAxes, linewidth = 1, color = 'k'))
    ax[0][1].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[0][1].set_ylim(-2.5, 2.5)
    ax[0][1].set_xlim(-27.5, -13)
    ax[0][1].set_title('Ribosome density', fontsize = 16, fontweight = 'bold')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest[gene].length_codons
        for node in forest[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if rib_cnt < rib_threshold or mrna_cnt < mrna_threshold else 0
            M.append(ratio_m)
            A.append(ratio_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[0][2].scatter(A, M, c = colors, cmap = cmap, alpha = 0.2, s = 30)
    ax[0][2].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[0][2].transAxes, linewidth = 1, color = 'k'))
    ax[0][2].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[0][2].set_ylim(-2.5, 2.5)
    ax[0][2].set_xlim(-5.5, 5.5)
    ax[0][2].set_title('Density ratio', fontsize = 16, fontweight = 'bold')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest_corrected[gene].length_codons
        for node in forest_corrected[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if mrna_cnt < mrna_threshold else 0
            M.append(mrna_m)
            A.append(mrna_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[1][0].scatter(A[colors == 0], M[colors == 0], c = colors[colors == 0], cmap = cmap, alpha = 0.2, s = 30)
    ax[1][0].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[1][0].transAxes, linewidth = 1, color = 'k'))
    ax[1][0].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[1][0].set_ylim(-2.5, 2.5)
    ax[1][0].set_xlim(-27.5, -13)
    ax[1][0].set_ylabel('(ii)', fontsize = 16, fontweight = 'bold', rotation = 'horizontal')
    
    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest_corrected[gene].length_codons
        for node in forest_corrected[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if rib_cnt < rib_threshold else 0
            M.append(rib_m)
            A.append(rib_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[1][1].scatter(A[colors == 0], M[colors == 0], c = colors[colors == 0], cmap = cmap, alpha = 0.2, s = 30)
    ax[1][1].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[1][1].transAxes, linewidth = 1, color = 'k'))
    ax[1][1].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[1][1].set_xlabel('A, $\\log_2$', fontsize = 16)
    ax[1][1].set_ylim(-2.5, 2.5)
    ax[1][1].set_xlim(-27.5, -13)

    M, A, colors = [], [], []
    for gene in genes :
        length_codons = forest_corrected[gene].length_codons
        for node in forest_corrected[gene].tree :
            left, right, ratio_a, ratio_m, mrna_a, mrna_m, rib_a, rib_m, mrna_cnt, rib_cnt, node_sigma = node
            color = 1 if rib_cnt < rib_threshold or mrna_cnt < mrna_threshold else 0
            M.append(ratio_m)
            A.append(ratio_a)
            colors.append(color)
    M, A, colors = np.array(M), np.array(A), np.array(colors)
    Z = regress_ma_lowess(M, A, colors, sorted = True)
    ax[1][2].scatter(A[colors == 0], M[colors == 0], c = colors[colors == 0], cmap = cmap, alpha = 0.2, s = 30)
    ax[1][2].add_line(Line2D([0, 1], [0.5, 0.5], transform = ax[1][2].transAxes, linewidth = 1, color = 'k'))
    ax[1][2].plot(Z[:, 0], Z[:, 1], color = 'red', linewidth = 1.5)
    ax[1][2].set_ylim(-2.5, 2.5)
    ax[1][2].set_xlim(-5.5, 5.5)

    f.tight_layout()
    f.subplots_adjust(left = None, bottom = None, right = None, top = None, wspace = 0.05, hspace = None)
    
    f.text(0.060, 0.5, 'M, $\\log_2$', va = 'center', rotation = 'vertical', fontsize = 16)
    f.set_size_inches(15, 6.0, forward = True)
    
    pylab.show()
