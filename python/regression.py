'''
A quick regression class provided by Amin.

Alexey Gritsenko
6-10-2014
'''

class LinearRegression():
    def __init__(self):
        from sklearn.linear_model import LinearRegression
        self.model = LinearRegression(fit_intercept = True, normalize = False, copy_X = True)

    def train(self, train_data, train_label):
        self.model.fit(train_data, train_label)
        self.weights = self.model.coef_
        self.bias = self.model.intercept_

    def predict(self, test_data):
        return self.model.predict(test_data)

    def cross_val(self, data, label, n_fold = 10, score = 'r2', num_jobs = 1):
        from sklearn import cross_validation
        return cross_validation.cross_val_score(self.model, data, label, cv = n_fold, scoring = score, n_jobs = num_jobs, verbose = 0, fit_params = None, score_func = None, pre_dispatch = '2*n_jobs')
