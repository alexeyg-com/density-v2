#!/bin/env python

import lib.reader
import lib.processors
import lib.writer
import os
from create_arrays import *

def get_changes() :
    ''' 
    Return read changes file.
    ''' 
    changes = lib.reader.read_changes_file('../data/all_chromosome_sequence_changes.tab')
    print '[+] Read changes file.'

    return changes

def update_alignments(input = None, output = None) :
    '''
    Update alignment to the latest genome release.
    :param input: a list of input filenames.
    :param output: a list of output filenames.
    '''
    if input is None :
        input = ['../data/reads/ingolia/GSM346111_fp_rich1_chr_best.txt', '../data/reads/ingolia/GSM346114_fp_rich2_chr_best.txt', '../data/reads/ingolia/GSM346117_mrna_rich1_chr_best.txt', '../data/reads/ingolia/GSM346118_mrna_rich2_chr_best.txt']
    if output is None :
        output = ['../alignment/ingolia/GSM346111_fp_rich1_chr_best.out', '../alignment/ingolia/GSM346114_fp_rich2_chr_best.out', '../alignment/ingolia/GSM346117_mrna_rich1_chr_best.out', '../alignment/ingolia/GSM346118_mrna_rich2_chr_best.out']

    changes = get_changes()
    reference = get_reference()
    for i in range(len(input)) :
        print '[+] Updating alignment: %s' % input[i]
        print '    [+] Reading alignment...'
        alignment = lib.reader.read_ingolia_alignment(input[i])
        print '    [+] Updating alignment coordinates...'
        alignment = lib.processors.apply_changes_to_ingolia_alignment(reference, changes, alignment, 59)
        print '    [+] Cleaning alignment...'
        alignment = lib.processors.clean_alignment_groups(alignment)
        lib.writer.write_pickle(alignment, output[i])
        print '    [+] Wrote alignment (pickle): %s' % output[i]

def get_ribosome_alignment(selection = -1) :
    '''
    Return ribosome alignment.
    :param selection: which replica to return. -1 for both.
    '''
    rich1_rib, rich2_rib = [], []
    if selection == 0 or selection < 0 :
        rich1_rib = lib.reader.read_pickle('../alignment/ingolia/GSM346111_fp_rich1_chr_best.out')
    if selection == 1 or selection < 0 :
        rich2_rib = lib.reader.read_pickle('../alignment/ingolia/GSM346114_fp_rich2_chr_best.out')
    alignment = rich1_rib + rich2_rib
    print '[+] Read ribosome alignment: %s alignment groups.' % format(len(alignment), ',d')

    return alignment

def get_mrna_alignment(selection = -1) :
    '''
    Return mRNA alignment.
    :param selection: which replica to return. -1 for both.
    '''
    rich1_mrna, rich2_mrna = [], []
    if selection == 0 or selection < 0 :
        rich1_mrna = lib.reader.read_pickle('../alignment/ingolia/GSM346117_mrna_rich1_chr_best.out')
    if selection == 1 or selection < 0 :
        rich2_mrna = lib.reader.read_pickle('../alignment/ingolia/GSM346118_mrna_rich2_chr_best.out')
    alignment = rich1_mrna + rich2_mrna
    print '[+] Read mRNA alignment: %s alignment groups.' % format(len(alignment), ',d')

    return alignment

def filter_alignments(alignment, mismatches = 2, min_length = 22, max_length = 32, max_nucA = 18, unique = False) :
    '''
    Filters alignments.
    :param alignment: alignment to filter.
    :param mismatches: maximum number of mismatches allowed.
    :param min_length: minimum allowed alignment length.
    :param max_length: maximum allowed alignment length.
    :param max_nucA: maximum number of A nucleotides allowed at the end of the read.
    :param unique: if true, only unambiguously aligned reads are kept.
    '''
    filtered_alignment, removed_groups, removed_alg = lib.processors.filter_alignments_ingolia(alignment, mismatches = mismatches, min_length = min_length, max_length = max_length, max_nucA = max_nucA, unique = unique)

    print '[+] Filtered alignments'
    print '    [i] Initial groups: %s' % format(len(alignment), ',d')
    print '    [i] Left groups: %s' % format(len(filtered_alignment), ',d')
    print '    [i] Removed groups %s' % format(removed_groups, ',d')
    print '    [i] Removed alignments: %s' % format(removed_alg, ',d')

    return filtered_alignment

def filter_mrna_alignment(alignment, mismatches = 2, unique = False) :
    '''
    Filter alignment with mRNA alignment default settings.
    :param alignment: alignment to filter.
    :param mismatches: maximum number of allowed mismatches.
    :param unique: if true, only unambiguously aligned reads are kept.
    '''
    return filter_alignments(alignment, mismatches = mismatches, min_length = 22, max_length = 32, max_nucA = 18, unique = unique)

def filter_rib_alignment(alignment, mismatches = 2, unique = False) :
    '''
    Filter alignment with ribosome alignment default settings.
    :param alignment: alignment to filter.
    :param mismatches: maximum number of allowed mismatches.
    :param unique: if true, only unambiguously aligned reads are kept.
    '''
    return filter_alignments(alignment, mismatches = mismatches, min_length = 27, max_length = 32, max_nucA = 18, unique = unique)

def process_alignments() :
    '''
    Process Ingolia alignment format into a format used internally.
    Also update alignment coordinates to the latest reference.
    '''
    files = ['../alignment/ingolia/GSM346111_fp_rich1_chr_best.out', '../alignment/ingolia/GSM346114_fp_rich2_chr_best.out', '../alignment/ingolia/GSM346117_mrna_rich1_chr_best.out', '../alignment/ingolia/GSM346118_mrna_rich2_chr_best.out']
    update_alignments()
    check_alignments(files = files, adapter = 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
    produce_alignment_statistics(files = files)

def create_records() :
    '''
    Creates read records for yeast genes.
    '''
    input = [get_ribosome_alignment, get_mrna_alignment]
    kernel = [lib.processors.ribosome_asite_kernel_ingolia, lib.processors.read_middle_kernel]
    filters = [filter_rib_alignment, filter_mrna_alignment]
    
    create_read_records(input, kernel, filters, output = ['../results/ingolia-reads/rib_read_1.out', '../results/ingolia-reads/mrna_read_1.out'], selection = 0)
    create_read_records(input, kernel, filters, output = ['../results/ingolia-reads/rib_read_2.out', '../results/ingolia-reads/mrna_read_2.out'], selection = 1)
    
    create_read_records(input, kernel, filters, output = ['../results/ingolia-reads/rib_read_unique_1.out', '../results/ingolia-reads/mrna_read_unique_1.out'], selection = 0, mismatches = 1, unique = True)
    create_read_records(input, kernel, filters, output = ['../results/ingolia-reads/rib_read_unique_2.out', '../results/ingolia-reads/mrna_read_unique_2.out'], selection = 1, mismatches = 1, unique = True)

def create_profiles_records() :
    '''
    Creates coverage records for yeast genes.
    '''
    input = [get_ribosome_alignment, get_mrna_alignment]
    kernel = [lib.processors.ribosome_coverage_kernel, lib.processors.mrna_coverage_kernel]
    assignment_kernel = [lib.processors.ribosome_asite_kernel_ingolia, lib.processors.read_middle_kernel]
    filters = [filter_rib_alignment, filter_mrna_alignment]
    
    create_coverage_records(input, kernel, assignment_kernel, filters, output = ['../results/ingolia-reads/rib_coverage_1.out', '../results/ingolia-reads/mrna_coverage_1.out'], selection = 0)
    create_coverage_records(input, kernel, assignment_kernel, filters, output = ['../results/ingolia-reads/rib_coverage_2.out', '../results/ingolia-reads/mrna_coverage_2.out'], selection = 1)
    
    create_coverage_records(input, kernel, assignment_kernel, filters, output = ['../results/ingolia-reads/rib_coverage_unique_1.out', '../results/ingolia-reads/mrna_coverage_unique_1.out'], selection = 0, mismatches = 1, unique = True)
    create_coverage_records(input, kernel, assignment_kernel, filters, output = ['../results/ingolia-reads/rib_coverage_unique_2.out', '../results/ingolia-reads/mrna_coverage_unique_2.out'], selection = 1, mismatches = 1, unique = True)

def main() :
    process_alignments()
    create_records()
    create_profiles_records()

if __name__ == "__main__":
    main()
