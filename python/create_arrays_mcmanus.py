#!/bin/env python

import lib.reader
import lib.processors
import lib.writer
import os
from create_arrays import *

def get_ribosome_alignment(selection = -1) :
    '''
    Return ribosome alignment.
    :param selection: which replica to return. -1 for both.
    '''
    rich1_rib, rich2_rib = [], []
    if selection == 0 or selection < 0 :
        rich1_rib = lib.reader.read_pickle('../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553_trim_recovered_extended_grouped.out')
    if selection == 1 or selection < 0 :
        rich2_rib = lib.reader.read_pickle('../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555_trim_recovered_extended_grouped.out')
    alignment = rich1_rib + rich2_rib
    print '[+] Read ribosome alignment: %s alignment groups.' % format(len(alignment), ',d')

    return alignment

def get_mrna_alignment(selection = -1) :
    '''
    Return mRNA alignment.
    :param selection: which replica to return. -1 for both.
    '''
    rich1_mrna, rich2_mrna = [], []
    if selection == 0 or selection < 0 :
        rich1_mrna = lib.reader.read_pickle('../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551_trim_recovered_extended_grouped.out')
    if selection == 1 or selection < 0 :
        rich2_mrna = lib.reader.read_pickle('../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552_trim_recovered_extended_grouped.out')
    alignment = rich1_mrna + rich2_mrna
    print '[+] Read mRNA alignment: %s alignment groups.' % format(len(alignment), ',d')

    return alignment

def filter_alignments(alignment, mismatches = 2, min_length = 22, max_length = 32, unique = False) :
    '''
    Filters alignments.
    :param alignment: alignment to filter.
    :param mismatches: maximum number of mismatches allowed.
    :param min_length: minimum allowed alignment length.
    :param max_length: maximum allowed alignment length.
    :param unique: if true, only unambiguously aligned reads are kept.
    '''
    filtered_alignment, removed_groups, removed_alg = lib.processors.filter_alignments_mcmanus(alignment, mismatches = mismatches, min_length = min_length, max_length = max_length, unique = unique)

    print '[+] Filtered alignments'
    print '    [i] Initial groups: %s' % format(len(alignment), ',d')
    print '    [i] Left groups: %s' % format(len(filtered_alignment), ',d')
    print '    [i] Removed groups %s' % format(removed_groups, ',d')
    print '    [i] Removed alignments: %s' % format(removed_alg, ',d')

    return filtered_alignment

def filter_mrna_alignment(alignment, mismatches = 2, unique = False) :
    '''
    Filter alignment with mRNA alignment default settings.
    :param alignment: alignment to filter.
    :param mismatches: maximum number of allowed mismatches.
    :param unique: if true, only unambiguously aligned reads are kept.
    '''
    return filter_alignments(alignment, mismatches = mismatches, min_length = 27, max_length = 40, unique = unique)

def filter_rib_alignment(alignment, mismatches = 2, unique = False) :
    '''
    Filter alignment with ribosome alignment default settings.
    :param alignment: alignment to filter.
    :param mismatches: maximum number of allowed mismatches.
    :param unique: if true, only unambiguously aligned reads are kept.
    '''
    return filter_alignments(alignment, mismatches = mismatches, min_length = 27, max_length = 33, unique = unique)

def prepare_references_for_alignment() :
    '''
    Prepare references for alignment (write gene and genome reference sequences).
    '''
    record_files = ['../data/gff/R64/SGD_Everything.gff3', '../data/gff/R64/Nagalakshmi_2008_UTRs.gff3', '../data/gff/R64/Yassour_2009_UTRs.gff3']
    records = get_records(record_files)
    records = lib.processors.group_gff_records_by_name(records)
    references = get_reference()
    records = lib.processors.combine_gff_records_into_genes(records, references, extension = 100)
    genome_fasta = lib.processors.convert_references_to_fasta(references)
    gene_fasta = lib.processors.extract_sequences_for_gene_records(records, references)
    lib.writer.write_fasta(gene_fasta, '../alignment/mcmanus/genes.fasta')
    lib.writer.write_fasta(genome_fasta, '../alignment/mcmanus/genome.fasta')

def align_reads() :
    '''
    Align reads to references (genes and genome) using bowtie.
    '''
    os.system('bowtie-build -f ../alignment/mcmanus/genes.fasta ../alignment/mcmanus/genes')
    os.system('bowtie-build -f ../alignment/mcmanus/genome.fasta ../alignment/mcmanus/genome')
    files = ['../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551_trim', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552_trim', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553_trim', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555_trim']
    for file in files :
        print '[i] Aligning reads: %s' % file
        os.system('bowtie --time -a --best --strata -m 255 --threads 12 --sam --chunkmbs 500 -l 15 --un %s_unaligned.fastq ../alignment/genes %s.fastq | samtools view -Sb - > %s.bam' % (file, file, file))
        os.system('bowtie --time -a --best --strata -m 255 --threads 12 --sam --chunkmbs 500 -l 15 --un %s_unaligned_genome.fastq ../alignment/genome %s_unaligned.fastq | samtools view -Sb - > %s_genome.bam' % (file, file, file))
        os.system('samtools sort -l 9 -n -@ 12 %s.bam %s_sorted' % (file, file))
        os.system('samtools sort -l 9 -n -@ 12 %s_genome.bam %s_genome_sorted' % (file, file))

def recover_alignments() :
    '''
    Recover alignments to genes to their genomic coordinates.
    '''
    record_files = ['../data/gff/R64/SGD_Everything.gff3', '../data/gff/R64/Nagalakshmi_2008_UTRs.gff3', '../data/gff/R64/Yassour_2009_UTRs.gff3']
    files = ['../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551_trim', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552_trim', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553_trim', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555_trim']
    references = get_reference()
    records = get_records(record_files)
    records = lib.processors.group_gff_records_by_name(records)
    records = lib.processors.combine_gff_records_into_genes(records, references, extension = 100)
    for file in files :
        print '[i] Processing alignments: %s' % file
        alignment_genes = lib.reader.read_bam(file + '_sorted.bam')
        print '   [+] Read gene alignment.'
        alignment_genes = lib.processors.restore_original_alignment(alignment_genes, records)
        print '   [+] Recovered original coordinates.'
        alignment_genome = lib.reader.read_bam(file + '_genome_sorted.bam')
        print '   [+] Read genome alignment.'
        alignment = alignment_genes + alignment_genome
        alignment = alignment_genes
        lib.writer.write_pickle(alignment, file + '_recovered.out')

# CTGTAGGCACCATCAAT - Ingolia linker, AGATCGGAAGAGCACACGTCTGA - Illumina adapter
def extend_alignments(adapter = 'CTGTAGGCACCATCAATAGATCGGAAGAGCACACGTCTGA') :
    '''
    Extend alignments of k-mers to full length read alignments against reference + adapter.
    '''
    file_pairs = [('../data/reads/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551_trim'), ('../data/reads/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552_trim'), ('../data/reads/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553_trim'), ('../data/reads/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555_trim')]
    references = get_reference()
    for file_before, file_after in file_pairs :
        print '[i] Processing alignments: %s' % file_after
        alignments = lib.reader.read_pickle(file_after + '_recovered.out')
        reads = lib.reader.read_fastq(file_before + '.fastq')
        alignments = lib.processors.extend_alignment(alignments, reads, references, adapter)
        del reads
        lib.writer.write_pickle(alignments, file_after + '_recovered_extended.out')
        del alignments

def group_multiple_alignments() :
    '''
    Group multiple alignments of the same read together.
    '''
    files = ['../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551_trim', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552_trim', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553_trim', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555_trim']
    for file in files :
        print '[i] Grouping alignments: %s' % file
        alignments = lib.reader.read_pickle(file + '_recovered_extended.out')
        grouped = lib.processors.group_multiple_alignments(alignments)
        del alignments
        lib.writer.write_pickle(grouped, file + '_recovered_extended_grouped.out')
        del grouped

def trim_reads(file_pairs = None, trim_length = 21) :
    '''
    Trim reads prior to alignment.
    :param file_pairs : pairs of files to trim and save to.
    :param trim_length : length to trim the reads to.
    '''
    if file_pairs is None :
        file_pairs = [('../data/reads/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551.fastq', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551_trim.fastq'), ('../data/reads/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552.fastq', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552_trim.fastq'), ('../data/reads/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553.fastq', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553_trim.fastq'), ('../data/reads/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555.fastq', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555_trim.fastq')]
    for input_file, output_file in file_pairs :
        print '[i] Trimming %s -> %s' % (input_file, output_file)
        lib.processors.trim_fastq(input_file, output_file, trim_length)

def create_records() :
    '''
    Creates read records for yeast genes.
    '''
    input = [get_ribosome_alignment, get_mrna_alignment]
    kernel = [lib.processors.ribosome_asite_kernel_mcmanus, lib.processors.read_middle_kernel]
    filters = [filter_rib_alignment, filter_mrna_alignment]
    
    create_read_records(input, kernel, filters, output = ['../results/mcmanus-reads/rib_read_1.out', '../results/mcmanus-reads/mrna_read_1.out'], selection = 0)
    create_read_records(input, kernel, filters, output = ['../results/mcmanus-reads/rib_read_2.out', '../results/mcmanus-reads/mrna_read_2.out'], selection = 1)
    
    create_read_records(input, kernel, filters, output = ['../results/mcmanus-reads/rib_read_unique_1.out', '../results/mcmanus-reads/mrna_read_unique_1.out'], selection = 0, mismatches = 1, unique = True)
    create_read_records(input, kernel, filters, output = ['../results/mcmanus-reads/rib_read_unique_2.out', '../results/mcmanus-reads/mrna_read_unique_2.out'], selection = 1, mismatches = 1, unique = True)

def create_profiles_records() :
    '''
    Creates coverage records for yeast genes.
    '''
    input = [get_ribosome_alignment, get_mrna_alignment]
    kernel = [lib.processors.ribosome_coverage_kernel, lib.processors.mrna_coverage_kernel]
    assignment_kernel = [lib.processors.ribosome_asite_kernel_mcmanus, lib.processors.read_middle_kernel]
    filters = [filter_rib_alignment, filter_mrna_alignment]
    
    create_coverage_records(input, kernel, assignment_kernel, filters, output = ['../results/mcmanus-reads/rib_coverage_1.out', '../results/mcmanus-reads/mrna_coverage_1.out'], selection = 0)
    create_coverage_records(input, kernel, assignment_kernel, filters, output = ['../results/mcmanus-reads/rib_coverage_2.out', '../results/mcmanus-reads/mrna_coverage_2.out'], selection = 1)
    
    create_coverage_records(input, kernel, assignment_kernel, filters, output = ['../results/mcmanus-reads/rib_coverage_unique_1.out', '../results/mcmanus-reads/mrna_coverage_unique_1.out'], selection = 0, mismatches = 1, unique = True)
    create_coverage_records(input, kernel, assignment_kernel, filters, output = ['../results/mcmanus-reads/rib_coverage_unique_2.out', '../results/mcmanus-reads/mrna_coverage_unique_2.out'], selection = 1, mismatches = 1, unique = True)

def process_reads() :
    '''
    Process FASTQ read files into properly aligned reads.
    '''
    files = ['../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep1/SRR948551_trim', '../alignment/mcmanus/S_cer_mRNA_RiboProf_matched_Rep2/SRR948552_trim', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep1/SRR948553_trim', '../alignment/mcmanus/S_cer_Ribo-seq_matched_Rep2/SRR948555_trim']
    trim_reads()
    prepare_references_for_alignment()
    align_reads()
    recover_alignments()
    extend_alignments()
    check_alignments(files = [file + '_recovered_extended.out' for file in files], adapter = 'CTGTAGGCACCATCAATAGATCGGAAGAGCACACGTCTGA')
    group_multiple_alignments()
    produce_alignment_statistics(files = [file + '_recovered_extended_grouped.out' for file in files])
    
def main() :
    process_reads()
    create_records()
    create_profiles_records()

if __name__ == "__main__":
    main()
