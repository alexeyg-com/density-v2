#!/usr/bin/env bash

output="/dev/shm/alexeygritsenk/"
prefix="shah-time-2.4mil-seed-1413_ribo_pos_"
threshold=100
pid=50615

while [ 1 ];
do
    d=`date`
    count=`ls $output/$prefix* | wc -l`
    echo "($d) Counted: $count"
    if [ "$count" -gt "$threshold" ]
    then
        echo "($d) Pausing simulation."
        kill -STOP $pid
    else
        echo "($d) Resuming simulation."
        kill -CONT $pid
    fi
    echo "($d) Running script."
    ./collect-shah.py --dest /dev/null
    sleep 30;
done
