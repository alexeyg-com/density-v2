from lib.SimpleNamespace import SimpleNamespace

def read_ingolia(filename) :
    '''
    Read Ingolia quant file.
    :param filename: path to the file to be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    result = {}
    for line in lines :
        parts = line.split()
        name = parts[0]
        count = int(parts[3])
        norm_density = float(parts[1])
        if len(name) == 7 or len(name) == 9 :
            res = SimpleNamespace()
            res.count = count
            res.norm_density = norm_density
            result[name] = res
    return result

def read_shah(filename) :
    '''
    Read Shah transcript copy file.
    :param filename: path to the file to be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    result = {}
    for line in lines :
        parts = line.split()
        name = parts[0]
        copies = int(parts[2])
        res = SimpleNamespace()
        res.copies = copies
        result[name] = res
    return result

def read_siwiak(filename) :
    '''
    Read Siwiak supplementary table CSV file.
    :param filename: path to the file to be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    result = {}
    for line in lines :
        parts = line.split(',')
        name = parts[0]
        mrna = float(parts[2])
        density = float(parts[5])
        res = SimpleNamespace()
        res.mrna = mrna
        res.ribosome_density = density
        result[name] = res
    return result
   
def process_forest(forest) :
    '''
    Process forest output into read (combined read counts).
    :param forest: forest to be processed.
    '''
    genes = forest.keys()
    result = {}
    for gene in genes :
        tree = forest[gene]
        for node in tree.tree :
            left, right, ratio_average_log, ratio_m_log, mrna_average_log, mrna_m_log, rib_average_log, rib_m_log, mrna_cnt, rib_cnt, sigma = node
            if tree.length_codons == right - left + 1 :
                res = SimpleNamespace()
                res.mrna_count = mrna_cnt
                res.ribosome_count = rib_cnt
                result[gene] = res
                break
    return result

def write_csv(filename, mrna1, mrna2, fp1, fp2, shah, siwiak, tree) :
    '''
    Write combined CSV file.
    :param filename: name of the file to be written.
    '''
    genes = fp1.keys()
    fout = open(filename, 'wb')
    fout.write('Name\tmRNA A\tmRNA B\tRib A\tRib B\tmRNA A density\tmRNA B density\tRib A density\tRib B density\tShah mRNA copies\tSiwiak mRNA\tSiwiak ribosome density\tOur mRNA count\tOur Ribosome count\n')
    for name in genes :
        fout.write('%s\t' % name)
        fout.write('%d\t' % mrna1[name].count)
        fout.write('%d\t' % mrna2[name].count)
        fout.write('%d\t' % fp1[name].count)
        fout.write('%d\t' % fp2[name].count)
        fout.write('%.3f\t' % mrna1[name].norm_density)
        fout.write('%.3f\t' % mrna2[name].norm_density)
        fout.write('%.3f\t' % fp1[name].norm_density)
        fout.write('%.3f\t' % fp2[name].norm_density)
        if shah.has_key(name) :
            fout.write('%d\t' % shah[name].copies)
        else :
            fout.write('\t')
        if siwiak.has_key(name) :
            fout.write('%.3f\t' % siwiak[name].mrna)
            fout.write('%.3f\t' % siwiak[name].ribosome_density)
        else :
            fout.write('\t\t')
        if tree.has_key(name) :
            fout.write('%.3f\t' % tree[name].mrna_count)
            fout.write('%.3f\t' % tree[name].ribosome_count)
        else :
            fout.write('\t\t')
        fout.write('\n')
    fout.close()
