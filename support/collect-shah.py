#!/bin/env python
import os
import sys
import argparse
import numpy as np
import glob
import shutil

lib_path = os.path.abspath('../python/')
sys.path.append(lib_path)

from lib.SimpleNamespace import SimpleNamespace
from lib.reader import read_pickle
from lib.writer import write_pickle

def create_positions(mrna_filename) :
    fin = open(mrna_filename, 'rb')
    lines = fin.readlines()
    lines = lines[1:]
    fin.close()
    
    result = SimpleNamespace()
    result.time = 0
    result.mrna = []
    result.positions = []
    for line in lines :
        parts = line.split()
        item = SimpleNamespace()
        item.name = parts[0]
        item.exp = int(parts[2])
        result.mrna.append(item)
    
    return result

def load_positions(filename) :
    return read_pickle(filename)

def write_positions(positions, filename) :
    write_pickle(positions, filename)

def get_files(path, prefix) :
    return glob.glob('%s/%s*' % (path, prefix))

def leave_outputs(outputs, prefix, leave) :
    outputs = sorted(outputs, key = lambda x : int(os.path.basename(x)[len(prefix):]))
    outputs = outputs[:-leave]
    return outputs

def process_file(positions, filename, dest) :
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()

    n_lines = len(lines)
    for i in range(n_lines) :
        line = lines[i].split()
        line = np.array([int(x) for x in line])
        if positions.time == 0:
            positions.positions.append(line)
        else :
            positions.positions[i] = positions.positions[i] + line
    positions.time = positions.time + 1

    if dest is not None :
        if dest == '/dev/null' :
            os.remove(filename)
        else :
            shutil.move(filename, dest)
        print '\t[i] Moved file to: %s' % dest

def process_files(positions, outputs, dest = None) :
    for output in outputs :
        print '[i] Processing: %s' % os.path.basename(output)
        process_file(positions, output, dest)

def main(args) :
    if os.path.isfile(args.output) :
        positions = load_positions(args.output)
        print '[+] Read: %s' % args.output
    else :
        positions = create_positions(args.mrna)
        print '[i] Created position tracking data structure.'
    outputs = get_files(args.path, args.prefix)
    outputs = leave_outputs(outputs, args.prefix, args.leave)
    if len(outputs) > args.max :
        outputs = outputs[:args.max]
    process_files(positions, outputs, args.dest)
    if len(outputs) > 1 :
        write_positions(positions, args.output)
        print '[+] Wrote: %s' % args.output

if __name__ == "__main__" :
    parser = argparse.ArgumentParser(description = 'Parse output of the Shah model to create a steady-state ribosome occupancy map.')
    parser.add_argument('--path', type = str, default = '/dev/shm/alexeygritsenk', help = 'Path with input files.')
    parser.add_argument('--dest', type = str, default = '/data/tmp/alexeygritsenk', help = 'Path where the input files should be moved after processing.')
    parser.add_argument('--prefix', type = str, default = 'shah-time-2.4mil-seed-1413_ribo_pos_', help = 'Prefix of the files to be processed.')
    parser.add_argument('--mrna', type = str, default = 'S.cer.mRNA.abndc.ini.tsv', help = 'A file with a description of the number of mRNA molecules in the cell.')
    parser.add_argument('--leave', type = int, default = '1', help = 'Do not read last LEAVE files. They may still be incomplete.')
    parser.add_argument('--max', type = int, default = '100', help = 'Maximum number of files to process in a single run.')
    parser.add_argument('--output', type = str, default = 'tracking.out', help = 'Name of the output file to store tacking information.')
    args = parser.parse_args()
    main(args)
